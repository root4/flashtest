package;

import flixel.addons.display.FlxStarField.FlxStarField2D;
import flixel.addons.display.shapes.FlxShapeLightning;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.effects.FlxWaveSprite.WaveMode;
import flixel.addons.plugin.control.FlxControl;
import flixel.addons.weapon.FlxBullet;
import flixel.addons.weapon.FlxWeapon;
import flixel.effects.particles.FlxEmitter;
import flixel.effects.particles.FlxParticle;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.FlxSubState;
import flixel.group.FlxGroup;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.ui.FlxButton;
import flixel.ui.FlxVirtualPad;
import flixel.util.FlxCollision;
import flixel.util.FlxColor;
import flixel.util.FlxMath;
import flixel.util.FlxRect;
import flixel.util.FlxStringUtil;
import flixel.util.FlxTimer;
import openfl.Assets;
import openfl.display.BlendMode;
import openfl.events.TouchEvent;

/**
 * A FlxState which can be used for the actual gameplay.
 */
class PlayState extends FlxState
{
	private var player:Player;

	
	private var loader:FlxOgmoLoader;
	public var midgroundMap:FlxTilemap;
	private var backgroundMap:FlxTilemap;
	private var foregroundMap:FlxTilemap;
	
	
	private var tileGroup:FlxGroup;
	private var turretGroup:Array<Turret>;
	private var spikeGroup:FlxGroup;
	private var staticSpikeGroup:FlxGroup;
	private var door:FlxSprite;
	private var enemies:FlxGroup;
	private var goomba:FlxGroup;
	private var keys:FlxGroup;
	
	private var score:Int;
	private var scoreText:FlxText;
	private var key:Int;
	private var keyText:FlxText;
	private var star:FlxStarField2D;
	
	private var darkness:FlxSprite;
	private var light:FlxSprite;
	private var falling:FlxTilemap;
	
	
	private var particleEmitter:FlxEmitter;
	private var particle:FlxParticle;
	
	private var scoreEmitter:FlxEmitter;
	
	public var right:FlxSprite;
	public var left:FlxSprite;
	public var up:FlxSprite;
	
	private var buffer:FlxTimer;
	private var timer:Float;
	
	
	private var gun:FlxWeapon;
	
	private var goombaHit:FlxSound;
	private var enemyHit:FlxSound;
	private var particleCollect:FlxSound;
	private var crumbling:FlxSound;
	private var hit:FlxSound;
	private var keySound:FlxSound;
	private var shoot:FlxSound;
	
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		
		super.create();
		timer = 0 ;
		FlxG.sound.volume = 0;
		FlxG.sound.playMusic("assets/music/main.wav", 1, true);
		loader = new FlxOgmoLoader("assets/data/Level1.oel");
		midgroundMap = loader.loadTilemap("assets/images/pickleTile.png", 16, 16, "ground");
		midgroundMap.allowCollisions = FlxObject.ANY;
		backgroundMap = loader.loadTilemap("assets/images/ships.png",32,16, "background");
		backgroundMap.allowCollisions = FlxObject.NONE;
		foregroundMap = loader.loadTilemap("assets/images/upTile.png", 16, 2, "foreground");
		foregroundMap.allowCollisions = FlxObject.UP;
		
		
		tileGroup = new FlxGroup();
		turretGroup = new Array<Turret>();
		spikeGroup = new FlxGroup();
		staticSpikeGroup = new FlxGroup();
		enemies = new FlxGroup();
		keys = new FlxGroup();
		goomba = new FlxGroup();
		
		player = new Player(20, 20);
		player.health = 50;
		
		goombaHit = FlxG.sound.load("assets/images/goomba_hit.wav", .5, false);
		enemyHit = FlxG.sound.load("assets/images/enemy_hit.wav", .5, false);
		particleCollect = FlxG.sound.load("assets/images/particle.wav", .5, false);
		crumbling = FlxG.sound.load("assets/images/crumbling.wav", .5, false);
		hit = FlxG.sound.load("assets/images/hurt.wav", .5, false);
		keySound = FlxG.sound.load("assets/images/key.wav", .5, false);
		shoot = FlxG.sound.load("assets/images/shoot.wav", .5, false);
		
		
		loader.loadEntities(placeTiles, "entities");
		particleEmitter = new FlxEmitter(player.x, player.y);
		particleEmitter.acceleration.y = 200;
		particleEmitter.bounce = .3;
		
		for (i in 0...100)
		{
			particle = new FlxParticle();
			particle.makeGraphic(1,1, FlxColor.RED);
			particle.allowCollisions = FlxObject.ANY;
		
			particleEmitter.add(particle);
		}
		
		scoreEmitter = new FlxEmitter(0, 0);
		scoreEmitter.gravity = 350;
		scoreEmitter.setRotation(0, 0);
		//scoreEmitter.setXSpeed( -80, 80);
		//scoreEmitter.setYSpeed( -200, -300);
		for (i in 0...50)
		{
			scoreEmitter.add(new Gear());
		}
		
		key = 0;
		keyText = new FlxText(100, 0, 100, "Keys" + key + "/3");
		keyText.scrollFactor.set(0, 0);
		
		score = 0;
		scoreText = new FlxText(0, 0, 100, "Health: " + player.health);
		scoreText.scrollFactor.set(0, 0);
		//add(new FlxText(0, 0, 100, "Play State"));
		FlxG.camera.bgColor = 0xFF474747;
		
		//FlxG.cameras.reset(new FlxCamera(0, 0, Std.int(FlxG.width/2),Std.int(FlxG.height/2)));
	//	FlxG.camera.setBounds(0, 0, Std.int(midgroundMap.width) , Std.int(midgroundMap.height + 64));
		FlxG.camera.zoom = 2;
		FlxG.camera.width = Std.int(FlxG.camera.width / 2);
		FlxG.camera.height = Std.int(FlxG.camera.height / 2);
		FlxG.camera.setScale(4,4);
		
		//FlxG.camera.follow(player, FlxCamera.STYLE_TOPDOWN);
			//FlxG.camera.zoom = 4;
		

		star = new FlxStarField2D(0, 0, Std.int(midgroundMap.width),Std.int(midgroundMap.height + 64));
	//	star.setStarDepthColors(4, 0xffbada55, 0xfff4f4f4);
		star.setStarSpeed(25, 100);
		star.bgColor = FlxColor.BLACK;
		
		
		darkness = new FlxSprite(0, 0);
		darkness.makeGraphic(Std.int(midgroundMap.width),Std.int(midgroundMap.height  + 64), 0x77000000,true);
		darkness.blend = BlendMode.MULTIPLY;
		light = new FlxSprite(player.x, player.y);
		light.loadGraphic("assets/images/yellowLight.png");
		light.setGraphicSize(128, 128);
		light.blend = BlendMode.LIGHTEN;
		
	
		right = new FlxSprite(100,180);
		right.loadGraphic("assets/images/right.png", false, 75, 61);
		right.visible = true;
		right.alpha = .5;
		right.scrollFactor.set(0, 0);
		
		
		left = new FlxSprite(20, 180);
		left.loadGraphic("assets/images/left.png", false, 75, 61);
		left.visible = true;
		left.alpha = .5;
		left.scrollFactor.set(0, 0);
		
		
		up = new FlxSprite(325, 180);
		up.loadGraphic("assets/images/up.png", false, 80,80);
		up.visible = true;
		up.scale.set(.8,.8);
		up.alpha = .5;
		up.updateHitbox();
		up.scrollFactor.set(0, 0);
		
		
		backgroundMap.pixelPerfectRender = false;
		backgroundMap.scrollFactor.set(.6, 1);
		
		gun = new FlxWeapon("pistol");
		gun.makeImageBullet(50, "assets/images/bullet.png");
		gun.setBulletSpeed(200);
		gun.setParent(player, 0, Std.int(player.height / 3.5), true);
		gun.bulletLifeSpan = .7;
		gun.setBulletBounds(new FlxRect(0, 0, Std.int(midgroundMap.width), Std.int(midgroundMap.height)));
		gun.setFireRate(500);
		gun.onFireSound = shoot;
		
		
		buffer = new FlxTimer();
		
		
		
		add(star);
		add(backgroundMap);
		add(darkness);
		add(gun.group);
		add(spikeGroup);
		add(midgroundMap);
		add(door);
		add(player);
		
		add(particleEmitter);
		add(staticSpikeGroup);
	//	add(right);
	//	add(left);
		//add(up);
	
		add(foregroundMap);
		for (i in 0...turretGroup.length)
	{
		add(turretGroup[i]);
	}
	add(tileGroup);
	add(enemies);
	add(goomba);
	add(scoreEmitter);
	add(scoreText);
	add(keys);
	add(keyText);
	
		//
	//FlxG.debugger.drawDebug = true;
	
	
		
	}
	
	function timeout(thistimer:FlxTimer) 
	{
		//FlxG.resetState();
		
		this.openSubState(new PauseState(timer, false));

	}
	
	function moveUp(){	player.jump = true;}
	
	function moveLeft(){player.moveL = true;}
	
	function moveRight(){player.moveR = true;}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		FlxG.overlap(scoreEmitter, player, collect);
		FlxG.overlap(keys, player, removeKey);
		
		FlxG.collide(midgroundMap, enemies);
		FlxG.collide(midgroundMap, goomba);
		FlxG.overlap(goomba, player, hitGoomba);
	if (FlxG.collide(enemies, player))
	{
		hit.play();
			player.health --;
	}
		
		if (FlxG.keys.anyPressed(["SPACE"]))
		{
			
			gun.fire();
			
		}
		//darkness.makeGraphic(Std.int(midgroundMap.width),Std.int(midgroundMap.height  + 64), 0xff000000,true);
		//darkness.stamp(light, Std.int(player.x - (light.width/2)), Std.int(player.y - (light.height/2)));
		
		player.getInput(this);
		FlxG.camera.follow(player,FlxCamera.STYLE_LOCKON);
		
		for (i in 0...turretGroup.length)
	{
		FlxG.collide(turretGroup[i], player);
		FlxG.overlap(turretGroup[i].getBullets(), player,hitBullet) ;
		FlxG.collide( turretGroup[i].getBullets(), midgroundMap, removeBullet);
		
	}
	if (player.inWorldBounds() == false && player.isAlive == true)
	{
		killPlayer();
	}
	FlxG.collide(scoreEmitter, midgroundMap);
	FlxG.collide(scoreEmitter, tileGroup);
	FlxG.collide(scoreEmitter, foregroundMap);
	FlxG.collide(particleEmitter, midgroundMap);

	FlxG.collide(midgroundMap, player);
	
		
	
		if (FlxG.collide(tileGroup, player, touchTile)) {
			crumbling.play();
			FlxG.camera.shake(0.01);

		}
		else
		crumbling.stop();
	FlxG.overlap(spikeGroup, player, activateSpikes);
	if (FlxG.collide(staticSpikeGroup, player))
		player.health = 0;
	if(key ==3)
		FlxG.overlap(door, player, restartGame);
	
	gun.bulletsOverlap(enemies, killEnemy);
	
	if (  FlxG.keys.anyPressed(["DOWN", "S"]))
		player.allowCollisions = FlxObject.NONE;
	else
		player.allowCollisions = FlxObject.DOWN;
		FlxG.collide(player, foregroundMap);
		player.allowCollisions = FlxObject.ANY;
	
		
		scoreText.text= "Health: " + player.health;
		scoreText.text = "Time: " + Std.string(timer).substr(0, 4);
		if (player.health <=0)
			killPlayer();
		
		if (buffer.finished)
			timeout(buffer);
			
		timer += FlxG.elapsed;
			
				super.update();

	}	
	
	function hitGoomba(thisGoom:Goomba,thisPlayer:FlxObject) 
	{
		FlxG.collide(thisGoom, player);
		if (player.y < thisGoom.y - thisGoom.height)
		{
			goombaHit.play();
			trace(player.y +":" + thisGoom.y);
			thisGoom.hit();
			scoreEmitter.setPosition(thisGoom.x, thisGoom.y);
			scoreEmitter.start(true, 4, 0, 2);

		}
		else
		{
			hit.play();
		player.health--;
		}
	}
	
	function collect(thisParticle:FlxParticle, thisPlayer:Player) 
	{
		particleCollect.play();
		thisParticle.kill();
		if(player.health<100)
			player.health++;
		scoreText.text= "Health: " + player.health;
	}
	
	function killEnemy(thisBullet:FlxObject,thisEnemy:FlxObject) 
	{
		
		enemyHit.play();
		scoreEmitter.setPosition(thisBullet.x, thisBullet.y);

		thisBullet.kill();
		thisEnemy.kill();
		
		scoreEmitter.start(true, 4, 0, 5);
		
	}
	
	function killPlayer() 
	{

		if (player.isAlive)
				{
					
					buffer.start(2,timeout);
					this.remove(player, true);
					player.isAlive = false;
					particleEmitter.setPosition(player.x, player.y);
					particleEmitter.start(true,3,0.1,50,4);
					
					trace(player.isAlive);
				}
		
	}
	
	function activateSpikes(thisSpike:Spikes,thisPlayer:Player) 
	{
		if (player.isAlive)
				{
					thisSpike.start();
					if (FlxCollision.pixelPerfectCheck(thisSpike, thisPlayer))
						player.health = 0;
				}
				
				
			
	}
	public function placeTiles(entityName:String, entityData:Xml):Void {
		
		var x:Int = Std.parseInt(entityData.get("x"));
		var y:Int = Std.parseInt(entityData.get("y"));
		if (entityName == "falling") {
					tileGroup.add(new FallingTile(x,y));
		}
		if (entityName == "turret") {
			
			trace("turret");
			turretGroup.push(new Turret(x,y,this,entityData.get("facingRight")));
		}
		if (entityName == "spike") {
			
			trace("spike");
			spikeGroup.add(new Spikes(x,y));
		}
		if (entityName == "player") {
			
			trace("player");
			player.setPosition(x, y);
		}
		if (entityName == "spikes")
		{
			var spike:FlxSprite;
			spike = new FlxSprite(x, y);
			spike.immovable = true;
			spike.loadGraphic("assets/images/staticSpike.png", false, 16, 16);

			staticSpikeGroup.add(spike);
		}
		if (entityName == "door")
		{
			door = new FlxSprite(x, y);
			door.loadGraphic("assets/images/door.png", false, 16, 32);
		}
		if (entityName == "enemy")
		{
			enemies.add(new Enemy(x, y,this));
		}
		if (entityName == "goomba")
		{
			goomba.add(new Goomba(x, y,this));
		}
		if (entityName == "key")
		{
			var sprite:FlxSprite;
			sprite = new FlxSprite(x, y);
			sprite.loadGraphic("assets/images/key.png", false, 8, 8);
			
			keys.add(sprite);
		}
	}
	
	function restartGame(thisObject:FlxObject, thisPlayer:Player) {
		FlxG.resetState();
	}
	

	function removeBullet(thisBullet:FlxObject,thisPlayer:FlxObject)
	{
		thisBullet.kill();

	}
	function removeKey(thisKey:FlxObject, thisPlayer:FlxObject) {
		thisKey.kill();
		keySound.play();
		key ++;
		keyText.text = ("Keys:" + key + "/3");
	}
	function hitBullet(thisBullet:FlxObject, thisPlayer:Player)
	{
		player.health -= 45;
		hit.play();
		thisBullet.kill();
		
	}
	
	function touchTile(thisTile:FallingTile, thisPlayer:Player) {
		thisTile.start();
	}
}