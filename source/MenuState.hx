package;

import flixel.addons.display.FlxStarField.FlxStarField2D;
import flixel.addons.display.FlxStarField.FlxStarField3D;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.ui.FlxVirtualPad;
import flixel.util.FlxMath;
import openfl.system.System;

/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	
	 public var start:FlxButton;
	 public var settings:FlxButton;
	 public var exit:FlxButton;
	 
	 public var stars:FlxStarField2D;
	override public function create():Void
	{
		super.create();
		//this.add(new FlxText(0, 0, 100, "Hello World", 12));
		//FlxG.camera.bgColor = 0xffBADA55;
		start = new FlxButton(0, 0, "Start", gotoPlayState);
		start.centerOrigin();
		start.setPosition((FlxG.width -start.width) / 2, 50);
		
		settings = new FlxButton(0, 0, "Settings", gotoSettingState);
		settings.centerOrigin();
		settings.setPosition((FlxG.width - settings.width) / 2, 100);
		
		exit = new FlxButton(0, 0, "Exit", gotoExit);
		exit.centerOrigin();
		exit.setPosition((FlxG.width - exit.width) / 2, 150);
		
		
		stars = new FlxStarField2D(0, 0, FlxG.width, FlxG.height, 100);
		
		
		this.add(stars);
		this.add(start);
		this.add(exit);
		this.add(settings);
		
	}
	function gotoPlayState():Void {
		
		FlxG.switchState(new PlayState());
	}
	function gotoSettingState():Void {
		FlxG.switchState(new SettingState());
	}
	function gotoExit():Void {
		System.exit(0);
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		
		
		exit.destroy();
		exit = null;
		start.destroy();
		start = null;
		settings.destroy();
		settings = null;
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();
	}	
}