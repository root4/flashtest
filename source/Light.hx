package ;
import flixel.FlxG;
import flixel.FlxSprite;
import openfl.display.BlendMode;

/**
 * ...
 * @author ...
 */
class Light extends FlxSprite
{
	
	private var darkness:FlxSprite;

	public function new(x:Int, y:Int) 
	{
		super(x, y);
		this.loadGraphic("assets/images/light.png");
		this.blend = BlendMode.OVERLAY;
		
	}
	
	
	
}