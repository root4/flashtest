package ;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class Turret extends FlxSprite
{
	private var startTimer:Float;
	private var started:Bool;
	private var bullet:FlxGroup;
	private var state:FlxState;
	private var fireTimer:Float;
	public function new(x:Int, y:Int, state:FlxState, facingRight:String) 
	{
		super(x, y);
		this.loadGraphic("assets/images/turret.png", true, 16, 16, true);
		animation.add("fire", [0, 1, 2, 3, 4, 5, 6, 7], 5, true);
		this.setFacingFlip(FlxObject.RIGHT, true, false);
		this.setFacingFlip(FlxObject.LEFT, false, false);
		this.allowCollisions = FlxObject.NONE;
		
		startTimer = Math.random();
		startTimer = startTimer * 2;
		fireTimer = 1;
		started = false;
		bullet = new FlxGroup();
		this.state = state;
		
		if (facingRight == "True")
			this.facing = FlxObject.RIGHT;
		else
			this.facing = FlxObject.LEFT;
			
			trace(this.facing);
	}
	
	override public function update():Void {
		fireTimer = fireTimer - FlxG.elapsed;
		if(!started){
		if(startTimer >=0){
		startTimer = startTimer - FlxG.elapsed;
		}
		else if (startTimer <= 0)
		{
			this.start();
		}
		}
		else {
			if (this.animation.curAnim.curIndex == 1 && fireTimer <=0)
			{
				fireTimer = 1;
				this.fire();
			}
		}
		
		bullet.update();
		super.update();
	}
	private function start():Void {
			this.animation.play("fire");
			started = true;
			

	}
	private function fire():Void {
		var sprite:FlxSprite;
		sprite = new FlxSprite(x, y + 8);
		sprite.makeGraphic(4, 2, FlxColor.TEAL);
		if (this.facing == FlxObject.RIGHT)
		{
			sprite.velocity.x = 100;
			sprite.setPosition(x + 16, y + 8);
		}
		else
			sprite.velocity.x = -100;
		
		
			
		sprite.allowCollisions = FlxObject.ANY;
		bullet.add(sprite);
		state.add(bullet);

	}
	public function getBullets():FlxGroup {
		return bullet;
	}
	
}