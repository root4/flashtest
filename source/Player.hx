package ;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxPoint;

/**
 * ...
 * @author ...
 */
class Player extends FlxSprite
{
	private var jumpPower:Int = 200;
	public var isAlive:Bool = true;
	public var moveR:Bool = false;
	public var moveL:Bool = false;
	public var jump:Bool = false;
	public function new(x:Float,y:Float) 
	{
		super();
		this.loadGraphic("assets/images/player.png", true,8,17,true);
		this.width = 8;
		this.height = 16;
		this.offset = new FlxPoint(0, 1);
	
		
		this.acceleration.y = 550;
		this.maxVelocity.set(100, 1000);
		
		this.drag.set(1600, 1600);
		
		animation.add("walk", [0, 1, 2, 3,4,5,6,7,8,9,10,11], 20, true);
		animation.add("idle", [0], 1, true);
		this.setFacingFlip(FlxObject.RIGHT, false, false);
		this.setFacingFlip(FlxObject.LEFT, true, false);

	}
	override public function update():Void 
	{
		
		
		this.acceleration.x = 0;
		
		if (velocity.x == 0)
		{
			animation.play("idle");
		}
		
		
		if (moveL) {
			this.acceleration.x = -400;
			facing = FlxObject.LEFT;
			animation.play("walk");
			
		
		}
		else if  ( moveR) {
			this.acceleration.x = 400;
			this.facing = FlxObject.RIGHT;
			animation.play("walk");
			
			

		}
		if (this.isTouching(FlxObject.FLOOR) &&  jump) {
			this.velocity.y = -jumpPower;
			
		}
		jump = false;
		moveR = false;
		moveL = false;
		super.update();
	}
	public function getInput(state:PlayState):Void {
		
		getDesktopInput(state);
	
	}
	private function getMobileInput(state:PlayState):Void {
		for (touch in FlxG.touches.list)
		{
			
					if (touch.overlaps(state.right))
					{
						moveR = true;
						trace("right");
					}
					if (touch.overlaps(state.left))
					{
						moveL = true;
						trace("left");
					}
					if (touch.overlaps(state.up))
					{
						jump = true;
						trace("up");
					}
				
				
					
		}
	}
	private function getDesktopInput(state:PlayState):Void {
		
		if (FlxG.keys.anyPressed(["Left", "A"]) || FlxG.mouse.inCoords(state.left.x,state.left.y,state.left.width,state.left.height ))
			moveL = true;
		else if (FlxG.keys.anyPressed(["Right", "D"])|| FlxG.mouse.inCoords(state.right.x,state.right.y,state.right.width,state.right.height  ))
			moveR = true;
			
		if (FlxG.keys.anyJustPressed(["UP", "W"]))
			jump = true;
	}
}