package ;
import flixel.addons.display.shapes.FlxShape;
import flixel.addons.display.shapes.FlxShapeBox;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.system.ui.FlxSoundTray;
import flixel.util.FlxColor;
import flixel.util.FlxRect;
import flixel.util.FlxSpriteUtil.FillStyle;
import flixel.util.FlxSpriteUtil.LineStyle;

/**
 * ...
 * @author ...
 */
class SettingState extends FlxState
{

	private var rect:FlxShapeBox;
	private var canvas:FlxSprite;

	public function new() 
	{
		rect = new FlxShapeBox(0, 0, 100, 100, { thickness:1, color:0xff66D92F }, { hasFill:false });
		canvas = new FlxSprite();
		canvas.makeGraphic(100, 100, FlxColor.BLACK, false);
		canvas.stamp(rect, 0, 0);
		add(canvas);
		super();
		
	}
	
}