package ;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;

/**
 * ...
 * @author ...
 */
class Enemy extends FlxSprite
{
	

	private var edge:FlxSprite;
	private var state:PlayState;
	public function new(x:Int, y:Int,state:PlayState) 
	{
		this.state = state;
		super(x, y);
		edge = new FlxSprite(x - 16, y);
		edge.makeGraphic(8, 16, 0x00000000);
		this.loadGraphic("assets/images/enemy.png", true, 16, 16, true);
		animation.add("walk", [0, 1, 2], 10, true);
		this.setFacingFlip(FlxObject.RIGHT, true, false);
		this.setFacingFlip(FlxObject.LEFT, false, false);
		this.facing = FlxObject.RIGHT;
		this.velocity.x = 30;
		this.acceleration.y = .5;
		edge.x = this.x - 16;
		edge.acceleration.y = .5;
		state.add(edge);
		
	
		animation.play("walk");
	}
	
	override public function update():Void {
		
	FlxG.collide(state.midgroundMap, edge);
	if (facing == FlxObject.RIGHT)
	{
		edge.x = this.x + 16;
		
	}
	else {
		edge.x = this.x - 8;
	
	}
		if ((edge.isTouching(FlxObject.FLOOR) == false) || this.velocity.x ==0)
		{
			
			if (facing == FlxObject.LEFT)
			{
			this.facing = FlxObject.RIGHT;
			this.velocity.x = 30;
			edge.x = this.x +16;
				edge.y = this.y;
			}
			else
			{
			this.facing = FlxObject.LEFT;
			this.velocity.x = -30;
					edge.x = this.x - 8;
						edge.y = this.y;

			}
		}
		
		
		
		super.update();
	}
	
	public function getEdge():FlxSprite {
		return edge;
	}
	
}