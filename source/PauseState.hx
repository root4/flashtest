package ;

import flash.geom.Matrix3D;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxSubState;
import flixel.text.FlxText;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class PauseState extends FlxSubState
{
	private var message:FlxText;
	private var time:Float;
	private var canvas:FlxSprite;
	public function new( time:Float, finished:Bool) 
	{	
		this.time = time;
		super(0x88000000);
	
		
		
		
		
	}
	public override function create():Void {
	
		canvas = new FlxSprite(0, 0);
		canvas.makeGraphic(FlxG.width, FlxG.height,0x88000000);
		add(canvas);
		
		message = new FlxText((FlxG.width/2) - 200, 100, 200, "You finished in " + time + " seconds");
		message.color = FlxColor.BLUE;
		message.shadowOffset.set(1, 1); // = 0xff000000;
		add(message);
	}
	override public function update():Void {
		
	///	message.setPosition(FlxG.mouse.x, FlxG.mouse.y);
		super.update();
	}
	
}