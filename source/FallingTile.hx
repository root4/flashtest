package ;
import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.system.FlxCollisionType;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class FallingTile extends FlxSprite
{

	var timer:Float;
	var falling:Bool;
	public function new(x:Int, y:Int) 
	{
		super(x, y);
		this.loadGraphic("assets/images/Tile.png", false, 16, 16, true);
		timer = .25;
		falling = false;
		this.immovable = true;
		this.maxVelocity.set(0.0000000, 50);
	
	}
	override public function update():Void {
		
		this.velocity.x = 0;
		if (falling && timer <= 0)
		{
			
			this.set_solid(true);
			this.allowCollisions = FlxObject.ANY;
			this.mass =0.9;
			this.immovable = false;
			this.acceleration.y = 10;

		}
		else if (falling)
		{
			timer = timer - FlxG.elapsed;
		}
		super.update();
	}
	public function start():Void {
		falling = true;
	}
}