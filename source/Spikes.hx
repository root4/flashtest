package ;
import flixel.FlxG;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Spikes extends FlxSprite
{

	private var startTimer:Float;
	private var startCycle:Bool;
	public var cycle:Bool;
	public function new(x:Int, y:Int) 
	{
		super(x, y);
		this.loadGraphic("assets/images/spike.png", true, 16, 16, true);
		animation.add("up", [0, 1, 2, 3, 4, 5, 6,7,8,9,10,11,12,13,14], 10, false);
		
		startTimer = .25;
	}
	public function start():Void {
		startCycle = true;
	}
	override public function update():Void {
		
		if (startCycle == true)
		{
			if (startTimer >= 0)
			{
			startTimer -= FlxG.elapsed;
			}
			else if (startTimer <= 0)
			{
				animation.play("up");
				
				startCycle = false;
				cycle = true;
			}
		}
		if (cycle) {
			if (animation.finished)
			{
				startTimer = .25;
				cycle = false;
				trace("done");
			}
		}
		
		
		super.update();
	}
	
}