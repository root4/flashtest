package ;
import flixel.effects.FlxSpriteFilter;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.util.FlxColor;

/**
 * ...
 * @author ...
 */
class Splash extends FlxState
{

	
	private var screen:FlxSprite;
	override public function create():Void {
		
		screen = new FlxSprite(0, 0);
		screen.loadGraphic("assets/images/final root games.png", false, 512, 256);
		screen.setGraphicSize(FlxG.width, FlxG.height);
		screen.updateHitbox();
		add(screen);
	}
	override public function update():Void {
		FlxG.camera.fade(FlxColor.BLACK, 1, true, changeFade);
	}
	function changeFade():Void {
		FlxG.camera.fade(FlxColor.BLACK, 1, false, changeScreen);

	}
	
	function changeScreen():Void
	{
		FlxG.switchState(new MenuState());
	}
}