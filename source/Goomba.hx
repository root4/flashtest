package ;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;

/**
 * ...
 * @author ...
 */
class Goomba extends FlxSprite
{

	private var edge:FlxSprite;
	private var state:PlayState;
	public var isHit:Bool;
		public function new(x:Int, y:Int,state:PlayState) 
	{
		this.state = state;
		super(x , y + 8 );
		edge = new FlxSprite(x - 16, y);
		edge.makeGraphic(8, 8, 0x00000000);
		this.loadGraphic("assets/images/goomba.png", true, 16, 8, true);
		animation.add("walk", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],20, true);
		animation.add("squash", [10, 11, 12], 40, false);
		
		
		this.setFacingFlip(FlxObject.RIGHT, true, false);
		this.setFacingFlip(FlxObject.LEFT, false, false);
		this.facing = FlxObject.RIGHT;
		this.velocity.x = 30;
		this.acceleration.y = .5;
		edge.x = this.x - 16;
		edge.acceleration.y = .5;
		this.elasticity = 0;
		//this.immovable = true;
		state.add(edge);
		isHit = false;
	
		animation.play("walk");
	}
	
	override public function update():Void {
		
		
		this.acceleration.y = 0;
		this.acceleration.x = 0;
		
		
			FlxG.collide(state.midgroundMap, edge);
			if (facing == FlxObject.RIGHT)
			{
				edge.x = this.x + 16;
		
			}
			else {
				edge.x = this.x-8;
			}
		if ((edge.isTouching(FlxObject.FLOOR) == false) || this.velocity.x ==0 )
		{
			
			if (facing == FlxObject.LEFT)
			{
			this.facing = FlxObject.RIGHT;
			this.velocity.x = 30;
			edge.x = this.x +16;
				edge.y = this.y;
			}
			else
			{
			this.facing = FlxObject.LEFT;
			this.velocity.x = -30;
					edge.x = this.x - 8 ;
				edge.y = this.y;

			}
		}
		
		
		
		super.update();
	}
	
	public function getEdge():FlxSprite {
		return edge;
	}
	public function hit():Void {
		if(!isHit){
			isHit = true;
			animation.play("squash");
		}
			
		if (animation.get("squash").finished)
		{
			edge.kill();
			this.kill();
		}

	}
}