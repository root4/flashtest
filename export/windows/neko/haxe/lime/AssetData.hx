package lime;


import lime.utils.Assets;


class AssetData {

	private static var initialized:Bool = false;
	
	public static var library = new #if haxe3 Map <String, #else Hash <#end LibraryType> ();
	public static var path = new #if haxe3 Map <String, #else Hash <#end String> ();
	public static var type = new #if haxe3 Map <String, #else Hash <#end AssetType> ();	
	
	public static function initialize():Void {
		
		if (!initialized) {
			
			path.set ("assets/data/data-goes-here.txt", "assets/data/data-goes-here.txt");
			type.set ("assets/data/data-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/data/Level1.oel", "assets/data/Level1.oel");
			type.set ("assets/data/Level1.oel", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/blade.wav", "assets/images/blade.wav");
			type.set ("assets/images/blade.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/bullet.png", "assets/images/bullet.png");
			type.set ("assets/images/bullet.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/crumbling.wav", "assets/images/crumbling.wav");
			type.set ("assets/images/crumbling.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/door.png", "assets/images/door.png");
			type.set ("assets/images/door.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/enemy.png", "assets/images/enemy.png");
			type.set ("assets/images/enemy.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/enemy_hit.wav", "assets/images/enemy_hit.wav");
			type.set ("assets/images/enemy_hit.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/final root games.png", "assets/images/final root games.png");
			type.set ("assets/images/final root games.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/goomba.png", "assets/images/goomba.png");
			type.set ("assets/images/goomba.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/goomba_hit.wav", "assets/images/goomba_hit.wav");
			type.set ("assets/images/goomba_hit.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/hurt.wav", "assets/images/hurt.wav");
			type.set ("assets/images/hurt.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/images-go-here.txt", "assets/images/images-go-here.txt");
			type.set ("assets/images/images-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/images/key.png", "assets/images/key.png");
			type.set ("assets/images/key.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/key.wav", "assets/images/key.wav");
			type.set ("assets/images/key.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/left.png", "assets/images/left.png");
			type.set ("assets/images/left.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/light.png", "assets/images/light.png");
			type.set ("assets/images/light.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/particle.png", "assets/images/particle.png");
			type.set ("assets/images/particle.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/particle.wav", "assets/images/particle.wav");
			type.set ("assets/images/particle.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/pickleTile.png", "assets/images/pickleTile.png");
			type.set ("assets/images/pickleTile.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/player.png", "assets/images/player.png");
			type.set ("assets/images/player.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/player1.png", "assets/images/player1.png");
			type.set ("assets/images/player1.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/right.png", "assets/images/right.png");
			type.set ("assets/images/right.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/ships.png", "assets/images/ships.png");
			type.set ("assets/images/ships.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/shoot.wav", "assets/images/shoot.wav");
			type.set ("assets/images/shoot.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/images/spike.png", "assets/images/spike.png");
			type.set ("assets/images/spike.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/squash.png", "assets/images/squash.png");
			type.set ("assets/images/squash.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/staticSpike.png", "assets/images/staticSpike.png");
			type.set ("assets/images/staticSpike.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/Tile.png", "assets/images/Tile.png");
			type.set ("assets/images/Tile.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/turret.png", "assets/images/turret.png");
			type.set ("assets/images/turret.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/up.png", "assets/images/up.png");
			type.set ("assets/images/up.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/upTile.png", "assets/images/upTile.png");
			type.set ("assets/images/upTile.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/images/yellowLight.png", "assets/images/yellowLight.png");
			type.set ("assets/images/yellowLight.png", Reflect.field (AssetType, "image".toUpperCase ()));
			path.set ("assets/music/main.wav", "assets/music/main.wav");
			type.set ("assets/music/main.wav", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/music/music-goes-here.txt", "assets/music/music-goes-here.txt");
			type.set ("assets/music/music-goes-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/sounds/sounds-go-here.txt", "assets/sounds/sounds-go-here.txt");
			type.set ("assets/sounds/sounds-go-here.txt", Reflect.field (AssetType, "text".toUpperCase ()));
			path.set ("assets/sounds/beep.ogg", "assets/sounds/beep.ogg");
			type.set ("assets/sounds/beep.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			path.set ("assets/sounds/flixel.ogg", "assets/sounds/flixel.ogg");
			type.set ("assets/sounds/flixel.ogg", Reflect.field (AssetType, "sound".toUpperCase ()));
			
			
			initialized = true;
			
		} //!initialized
		
	} //initialize
	
	
} //AssetData
