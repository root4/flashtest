#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeLine
#define INCLUDED_flixel_addons_display_shapes_FlxShapeLine

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/addons/display/shapes/FlxShape.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShape)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShapeLine)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxPooled)
HX_DECLARE_CLASS2(flixel,util,FlxPoint)
HX_DECLARE_CLASS2(openfl,geom,Matrix)
namespace flixel{
namespace addons{
namespace display{
namespace shapes{


class HXCPP_CLASS_ATTRIBUTES  FlxShapeLine_obj : public ::flixel::addons::display::shapes::FlxShape_obj{
	public:
		typedef ::flixel::addons::display::shapes::FlxShape_obj super;
		typedef FlxShapeLine_obj OBJ_;
		FlxShapeLine_obj();
		Void __construct(Float X,Float Y,::flixel::util::FlxPoint a,::flixel::util::FlxPoint b,Dynamic LineStyle_);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxShapeLine_obj > __new(Float X,Float Y,::flixel::util::FlxPoint a,::flixel::util::FlxPoint b,Dynamic LineStyle_);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxShapeLine_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxShapeLine"); }

		::flixel::util::FlxPoint point;
		::flixel::util::FlxPoint point2;
		virtual Void drawSpecificShape( ::openfl::geom::Matrix matrix);

		virtual Void setPoint( ::flixel::util::FlxPoint p);
		Dynamic setPoint_dyn();

};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes

#endif /* INCLUDED_flixel_addons_display_shapes_FlxShapeLine */ 
