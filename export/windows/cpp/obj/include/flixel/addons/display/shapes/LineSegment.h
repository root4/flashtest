#ifndef INCLUDED_flixel_addons_display_shapes_LineSegment
#define INCLUDED_flixel_addons_display_shapes_LineSegment

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS4(flixel,addons,display,shapes,LineSegment)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxPooled)
HX_DECLARE_CLASS2(flixel,util,FlxPoint)
namespace flixel{
namespace addons{
namespace display{
namespace shapes{


class HXCPP_CLASS_ATTRIBUTES  LineSegment_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef LineSegment_obj OBJ_;
		LineSegment_obj();
		Void __construct(::flixel::util::FlxPoint A,::flixel::util::FlxPoint B);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< LineSegment_obj > __new(::flixel::util::FlxPoint A,::flixel::util::FlxPoint B);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~LineSegment_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("LineSegment"); }

		::flixel::util::FlxPoint a;
		::flixel::util::FlxPoint b;
		virtual ::flixel::addons::display::shapes::LineSegment copy( );
		Dynamic copy_dyn();

};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes

#endif /* INCLUDED_flixel_addons_display_shapes_LineSegment */ 
