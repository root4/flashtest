#ifndef INCLUDED_flixel_addons_display_FlxStarField3D
#define INCLUDED_flixel_addons_display_FlxStarField3D

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/addons/display/_FlxStarField/FlxStarField.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS3(flixel,addons,display,FlxStarField3D)
HX_DECLARE_CLASS4(flixel,addons,display,_FlxStarField,FlxStarField)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxPooled)
HX_DECLARE_CLASS2(flixel,util,FlxPoint)
namespace flixel{
namespace addons{
namespace display{


class HXCPP_CLASS_ATTRIBUTES  FlxStarField3D_obj : public ::flixel::addons::display::_FlxStarField::FlxStarField_obj{
	public:
		typedef ::flixel::addons::display::_FlxStarField::FlxStarField_obj super;
		typedef FlxStarField3D_obj OBJ_;
		FlxStarField3D_obj();
		Void __construct(hx::Null< int >  __o_X,hx::Null< int >  __o_Y,hx::Null< int >  __o_Width,hx::Null< int >  __o_Height,hx::Null< int >  __o_StarAmount);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxStarField3D_obj > __new(hx::Null< int >  __o_X,hx::Null< int >  __o_Y,hx::Null< int >  __o_Width,hx::Null< int >  __o_Height,hx::Null< int >  __o_StarAmount);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxStarField3D_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxStarField3D"); }

		::flixel::util::FlxPoint center;
		virtual Void destroy( );

		virtual Void update( );

};

} // end namespace flixel
} // end namespace addons
} // end namespace display

#endif /* INCLUDED_flixel_addons_display_FlxStarField3D */ 
