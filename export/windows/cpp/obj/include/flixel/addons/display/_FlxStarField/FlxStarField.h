#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStarField
#define INCLUDED_flixel_addons_display__FlxStarField_FlxStarField

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS4(flixel,addons,display,_FlxStarField,FlxStar)
HX_DECLARE_CLASS4(flixel,addons,display,_FlxStarField,FlxStarField)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
namespace flixel{
namespace addons{
namespace display{
namespace _FlxStarField{


class HXCPP_CLASS_ATTRIBUTES  FlxStarField_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef FlxStarField_obj OBJ_;
		FlxStarField_obj();
		Void __construct(int X,int Y,int Width,int Height,int StarAmount);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxStarField_obj > __new(int X,int Y,int Width,int Height,int StarAmount);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxStarField_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxStarField"); }

		int bgColor;
		Array< ::Dynamic > _stars;
		Array< int > _depthColors;
		Float _minSpeed;
		Float _maxSpeed;
		virtual Void destroy( );

		virtual Void draw( );

		virtual Void setStarDepthColors( int Depth,hx::Null< int >  LowestColor,hx::Null< int >  HighestColor);
		Dynamic setStarDepthColors_dyn();

		virtual Void setStarSpeed( int Min,int Max);
		Dynamic setStarSpeed_dyn();

};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace _FlxStarField

#endif /* INCLUDED_flixel_addons_display__FlxStarField_FlxStarField */ 
