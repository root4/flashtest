#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStar
#define INCLUDED_flixel_addons_display__FlxStarField_FlxStar

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS4(flixel,addons,display,_FlxStarField,FlxStar)
namespace flixel{
namespace addons{
namespace display{
namespace _FlxStarField{


class HXCPP_CLASS_ATTRIBUTES  FlxStar_obj : public hx::Object{
	public:
		typedef hx::Object super;
		typedef FlxStar_obj OBJ_;
		FlxStar_obj();
		Void __construct();

	public:
		inline void *operator new( size_t inSize, bool inContainer=false)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxStar_obj > __new();
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxStar_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		::String __ToString() const { return HX_CSTRING("FlxStar"); }

		int index;
		Float x;
		Float y;
		Float d;
		Float r;
		Float speed;
};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace _FlxStarField

#endif /* INCLUDED_flixel_addons_display__FlxStarField_FlxStar */ 
