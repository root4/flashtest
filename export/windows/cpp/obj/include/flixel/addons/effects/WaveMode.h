#ifndef INCLUDED_flixel_addons_effects_WaveMode
#define INCLUDED_flixel_addons_effects_WaveMode

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

HX_DECLARE_CLASS3(flixel,addons,effects,WaveMode)
namespace flixel{
namespace addons{
namespace effects{


class WaveMode_obj : public hx::EnumBase_obj
{
	typedef hx::EnumBase_obj super;
		typedef WaveMode_obj OBJ_;

	public:
		WaveMode_obj() {};
		HX_DO_ENUM_RTTI;
		static void __boot();
		static void __register();
		::String GetEnumName( ) const { return HX_CSTRING("flixel.addons.effects.WaveMode"); }
		::String __ToString() const { return HX_CSTRING("WaveMode.") + tag; }

		static ::flixel::addons::effects::WaveMode ALL;
		static inline ::flixel::addons::effects::WaveMode ALL_dyn() { return ALL; }
		static ::flixel::addons::effects::WaveMode BOTTOM;
		static inline ::flixel::addons::effects::WaveMode BOTTOM_dyn() { return BOTTOM; }
		static ::flixel::addons::effects::WaveMode TOP;
		static inline ::flixel::addons::effects::WaveMode TOP_dyn() { return TOP; }
};

} // end namespace flixel
} // end namespace addons
} // end namespace effects

#endif /* INCLUDED_flixel_addons_effects_WaveMode */ 
