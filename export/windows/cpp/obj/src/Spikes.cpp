#include <hxcpp.h>

#ifndef INCLUDED_Spikes
#include <Spikes.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif

Void Spikes_obj::__construct(int x,int y)
{
HX_STACK_FRAME("Spikes","new",0x30c8077f,"Spikes.new","Spikes.hx",16,0xb473acd1)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
{
	HX_STACK_LINE(17)
	super::__construct(x,y,null());
	HX_STACK_LINE(18)
	this->loadGraphic(HX_CSTRING("assets/images/spike.png"),true,(int)16,(int)16,true,null());
	HX_STACK_LINE(19)
	this->animation->add(HX_CSTRING("up"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8).Add((int)9).Add((int)10).Add((int)11).Add((int)12).Add((int)13).Add((int)14),(int)10,false);
	HX_STACK_LINE(21)
	this->startTimer = .25;
}
;
	return null();
}

//Spikes_obj::~Spikes_obj() { }

Dynamic Spikes_obj::__CreateEmpty() { return  new Spikes_obj; }
hx::ObjectPtr< Spikes_obj > Spikes_obj::__new(int x,int y)
{  hx::ObjectPtr< Spikes_obj > result = new Spikes_obj();
	result->__construct(x,y);
	return result;}

Dynamic Spikes_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Spikes_obj > result = new Spikes_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void Spikes_obj::start( ){
{
		HX_STACK_FRAME("Spikes","start",0xe352f141,"Spikes.start","Spikes.hx",24,0xb473acd1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(24)
		this->startCycle = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Spikes_obj,start,(void))

Void Spikes_obj::update( ){
{
		HX_STACK_FRAME("Spikes","update",0x864b4c4a,"Spikes.update","Spikes.hx",26,0xb473acd1)
		HX_STACK_THIS(this)
		HX_STACK_LINE(28)
		if (((this->startCycle == true))){
			HX_STACK_LINE(30)
			if (((this->startTimer >= (int)0))){
				HX_STACK_LINE(32)
				hx::SubEq(this->startTimer,::flixel::FlxG_obj::elapsed);
			}
			else{
				HX_STACK_LINE(34)
				if (((this->startTimer <= (int)0))){
					HX_STACK_LINE(36)
					this->animation->play(HX_CSTRING("up"),null(),null());
					HX_STACK_LINE(38)
					this->startCycle = false;
					HX_STACK_LINE(39)
					this->cycle = true;
				}
			}
		}
		HX_STACK_LINE(42)
		if ((this->cycle)){
			HX_STACK_LINE(43)
			if ((this->animation->get_finished())){
				HX_STACK_LINE(45)
				this->startTimer = .25;
				HX_STACK_LINE(46)
				this->cycle = false;
				HX_STACK_LINE(47)
				::haxe::Log_obj::trace(HX_CSTRING("done"),hx::SourceInfo(HX_CSTRING("Spikes.hx"),47,HX_CSTRING("Spikes"),HX_CSTRING("update")));
			}
		}
		HX_STACK_LINE(52)
		this->super::update();
	}
return null();
}



Spikes_obj::Spikes_obj()
{
}

Dynamic Spikes_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"cycle") ) { return cycle; }
		if (HX_FIELD_EQ(inName,"start") ) { return start_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"startTimer") ) { return startTimer; }
		if (HX_FIELD_EQ(inName,"startCycle") ) { return startCycle; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Spikes_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"cycle") ) { cycle=inValue.Cast< bool >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"startTimer") ) { startTimer=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"startCycle") ) { startCycle=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Spikes_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("startTimer"));
	outFields->push(HX_CSTRING("startCycle"));
	outFields->push(HX_CSTRING("cycle"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsFloat,(int)offsetof(Spikes_obj,startTimer),HX_CSTRING("startTimer")},
	{hx::fsBool,(int)offsetof(Spikes_obj,startCycle),HX_CSTRING("startCycle")},
	{hx::fsBool,(int)offsetof(Spikes_obj,cycle),HX_CSTRING("cycle")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("startTimer"),
	HX_CSTRING("startCycle"),
	HX_CSTRING("cycle"),
	HX_CSTRING("start"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Spikes_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Spikes_obj::__mClass,"__mClass");
};

#endif

Class Spikes_obj::__mClass;

void Spikes_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Spikes"), hx::TCanCast< Spikes_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Spikes_obj::__boot()
{
}

