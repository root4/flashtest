#include <hxcpp.h>

#ifndef INCLUDED_IMap
#include <IMap.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_input_keyboard_FlxKeyboard
#include <flixel/input/keyboard/FlxKeyboard.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_haxe_ds_IntMap
#include <haxe/ds/IntMap.h>
#endif

Void Player_obj::__construct(Float x,Float y)
{
HX_STACK_FRAME("Player","new",0x8d5554f3,"Player.new","Player.hx",11,0xa27fc9dd)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
{
	HX_STACK_LINE(17)
	this->jump = false;
	HX_STACK_LINE(16)
	this->moveL = false;
	HX_STACK_LINE(15)
	this->moveR = false;
	HX_STACK_LINE(14)
	this->isAlive = true;
	HX_STACK_LINE(13)
	this->jumpPower = (int)200;
	HX_STACK_LINE(20)
	super::__construct(null(),null(),null());
	HX_STACK_LINE(21)
	this->loadGraphic(HX_CSTRING("assets/images/player.png"),true,(int)8,(int)17,true,null());
	HX_STACK_LINE(22)
	this->set_width((int)8);
	HX_STACK_LINE(23)
	this->set_height((int)15);
	HX_STACK_LINE(24)
	::flixel::util::FlxPoint _g = ::flixel::util::FlxPoint_obj::__new((int)0,(int)1);		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(24)
	this->offset = _g;
	HX_STACK_LINE(27)
	this->acceleration->set_y((int)500);
	HX_STACK_LINE(28)
	this->maxVelocity->set((int)100,(int)200);
	HX_STACK_LINE(30)
	this->drag->set((int)1600,(int)1600);
	HX_STACK_LINE(32)
	this->animation->add(HX_CSTRING("walk"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7).Add((int)8).Add((int)9).Add((int)10).Add((int)11),(int)20,true);
	HX_STACK_LINE(33)
	this->animation->add(HX_CSTRING("idle"),Array_obj< int >::__new().Add((int)0),(int)1,true);
	struct _Function_1_1{
		inline static Dynamic Block( ){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Player.hx",34,0xa27fc9dd)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("x") , false,false);
				__result->Add(HX_CSTRING("y") , false,false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(34)
	this->_facingFlip->set((int)16,_Function_1_1::Block());
	struct _Function_1_2{
		inline static Dynamic Block( ){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Player.hx",35,0xa27fc9dd)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("x") , true,false);
				__result->Add(HX_CSTRING("y") , false,false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(35)
	this->_facingFlip->set((int)1,_Function_1_2::Block());
}
;
	return null();
}

//Player_obj::~Player_obj() { }

Dynamic Player_obj::__CreateEmpty() { return  new Player_obj; }
hx::ObjectPtr< Player_obj > Player_obj::__new(Float x,Float y)
{  hx::ObjectPtr< Player_obj > result = new Player_obj();
	result->__construct(x,y);
	return result;}

Dynamic Player_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Player_obj > result = new Player_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void Player_obj::update( ){
{
		HX_STACK_FRAME("Player","update",0xf1f8df56,"Player.update","Player.hx",39,0xa27fc9dd)
		HX_STACK_THIS(this)
		HX_STACK_LINE(42)
		this->acceleration->set_x((int)0);
		HX_STACK_LINE(44)
		if (((this->velocity->x == (int)0))){
			HX_STACK_LINE(46)
			this->animation->play(HX_CSTRING("idle"),null(),null());
		}
		HX_STACK_LINE(49)
		if (((  ((!(::flixel::FlxG_obj::keys->checkKeyStatus(Array_obj< ::String >::__new().Add(HX_CSTRING("Left")).Add(HX_CSTRING("A")),(int)1)))) ? bool(this->moveL) : bool(true) ))){
			HX_STACK_LINE(50)
			this->acceleration->set_x((int)-400);
			HX_STACK_LINE(51)
			this->set_facing((int)1);
			HX_STACK_LINE(52)
			this->animation->play(HX_CSTRING("walk"),null(),null());
		}
		else{
			HX_STACK_LINE(56)
			if (((  ((!(::flixel::FlxG_obj::keys->checkKeyStatus(Array_obj< ::String >::__new().Add(HX_CSTRING("Right")).Add(HX_CSTRING("D")),(int)1)))) ? bool(this->moveR) : bool(true) ))){
				HX_STACK_LINE(57)
				this->acceleration->set_x((int)400);
				HX_STACK_LINE(58)
				this->set_facing((int)16);
				HX_STACK_LINE(59)
				this->animation->play(HX_CSTRING("walk"),null(),null());
			}
		}
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::Player_obj > __this){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Player.hx",64,0xa27fc9dd)
				{
					HX_STACK_LINE(64)
					return (  ((!(::flixel::FlxG_obj::keys->checkKeyStatus(Array_obj< ::String >::__new().Add(HX_CSTRING("UP")).Add(HX_CSTRING("W")),(int)2)))) ? bool(__this->jump) : bool(true) );
				}
				return null();
			}
		};
		HX_STACK_LINE(64)
		if (((  (((((int(this->touching) & int((int)4096))) > (int)0))) ? bool(_Function_1_1::Block(this)) : bool(false) ))){
			HX_STACK_LINE(65)
			this->velocity->set_y(-(this->jumpPower));
		}
		HX_STACK_LINE(68)
		this->jump = false;
		HX_STACK_LINE(69)
		this->moveR = false;
		HX_STACK_LINE(70)
		this->moveL = false;
		HX_STACK_LINE(71)
		this->super::update();
	}
return null();
}



Player_obj::Player_obj()
{
}

Dynamic Player_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"jump") ) { return jump; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"moveR") ) { return moveR; }
		if (HX_FIELD_EQ(inName,"moveL") ) { return moveL; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"isAlive") ) { return isAlive; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"jumpPower") ) { return jumpPower; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Player_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"jump") ) { jump=inValue.Cast< bool >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"moveR") ) { moveR=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"moveL") ) { moveL=inValue.Cast< bool >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"isAlive") ) { isAlive=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"jumpPower") ) { jumpPower=inValue.Cast< int >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Player_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("jumpPower"));
	outFields->push(HX_CSTRING("isAlive"));
	outFields->push(HX_CSTRING("moveR"));
	outFields->push(HX_CSTRING("moveL"));
	outFields->push(HX_CSTRING("jump"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsInt,(int)offsetof(Player_obj,jumpPower),HX_CSTRING("jumpPower")},
	{hx::fsBool,(int)offsetof(Player_obj,isAlive),HX_CSTRING("isAlive")},
	{hx::fsBool,(int)offsetof(Player_obj,moveR),HX_CSTRING("moveR")},
	{hx::fsBool,(int)offsetof(Player_obj,moveL),HX_CSTRING("moveL")},
	{hx::fsBool,(int)offsetof(Player_obj,jump),HX_CSTRING("jump")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("jumpPower"),
	HX_CSTRING("isAlive"),
	HX_CSTRING("moveR"),
	HX_CSTRING("moveL"),
	HX_CSTRING("jump"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Player_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Player_obj::__mClass,"__mClass");
};

#endif

Class Player_obj::__mClass;

void Player_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Player"), hx::TCanCast< Player_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Player_obj::__boot()
{
}

