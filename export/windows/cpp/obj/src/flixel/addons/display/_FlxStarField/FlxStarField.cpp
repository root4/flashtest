#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStar
#include <flixel/addons/display/_FlxStarField/FlxStar.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStarField
#include <flixel/addons/display/_FlxStarField/FlxStarField.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_util_FlxGradient
#include <flixel/util/FlxGradient.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl_display_BitmapData
#include <openfl/display/BitmapData.h>
#endif
#ifndef INCLUDED_openfl_display_IBitmapDrawable
#include <openfl/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl_geom_Rectangle
#include <openfl/geom/Rectangle.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace _FlxStarField{

Void FlxStarField_obj::__construct(int X,int Y,int Width,int Height,int StarAmount)
{
HX_STACK_FRAME("flixel.addons.display._FlxStarField.FlxStarField","new",0x0e13d0d6,"flixel.addons.display._FlxStarField.FlxStarField.new","flixel/addons/display/FlxStarField.hx",106,0xb2ef3fd2)
HX_STACK_THIS(this)
HX_STACK_ARG(X,"X")
HX_STACK_ARG(Y,"Y")
HX_STACK_ARG(Width,"Width")
HX_STACK_ARG(Height,"Height")
HX_STACK_ARG(StarAmount,"StarAmount")
{
	HX_STACK_LINE(108)
	this->bgColor = (int)-16777216;
	HX_STACK_LINE(117)
	super::__construct(X,Y,null());
	HX_STACK_LINE(118)
	if (((Width <= (int)0))){
		HX_STACK_LINE(118)
		Width = ::flixel::FlxG_obj::width;
	}
	else{
		HX_STACK_LINE(118)
		Width = Width;
	}
	HX_STACK_LINE(119)
	if (((Height <= (int)0))){
		HX_STACK_LINE(119)
		Height = ::flixel::FlxG_obj::height;
	}
	else{
		HX_STACK_LINE(119)
		Height = Height;
	}
	HX_STACK_LINE(120)
	this->makeGraphic(Width,Height,this->bgColor,true,null());
	HX_STACK_LINE(121)
	this->_stars = Array_obj< ::Dynamic >::__new();
	HX_STACK_LINE(123)
	{
		HX_STACK_LINE(123)
		int _g = (int)0;		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(123)
		while((true)){
			HX_STACK_LINE(123)
			if ((!(((_g < StarAmount))))){
				HX_STACK_LINE(123)
				break;
			}
			HX_STACK_LINE(123)
			int i = (_g)++;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(125)
			::flixel::addons::display::_FlxStarField::FlxStar star = ::flixel::addons::display::_FlxStarField::FlxStar_obj::__new();		HX_STACK_VAR(star,"star");
			HX_STACK_LINE(126)
			star->index = i;
			HX_STACK_LINE(127)
			int _g1 = ::flixel::util::FlxRandom_obj::intRanged((int)0,Width,null());		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(127)
			star->x = _g1;
			HX_STACK_LINE(128)
			int _g11 = ::flixel::util::FlxRandom_obj::intRanged((int)0,Height,null());		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(128)
			star->y = _g11;
			HX_STACK_LINE(129)
			star->d = (int)1;
			HX_STACK_LINE(130)
			int _g2 = ::flixel::util::FlxRandom_obj::_internalSeed = (int(hx::Mod((::flixel::util::FlxRandom_obj::_internalSeed * (int)48271),(int)2147483647)) & int((int)2147483647));		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(130)
			Float _g3 = (Float(_g2) / Float((int)2147483647));		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(130)
			Float _g4 = (_g3 * ::Math_obj::PI);		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(130)
			Float _g5 = (_g4 * (int)2);		HX_STACK_VAR(_g5,"_g5");
			HX_STACK_LINE(130)
			star->r = _g5;
			HX_STACK_LINE(131)
			this->_stars->push(star);
		}
	}
}
;
	return null();
}

//FlxStarField_obj::~FlxStarField_obj() { }

Dynamic FlxStarField_obj::__CreateEmpty() { return  new FlxStarField_obj; }
hx::ObjectPtr< FlxStarField_obj > FlxStarField_obj::__new(int X,int Y,int Width,int Height,int StarAmount)
{  hx::ObjectPtr< FlxStarField_obj > result = new FlxStarField_obj();
	result->__construct(X,Y,Width,Height,StarAmount);
	return result;}

Dynamic FlxStarField_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxStarField_obj > result = new FlxStarField_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4]);
	return result;}

Void FlxStarField_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.addons.display._FlxStarField.FlxStarField","destroy",0x038ea070,"flixel.addons.display._FlxStarField.FlxStarField.destroy","flixel/addons/display/FlxStarField.hx",136,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(137)
		{
			HX_STACK_LINE(137)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(137)
			Array< ::Dynamic > _g1 = this->_stars;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(137)
			while((true)){
				HX_STACK_LINE(137)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(137)
					break;
				}
				HX_STACK_LINE(137)
				::flixel::addons::display::_FlxStarField::FlxStar star = _g1->__get(_g).StaticCast< ::flixel::addons::display::_FlxStarField::FlxStar >();		HX_STACK_VAR(star,"star");
				HX_STACK_LINE(137)
				++(_g);
				HX_STACK_LINE(139)
				star = null();
			}
		}
		HX_STACK_LINE(141)
		this->_stars = null();
		HX_STACK_LINE(142)
		this->_depthColors = null();
		HX_STACK_LINE(143)
		this->super::destroy();
	}
return null();
}


Void FlxStarField_obj::draw( ){
{
		HX_STACK_FRAME("flixel.addons.display._FlxStarField.FlxStarField","draw",0x3cb092ce,"flixel.addons.display._FlxStarField.FlxStarField.draw","flixel/addons/display/FlxStarField.hx",147,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(148)
		this->get_pixels()->lock();
		HX_STACK_LINE(149)
		this->get_pixels()->fillRect(this->_flashRect,this->bgColor);
		HX_STACK_LINE(151)
		{
			HX_STACK_LINE(151)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(151)
			Array< ::Dynamic > _g1 = this->_stars;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(151)
			while((true)){
				HX_STACK_LINE(151)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(151)
					break;
				}
				HX_STACK_LINE(151)
				::flixel::addons::display::_FlxStarField::FlxStar star = _g1->__get(_g).StaticCast< ::flixel::addons::display::_FlxStarField::FlxStar >();		HX_STACK_VAR(star,"star");
				HX_STACK_LINE(151)
				++(_g);
				HX_STACK_LINE(153)
				int colorIndex = ::Std_obj::_int(((Float(((star->speed - this->_minSpeed))) / Float(((this->_maxSpeed - this->_minSpeed)))) * this->_depthColors->length));		HX_STACK_VAR(colorIndex,"colorIndex");
				HX_STACK_LINE(154)
				int _g2 = ::Std_obj::_int(star->x);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(154)
				int _g11 = ::Std_obj::_int(star->y);		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(154)
				this->get_pixels()->setPixel32(_g2,_g11,this->_depthColors->__get(colorIndex));
			}
		}
		HX_STACK_LINE(157)
		this->get_pixels()->unlock(null());
		HX_STACK_LINE(158)
		::openfl::display::BitmapData _g2 = this->get_pixels();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(158)
		this->framePixels = _g2;
		HX_STACK_LINE(159)
		this->super::draw();
	}
return null();
}


Void FlxStarField_obj::setStarDepthColors( int Depth,hx::Null< int >  __o_LowestColor,hx::Null< int >  __o_HighestColor){
int LowestColor = __o_LowestColor.Default(267933784);
int HighestColor = __o_HighestColor.Default(-723724);
	HX_STACK_FRAME("flixel.addons.display._FlxStarField.FlxStarField","setStarDepthColors",0xcf6b9729,"flixel.addons.display._FlxStarField.FlxStarField.setStarDepthColors","flixel/addons/display/FlxStarField.hx",170,0xb2ef3fd2)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Depth,"Depth")
	HX_STACK_ARG(LowestColor,"LowestColor")
	HX_STACK_ARG(HighestColor,"HighestColor")
{
		HX_STACK_LINE(171)
		Array< int > _g = ::flixel::util::FlxGradient_obj::createGradientArray((int)1,Depth,Array_obj< int >::__new().Add(LowestColor).Add(HighestColor),null(),null(),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(171)
		this->_depthColors = _g;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC3(FlxStarField_obj,setStarDepthColors,(void))

Void FlxStarField_obj::setStarSpeed( int Min,int Max){
{
		HX_STACK_FRAME("flixel.addons.display._FlxStarField.FlxStarField","setStarSpeed",0x19190b1d,"flixel.addons.display._FlxStarField.FlxStarField.setStarSpeed","flixel/addons/display/FlxStarField.hx",175,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_ARG(Min,"Min")
		HX_STACK_ARG(Max,"Max")
		HX_STACK_LINE(176)
		this->_minSpeed = Min;
		HX_STACK_LINE(177)
		this->_maxSpeed = Max;
		HX_STACK_LINE(179)
		{
			HX_STACK_LINE(179)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(179)
			Array< ::Dynamic > _g1 = this->_stars;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(179)
			while((true)){
				HX_STACK_LINE(179)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(179)
					break;
				}
				HX_STACK_LINE(179)
				::flixel::addons::display::_FlxStarField::FlxStar star = _g1->__get(_g).StaticCast< ::flixel::addons::display::_FlxStarField::FlxStar >();		HX_STACK_VAR(star,"star");
				HX_STACK_LINE(179)
				++(_g);
				HX_STACK_LINE(181)
				Float _g2 = ::flixel::util::FlxRandom_obj::floatRanged(Min,Max,null());		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(181)
				star->speed = _g2;
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(FlxStarField_obj,setStarSpeed,(void))


FlxStarField_obj::FlxStarField_obj()
{
}

void FlxStarField_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxStarField);
	HX_MARK_MEMBER_NAME(bgColor,"bgColor");
	HX_MARK_MEMBER_NAME(_stars,"_stars");
	HX_MARK_MEMBER_NAME(_depthColors,"_depthColors");
	HX_MARK_MEMBER_NAME(_minSpeed,"_minSpeed");
	HX_MARK_MEMBER_NAME(_maxSpeed,"_maxSpeed");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxStarField_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(bgColor,"bgColor");
	HX_VISIT_MEMBER_NAME(_stars,"_stars");
	HX_VISIT_MEMBER_NAME(_depthColors,"_depthColors");
	HX_VISIT_MEMBER_NAME(_minSpeed,"_minSpeed");
	HX_VISIT_MEMBER_NAME(_maxSpeed,"_maxSpeed");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxStarField_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"_stars") ) { return _stars; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"bgColor") ) { return bgColor; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"_minSpeed") ) { return _minSpeed; }
		if (HX_FIELD_EQ(inName,"_maxSpeed") ) { return _maxSpeed; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"_depthColors") ) { return _depthColors; }
		if (HX_FIELD_EQ(inName,"setStarSpeed") ) { return setStarSpeed_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"setStarDepthColors") ) { return setStarDepthColors_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxStarField_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"_stars") ) { _stars=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"bgColor") ) { bgColor=inValue.Cast< int >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"_minSpeed") ) { _minSpeed=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_maxSpeed") ) { _maxSpeed=inValue.Cast< Float >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"_depthColors") ) { _depthColors=inValue.Cast< Array< int > >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxStarField_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("bgColor"));
	outFields->push(HX_CSTRING("_stars"));
	outFields->push(HX_CSTRING("_depthColors"));
	outFields->push(HX_CSTRING("_minSpeed"));
	outFields->push(HX_CSTRING("_maxSpeed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsInt,(int)offsetof(FlxStarField_obj,bgColor),HX_CSTRING("bgColor")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(FlxStarField_obj,_stars),HX_CSTRING("_stars")},
	{hx::fsObject /*Array< int >*/ ,(int)offsetof(FlxStarField_obj,_depthColors),HX_CSTRING("_depthColors")},
	{hx::fsFloat,(int)offsetof(FlxStarField_obj,_minSpeed),HX_CSTRING("_minSpeed")},
	{hx::fsFloat,(int)offsetof(FlxStarField_obj,_maxSpeed),HX_CSTRING("_maxSpeed")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("bgColor"),
	HX_CSTRING("_stars"),
	HX_CSTRING("_depthColors"),
	HX_CSTRING("_minSpeed"),
	HX_CSTRING("_maxSpeed"),
	HX_CSTRING("destroy"),
	HX_CSTRING("draw"),
	HX_CSTRING("setStarDepthColors"),
	HX_CSTRING("setStarSpeed"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxStarField_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxStarField_obj::__mClass,"__mClass");
};

#endif

Class FlxStarField_obj::__mClass;

void FlxStarField_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display._FlxStarField.FlxStarField"), hx::TCanCast< FlxStarField_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxStarField_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace _FlxStarField
