#include <hxcpp.h>

#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_FlxStarField2D
#include <flixel/addons/display/FlxStarField2D.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStar
#include <flixel/addons/display/_FlxStarField/FlxStar.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStarField
#include <flixel/addons/display/_FlxStarField/FlxStarField.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxGradient
#include <flixel/util/FlxGradient.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxPoint
#include <flixel/util/FlxPool_flixel_util_FlxPoint.h>
#endif
namespace flixel{
namespace addons{
namespace display{

Void FlxStarField2D_obj::__construct(hx::Null< int >  __o_X,hx::Null< int >  __o_Y,hx::Null< int >  __o_Width,hx::Null< int >  __o_Height,hx::Null< int >  __o_StarAmount)
{
HX_STACK_FRAME("flixel.addons.display.FlxStarField2D","new",0x977dd6cf,"flixel.addons.display.FlxStarField2D.new","flixel/addons/display/FlxStarField.hx",20,0xb2ef3fd2)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(__o_Width,"Width")
HX_STACK_ARG(__o_Height,"Height")
HX_STACK_ARG(__o_StarAmount,"StarAmount")
int X = __o_X.Default(0);
int Y = __o_Y.Default(0);
int Width = __o_Width.Default(0);
int Height = __o_Height.Default(0);
int StarAmount = __o_StarAmount.Default(300);
{
	HX_STACK_LINE(21)
	super::__construct(X,Y,Width,Height,StarAmount);
	HX_STACK_LINE(22)
	::flixel::util::FlxPoint _g;		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(22)
	{
		HX_STACK_LINE(22)
		::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set((int)-1,(int)0);		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(22)
		point->_inPool = false;
		HX_STACK_LINE(22)
		_g = point;
	}
	HX_STACK_LINE(22)
	this->starVelocityOffset = _g;
	HX_STACK_LINE(23)
	{
		HX_STACK_LINE(23)
		Array< int > _g1 = ::flixel::util::FlxGradient_obj::createGradientArray((int)1,(int)5,Array_obj< int >::__new().Add((int)-10987432).Add((int)-723724),null(),null(),null());		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(23)
		this->_depthColors = _g1;
	}
	HX_STACK_LINE(24)
	this->setStarSpeed((int)100,(int)400);
}
;
	return null();
}

//FlxStarField2D_obj::~FlxStarField2D_obj() { }

Dynamic FlxStarField2D_obj::__CreateEmpty() { return  new FlxStarField2D_obj; }
hx::ObjectPtr< FlxStarField2D_obj > FlxStarField2D_obj::__new(hx::Null< int >  __o_X,hx::Null< int >  __o_Y,hx::Null< int >  __o_Width,hx::Null< int >  __o_Height,hx::Null< int >  __o_StarAmount)
{  hx::ObjectPtr< FlxStarField2D_obj > result = new FlxStarField2D_obj();
	result->__construct(__o_X,__o_Y,__o_Width,__o_Height,__o_StarAmount);
	return result;}

Dynamic FlxStarField2D_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxStarField2D_obj > result = new FlxStarField2D_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4]);
	return result;}

Void FlxStarField2D_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxStarField2D","destroy",0xe9a596e9,"flixel.addons.display.FlxStarField2D.destroy","flixel/addons/display/FlxStarField.hx",28,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(29)
		::flixel::util::FlxPoint _g = ::flixel::util::FlxDestroyUtil_obj::put(this->starVelocityOffset);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(29)
		this->starVelocityOffset = _g;
		HX_STACK_LINE(30)
		this->super::destroy();
	}
return null();
}


Void FlxStarField2D_obj::update( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxStarField2D","update",0x86c1eefa,"flixel.addons.display.FlxStarField2D.update","flixel/addons/display/FlxStarField.hx",34,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(35)
		{
			HX_STACK_LINE(35)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(35)
			Array< ::Dynamic > _g1 = this->_stars;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(35)
			while((true)){
				HX_STACK_LINE(35)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(35)
					break;
				}
				HX_STACK_LINE(35)
				::flixel::addons::display::_FlxStarField::FlxStar star = _g1->__get(_g).StaticCast< ::flixel::addons::display::_FlxStarField::FlxStar >();		HX_STACK_VAR(star,"star");
				HX_STACK_LINE(35)
				++(_g);
				HX_STACK_LINE(37)
				hx::AddEq(star->x,((this->starVelocityOffset->x * star->speed) * ::flixel::FlxG_obj::elapsed));
				HX_STACK_LINE(38)
				hx::AddEq(star->y,((this->starVelocityOffset->y * star->speed) * ::flixel::FlxG_obj::elapsed));
				HX_STACK_LINE(41)
				Float _g2 = this->get_width();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(41)
				if (((star->x > _g2))){
					HX_STACK_LINE(43)
					star->x = (int)0;
				}
				else{
					HX_STACK_LINE(45)
					if (((star->x < (int)0))){
						HX_STACK_LINE(47)
						Float _g11 = this->get_width();		HX_STACK_VAR(_g11,"_g11");
						HX_STACK_LINE(47)
						star->x = _g11;
					}
				}
				HX_STACK_LINE(50)
				Float _g21 = this->get_height();		HX_STACK_VAR(_g21,"_g21");
				HX_STACK_LINE(50)
				if (((star->y > _g21))){
					HX_STACK_LINE(52)
					star->y = (int)0;
				}
				else{
					HX_STACK_LINE(54)
					if (((star->y < (int)0))){
						HX_STACK_LINE(56)
						Float _g3 = this->get_height();		HX_STACK_VAR(_g3,"_g3");
						HX_STACK_LINE(56)
						star->y = _g3;
					}
				}
			}
		}
		HX_STACK_LINE(60)
		this->super::update();
	}
return null();
}



FlxStarField2D_obj::FlxStarField2D_obj()
{
}

void FlxStarField2D_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxStarField2D);
	HX_MARK_MEMBER_NAME(starVelocityOffset,"starVelocityOffset");
	::flixel::addons::display::_FlxStarField::FlxStarField_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxStarField2D_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(starVelocityOffset,"starVelocityOffset");
	::flixel::addons::display::_FlxStarField::FlxStarField_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxStarField2D_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"starVelocityOffset") ) { return starVelocityOffset; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxStarField2D_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 18:
		if (HX_FIELD_EQ(inName,"starVelocityOffset") ) { starVelocityOffset=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxStarField2D_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("starVelocityOffset"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(FlxStarField2D_obj,starVelocityOffset),HX_CSTRING("starVelocityOffset")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("starVelocityOffset"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxStarField2D_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxStarField2D_obj::__mClass,"__mClass");
};

#endif

Class FlxStarField2D_obj::__mClass;

void FlxStarField2D_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.FlxStarField2D"), hx::TCanCast< FlxStarField2D_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxStarField2D_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
