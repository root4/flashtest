#include <hxcpp.h>

#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStar
#include <flixel/addons/display/_FlxStarField/FlxStar.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace _FlxStarField{

Void FlxStar_obj::__construct()
{
HX_STACK_FRAME("flixel.addons.display._FlxStarField.FlxStar","new",0x658f6ee8,"flixel.addons.display._FlxStarField.FlxStar.new","flixel/addons/display/FlxStarField.hx",195,0xb2ef3fd2)
HX_STACK_THIS(this)
{
}
;
	return null();
}

//FlxStar_obj::~FlxStar_obj() { }

Dynamic FlxStar_obj::__CreateEmpty() { return  new FlxStar_obj; }
hx::ObjectPtr< FlxStar_obj > FlxStar_obj::__new()
{  hx::ObjectPtr< FlxStar_obj > result = new FlxStar_obj();
	result->__construct();
	return result;}

Dynamic FlxStar_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxStar_obj > result = new FlxStar_obj();
	result->__construct();
	return result;}


FlxStar_obj::FlxStar_obj()
{
}

Dynamic FlxStar_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 1:
		if (HX_FIELD_EQ(inName,"x") ) { return x; }
		if (HX_FIELD_EQ(inName,"y") ) { return y; }
		if (HX_FIELD_EQ(inName,"d") ) { return d; }
		if (HX_FIELD_EQ(inName,"r") ) { return r; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"index") ) { return index; }
		if (HX_FIELD_EQ(inName,"speed") ) { return speed; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxStar_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 1:
		if (HX_FIELD_EQ(inName,"x") ) { x=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"y") ) { y=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"d") ) { d=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"r") ) { r=inValue.Cast< Float >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"index") ) { index=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"speed") ) { speed=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxStar_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("index"));
	outFields->push(HX_CSTRING("x"));
	outFields->push(HX_CSTRING("y"));
	outFields->push(HX_CSTRING("d"));
	outFields->push(HX_CSTRING("r"));
	outFields->push(HX_CSTRING("speed"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsInt,(int)offsetof(FlxStar_obj,index),HX_CSTRING("index")},
	{hx::fsFloat,(int)offsetof(FlxStar_obj,x),HX_CSTRING("x")},
	{hx::fsFloat,(int)offsetof(FlxStar_obj,y),HX_CSTRING("y")},
	{hx::fsFloat,(int)offsetof(FlxStar_obj,d),HX_CSTRING("d")},
	{hx::fsFloat,(int)offsetof(FlxStar_obj,r),HX_CSTRING("r")},
	{hx::fsFloat,(int)offsetof(FlxStar_obj,speed),HX_CSTRING("speed")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("index"),
	HX_CSTRING("x"),
	HX_CSTRING("y"),
	HX_CSTRING("d"),
	HX_CSTRING("r"),
	HX_CSTRING("speed"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxStar_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxStar_obj::__mClass,"__mClass");
};

#endif

Class FlxStar_obj::__mClass;

void FlxStar_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display._FlxStarField.FlxStar"), hx::TCanCast< FlxStar_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxStar_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace _FlxStarField
