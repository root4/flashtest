#include <hxcpp.h>

#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShape
#include <flixel/addons/display/shapes/FlxShape.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeLine
#include <flixel/addons/display/shapes/FlxShapeLine.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCallbackPoint
#include <flixel/util/FlxCallbackPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxSpriteUtil
#include <flixel/util/FlxSpriteUtil.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl_geom_Matrix
#include <openfl/geom/Matrix.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace shapes{

Void FlxShapeLine_obj::__construct(Float X,Float Y,::flixel::util::FlxPoint a,::flixel::util::FlxPoint b,Dynamic LineStyle_)
{
HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLine","new",0x42cf895c,"flixel.addons.display.shapes.FlxShapeLine.new","flixel/addons/display/shapes/FlxShapeLine.hx",24,0xaa2af194)
HX_STACK_THIS(this)
HX_STACK_ARG(X,"X")
HX_STACK_ARG(Y,"Y")
HX_STACK_ARG(a,"a")
HX_STACK_ARG(b,"b")
HX_STACK_ARG(LineStyle_,"LineStyle_")
{
	HX_STACK_LINE(25)
	this->shape_id = HX_CSTRING("line");
	HX_STACK_LINE(27)
	::flixel::util::FlxCallbackPoint _g = ::flixel::util::FlxCallbackPoint_obj::__new(this->setPoint_dyn(),null(),null());		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(27)
	this->point = _g;
	HX_STACK_LINE(28)
	::flixel::util::FlxCallbackPoint _g1 = ::flixel::util::FlxCallbackPoint_obj::__new(this->setPoint_dyn(),null(),null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(28)
	this->point2 = _g1;
	HX_STACK_LINE(30)
	{
		HX_STACK_LINE(30)
		::flixel::util::FlxPoint _this = this->point;		HX_STACK_VAR(_this,"_this");
		HX_STACK_LINE(30)
		_this->set_x(a->x);
		HX_STACK_LINE(30)
		_this->set_y(a->y);
		HX_STACK_LINE(30)
		_this;
	}
	HX_STACK_LINE(31)
	{
		HX_STACK_LINE(31)
		::flixel::util::FlxPoint _this = this->point2;		HX_STACK_VAR(_this,"_this");
		HX_STACK_LINE(31)
		_this->set_x(b->x);
		HX_STACK_LINE(31)
		_this->set_y(b->y);
		HX_STACK_LINE(31)
		_this;
	}
	HX_STACK_LINE(33)
	if ((a->_weak)){
		HX_STACK_LINE(33)
		a->put();
	}
	HX_STACK_LINE(34)
	if ((b->_weak)){
		HX_STACK_LINE(34)
		b->put();
	}
	HX_STACK_LINE(36)
	Float strokeBuffer = this->lineStyle->__Field(HX_CSTRING("thickness"),true);		HX_STACK_VAR(strokeBuffer,"strokeBuffer");
	HX_STACK_LINE(38)
	Float trueWidth = ::Math_obj::abs((a->x - b->x));		HX_STACK_VAR(trueWidth,"trueWidth");
	HX_STACK_LINE(39)
	Float trueHeight = ::Math_obj::abs((a->y - b->y));		HX_STACK_VAR(trueHeight,"trueHeight");
	HX_STACK_LINE(41)
	Float w = (trueWidth + strokeBuffer);		HX_STACK_VAR(w,"w");
	HX_STACK_LINE(42)
	Float h = (trueHeight + strokeBuffer);		HX_STACK_VAR(h,"h");
	HX_STACK_LINE(44)
	if (((w <= (int)0))){
		HX_STACK_LINE(45)
		w = strokeBuffer;
	}
	HX_STACK_LINE(46)
	if (((h <= (int)0))){
		HX_STACK_LINE(47)
		h = strokeBuffer;
	}
	HX_STACK_LINE(49)
	super::__construct(X,Y,w,h,LineStyle_,null(),trueWidth,trueHeight);
}
;
	return null();
}

//FlxShapeLine_obj::~FlxShapeLine_obj() { }

Dynamic FlxShapeLine_obj::__CreateEmpty() { return  new FlxShapeLine_obj; }
hx::ObjectPtr< FlxShapeLine_obj > FlxShapeLine_obj::__new(Float X,Float Y,::flixel::util::FlxPoint a,::flixel::util::FlxPoint b,Dynamic LineStyle_)
{  hx::ObjectPtr< FlxShapeLine_obj > result = new FlxShapeLine_obj();
	result->__construct(X,Y,a,b,LineStyle_);
	return result;}

Dynamic FlxShapeLine_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxShapeLine_obj > result = new FlxShapeLine_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4]);
	return result;}

Void FlxShapeLine_obj::drawSpecificShape( ::openfl::geom::Matrix matrix){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLine","drawSpecificShape",0x6a983867,"flixel.addons.display.shapes.FlxShapeLine.drawSpecificShape","flixel/addons/display/shapes/FlxShapeLine.hx",54,0xaa2af194)
		HX_STACK_THIS(this)
		HX_STACK_ARG(matrix,"matrix")
		struct _Function_1_1{
			inline static Dynamic Block( ::openfl::geom::Matrix &matrix){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/shapes/FlxShapeLine.hx",54,0xaa2af194)
				{
					hx::Anon __result = hx::Anon_obj::Create();
					__result->Add(HX_CSTRING("matrix") , matrix,false);
					return __result;
				}
				return null();
			}
		};
		HX_STACK_LINE(54)
		::flixel::util::FlxSpriteUtil_obj::drawLine(hx::ObjectPtr<OBJ_>(this),this->point->x,this->point->y,this->point2->x,this->point2->y,this->lineStyle,_Function_1_1::Block(matrix));
	}
return null();
}


Void FlxShapeLine_obj::setPoint( ::flixel::util::FlxPoint p){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLine","setPoint",0x20bc52d2,"flixel.addons.display.shapes.FlxShapeLine.setPoint","flixel/addons/display/shapes/FlxShapeLine.hx",59,0xaa2af194)
		HX_STACK_THIS(this)
		HX_STACK_ARG(p,"p")
		HX_STACK_LINE(59)
		this->shapeDirty = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShapeLine_obj,setPoint,(void))


FlxShapeLine_obj::FlxShapeLine_obj()
{
}

void FlxShapeLine_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxShapeLine);
	HX_MARK_MEMBER_NAME(point,"point");
	HX_MARK_MEMBER_NAME(point2,"point2");
	::flixel::addons::display::shapes::FlxShape_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxShapeLine_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(point,"point");
	HX_VISIT_MEMBER_NAME(point2,"point2");
	::flixel::addons::display::shapes::FlxShape_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxShapeLine_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"point") ) { return point; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"point2") ) { return point2; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"setPoint") ) { return setPoint_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"drawSpecificShape") ) { return drawSpecificShape_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxShapeLine_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"point") ) { point=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"point2") ) { point2=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxShapeLine_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("point"));
	outFields->push(HX_CSTRING("point2"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(FlxShapeLine_obj,point),HX_CSTRING("point")},
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(FlxShapeLine_obj,point2),HX_CSTRING("point2")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("point"),
	HX_CSTRING("point2"),
	HX_CSTRING("drawSpecificShape"),
	HX_CSTRING("setPoint"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxShapeLine_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxShapeLine_obj::__mClass,"__mClass");
};

#endif

Class FlxShapeLine_obj::__mClass;

void FlxShapeLine_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.shapes.FlxShapeLine"), hx::TCanCast< FlxShapeLine_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxShapeLine_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes
