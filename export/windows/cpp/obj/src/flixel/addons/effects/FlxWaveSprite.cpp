#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_effects_FlxWaveSprite
#include <flixel/addons/effects/FlxWaveSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_effects_WaveMode
#include <flixel/addons/effects/WaveMode.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_system_layer_TileSheetData
#include <flixel/system/layer/TileSheetData.h>
#endif
#ifndef INCLUDED_flixel_util_loaders_CachedGraphics
#include <flixel/util/loaders/CachedGraphics.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl_display_BitmapData
#include <openfl/display/BitmapData.h>
#endif
#ifndef INCLUDED_openfl_display_IBitmapDrawable
#include <openfl/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl_geom_Point
#include <openfl/geom/Point.h>
#endif
#ifndef INCLUDED_openfl_geom_Rectangle
#include <openfl/geom/Rectangle.h>
#endif
namespace flixel{
namespace addons{
namespace effects{

Void FlxWaveSprite_obj::__construct(::flixel::FlxSprite Target,::flixel::addons::effects::WaveMode Mode,hx::Null< int >  __o_Strength,hx::Null< int >  __o_Center,hx::Null< Float >  __o_Speed)
{
HX_STACK_FRAME("flixel.addons.effects.FlxWaveSprite","new",0x522aaeed,"flixel.addons.effects.FlxWaveSprite.new","flixel/addons/effects/FlxWaveSprite.hx",15,0x56d0f144)
HX_STACK_THIS(this)
HX_STACK_ARG(Target,"Target")
HX_STACK_ARG(Mode,"Mode")
HX_STACK_ARG(__o_Strength,"Strength")
HX_STACK_ARG(__o_Center,"Center")
HX_STACK_ARG(__o_Speed,"Speed")
int Strength = __o_Strength.Default(20);
int Center = __o_Center.Default(-1);
Float Speed = __o_Speed.Default(3);
{
	HX_STACK_LINE(42)
	this->_time = (int)0;
	HX_STACK_LINE(40)
	this->_targetOffset = (int)-999;
	HX_STACK_LINE(55)
	super::__construct(null(),null(),null());
	HX_STACK_LINE(56)
	if (((Mode == null()))){
		HX_STACK_LINE(57)
		Mode = ::flixel::addons::effects::WaveMode_obj::ALL;
	}
	HX_STACK_LINE(58)
	this->_target = Target;
	HX_STACK_LINE(59)
	this->set_strength(Strength);
	HX_STACK_LINE(60)
	this->mode = Mode;
	HX_STACK_LINE(61)
	this->speed = Speed;
	HX_STACK_LINE(62)
	if (((Center < (int)0))){
		HX_STACK_LINE(63)
		Float _g = this->_target->get_height();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(63)
		Float _g1 = (_g * 0.5);		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(63)
		int _g2 = ::Std_obj::_int(_g1);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(63)
		this->center = _g2;
	}
	HX_STACK_LINE(64)
	this->initPixels();
	HX_STACK_LINE(65)
	this->dirty = true;
}
;
	return null();
}

//FlxWaveSprite_obj::~FlxWaveSprite_obj() { }

Dynamic FlxWaveSprite_obj::__CreateEmpty() { return  new FlxWaveSprite_obj; }
hx::ObjectPtr< FlxWaveSprite_obj > FlxWaveSprite_obj::__new(::flixel::FlxSprite Target,::flixel::addons::effects::WaveMode Mode,hx::Null< int >  __o_Strength,hx::Null< int >  __o_Center,hx::Null< Float >  __o_Speed)
{  hx::ObjectPtr< FlxWaveSprite_obj > result = new FlxWaveSprite_obj();
	result->__construct(Target,Mode,__o_Strength,__o_Center,__o_Speed);
	return result;}

Dynamic FlxWaveSprite_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxWaveSprite_obj > result = new FlxWaveSprite_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4]);
	return result;}

Void FlxWaveSprite_obj::draw( ){
{
		HX_STACK_FRAME("flixel.addons.effects.FlxWaveSprite","draw",0x8c9c08d7,"flixel.addons.effects.FlxWaveSprite.draw","flixel/addons/effects/FlxWaveSprite.hx",69,0x56d0f144)
		HX_STACK_THIS(this)
		HX_STACK_LINE(70)
		if (((bool(!(this->visible)) || bool((this->alpha == (int)0))))){
			HX_STACK_LINE(71)
			return null();
		}
		HX_STACK_LINE(73)
		::openfl::geom::Rectangle _g = this->get_pixels()->get_rect();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(73)
		this->get_pixels()->fillRect(_g,(int)0);
		HX_STACK_LINE(75)
		Float offset = (int)0;		HX_STACK_VAR(offset,"offset");
		HX_STACK_LINE(76)
		{
			HX_STACK_LINE(76)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(76)
			int _g2 = this->_target->frameHeight;		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(76)
			while((true)){
				HX_STACK_LINE(76)
				if ((!(((_g1 < _g2))))){
					HX_STACK_LINE(76)
					break;
				}
				HX_STACK_LINE(76)
				int oY = (_g1)++;		HX_STACK_VAR(oY,"oY");
				HX_STACK_LINE(78)
				Float p = (int)0;		HX_STACK_VAR(p,"p");
				HX_STACK_LINE(79)
				{
					HX_STACK_LINE(79)
					::flixel::addons::effects::WaveMode _g21 = this->mode;		HX_STACK_VAR(_g21,"_g21");
					HX_STACK_LINE(79)
					switch( (int)(_g21->__Index())){
						case (int)0: {
							HX_STACK_LINE(82)
							Float _g11 = ::Math_obj::sin(((0.3 * oY) + this->_time));		HX_STACK_VAR(_g11,"_g11");
							HX_STACK_LINE(82)
							Float _g22 = (((this->strength * 0.06) * 0.06) * _g11);		HX_STACK_VAR(_g22,"_g22");
							HX_STACK_LINE(82)
							Float _g3 = (this->center * _g22);		HX_STACK_VAR(_g3,"_g3");
							HX_STACK_LINE(82)
							offset = _g3;
						}
						;break;
						case (int)2: {
							HX_STACK_LINE(85)
							if (((oY >= this->center))){
								HX_STACK_LINE(87)
								p = (oY - this->center);
								HX_STACK_LINE(88)
								Float _g4 = ::Math_obj::sin(((0.3 * p) + this->_time));		HX_STACK_VAR(_g4,"_g4");
								HX_STACK_LINE(88)
								Float _g5 = (((this->strength * 0.06) * 0.06) * _g4);		HX_STACK_VAR(_g5,"_g5");
								HX_STACK_LINE(88)
								Float _g6 = (p * _g5);		HX_STACK_VAR(_g6,"_g6");
								HX_STACK_LINE(88)
								offset = _g6;
							}
						}
						;break;
						case (int)1: {
							HX_STACK_LINE(92)
							if (((oY <= this->center))){
								HX_STACK_LINE(94)
								p = (this->center - oY);
								HX_STACK_LINE(95)
								Float _g7 = ::Math_obj::sin(((0.3 * p) + this->_time));		HX_STACK_VAR(_g7,"_g7");
								HX_STACK_LINE(95)
								Float _g8 = (((this->strength * 0.06) * 0.06) * _g7);		HX_STACK_VAR(_g8,"_g8");
								HX_STACK_LINE(95)
								Float _g9 = (p * _g8);		HX_STACK_VAR(_g9,"_g9");
								HX_STACK_LINE(95)
								offset = _g9;
							}
						}
						;break;
					}
				}
				HX_STACK_LINE(99)
				this->_flashPoint->setTo((this->strength + offset),oY);
				HX_STACK_LINE(100)
				this->_flashRect2->setTo((int)0,oY,this->_target->frameWidth,(int)1);
				HX_STACK_LINE(101)
				::openfl::display::BitmapData _g10 = this->_target->get_pixels();		HX_STACK_VAR(_g10,"_g10");
				HX_STACK_LINE(101)
				this->get_pixels()->copyPixels(_g10,this->_flashRect2,this->_flashPoint,null(),null(),null());
			}
		}
		HX_STACK_LINE(104)
		if (((this->_targetOffset == (int)-999))){
			HX_STACK_LINE(106)
			this->_targetOffset = offset;
		}
		else{
			HX_STACK_LINE(110)
			if (((offset == this->_targetOffset))){
				HX_STACK_LINE(111)
				this->_time = (int)0;
			}
		}
		HX_STACK_LINE(113)
		hx::AddEq(this->_time,(::flixel::FlxG_obj::elapsed * this->speed));
		HX_STACK_LINE(115)
		this->cachedGraphics->get_tilesheet()->destroyFrameBitmapDatas();
		HX_STACK_LINE(116)
		this->dirty = true;
		HX_STACK_LINE(117)
		this->super::draw();
	}
return null();
}


Float FlxWaveSprite_obj::calculateOffset( Float p){
	HX_STACK_FRAME("flixel.addons.effects.FlxWaveSprite","calculateOffset",0x281aafa6,"flixel.addons.effects.FlxWaveSprite.calculateOffset","flixel/addons/effects/FlxWaveSprite.hx",121,0x56d0f144)
	HX_STACK_THIS(this)
	HX_STACK_ARG(p,"p")
	HX_STACK_LINE(122)
	Float _g = ::Math_obj::sin(((0.3 * p) + this->_time));		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(122)
	return (((this->strength * 0.06) * 0.06) * _g);
}


HX_DEFINE_DYNAMIC_FUNC1(FlxWaveSprite_obj,calculateOffset,return )

Void FlxWaveSprite_obj::initPixels( ){
{
		HX_STACK_FRAME("flixel.addons.effects.FlxWaveSprite","initPixels",0x05990770,"flixel.addons.effects.FlxWaveSprite.initPixels","flixel/addons/effects/FlxWaveSprite.hx",126,0x56d0f144)
		HX_STACK_THIS(this)
		HX_STACK_LINE(127)
		this->setPosition((this->_target->x - this->strength),this->_target->y);
		HX_STACK_LINE(128)
		int _g = ::Std_obj::_int((this->_target->frameWidth + (this->strength * (int)2)));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(128)
		this->makeGraphic(_g,this->_target->frameHeight,(int)0,true,null());
		HX_STACK_LINE(129)
		this->_flashPoint->setTo(this->strength,(int)0);
		HX_STACK_LINE(130)
		::openfl::display::BitmapData _g1 = this->_target->get_pixels();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(130)
		::openfl::geom::Rectangle _g2 = this->_target->get_pixels()->get_rect();		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(130)
		this->get_pixels()->copyPixels(_g1,_g2,this->_flashPoint,null(),null(),null());
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxWaveSprite_obj,initPixels,(void))

int FlxWaveSprite_obj::set_strength( int value){
	HX_STACK_FRAME("flixel.addons.effects.FlxWaveSprite","set_strength",0x990311d1,"flixel.addons.effects.FlxWaveSprite.set_strength","flixel/addons/effects/FlxWaveSprite.hx",134,0x56d0f144)
	HX_STACK_THIS(this)
	HX_STACK_ARG(value,"value")
	HX_STACK_LINE(135)
	if (((this->strength != value))){
		HX_STACK_LINE(137)
		this->strength = value;
		HX_STACK_LINE(138)
		this->initPixels();
	}
	HX_STACK_LINE(140)
	return this->strength;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxWaveSprite_obj,set_strength,return )

Float FlxWaveSprite_obj::BASE_STRENGTH;


FlxWaveSprite_obj::FlxWaveSprite_obj()
{
}

void FlxWaveSprite_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxWaveSprite);
	HX_MARK_MEMBER_NAME(mode,"mode");
	HX_MARK_MEMBER_NAME(speed,"speed");
	HX_MARK_MEMBER_NAME(center,"center");
	HX_MARK_MEMBER_NAME(strength,"strength");
	HX_MARK_MEMBER_NAME(_target,"_target");
	HX_MARK_MEMBER_NAME(_targetOffset,"_targetOffset");
	HX_MARK_MEMBER_NAME(_time,"_time");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxWaveSprite_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(mode,"mode");
	HX_VISIT_MEMBER_NAME(speed,"speed");
	HX_VISIT_MEMBER_NAME(center,"center");
	HX_VISIT_MEMBER_NAME(strength,"strength");
	HX_VISIT_MEMBER_NAME(_target,"_target");
	HX_VISIT_MEMBER_NAME(_targetOffset,"_targetOffset");
	HX_VISIT_MEMBER_NAME(_time,"_time");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxWaveSprite_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"mode") ) { return mode; }
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { return speed; }
		if (HX_FIELD_EQ(inName,"_time") ) { return _time; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"center") ) { return center; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"_target") ) { return _target; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"strength") ) { return strength; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"initPixels") ) { return initPixels_dyn(); }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"set_strength") ) { return set_strength_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"_targetOffset") ) { return _targetOffset; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"calculateOffset") ) { return calculateOffset_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxWaveSprite_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"mode") ) { mode=inValue.Cast< ::flixel::addons::effects::WaveMode >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"speed") ) { speed=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_time") ) { _time=inValue.Cast< Float >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"center") ) { center=inValue.Cast< int >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"_target") ) { _target=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"strength") ) { if (inCallProp) return set_strength(inValue);strength=inValue.Cast< int >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"_targetOffset") ) { _targetOffset=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxWaveSprite_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("mode"));
	outFields->push(HX_CSTRING("speed"));
	outFields->push(HX_CSTRING("center"));
	outFields->push(HX_CSTRING("strength"));
	outFields->push(HX_CSTRING("_target"));
	outFields->push(HX_CSTRING("_targetOffset"));
	outFields->push(HX_CSTRING("_time"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	HX_CSTRING("BASE_STRENGTH"),
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::addons::effects::WaveMode*/ ,(int)offsetof(FlxWaveSprite_obj,mode),HX_CSTRING("mode")},
	{hx::fsFloat,(int)offsetof(FlxWaveSprite_obj,speed),HX_CSTRING("speed")},
	{hx::fsInt,(int)offsetof(FlxWaveSprite_obj,center),HX_CSTRING("center")},
	{hx::fsInt,(int)offsetof(FlxWaveSprite_obj,strength),HX_CSTRING("strength")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(FlxWaveSprite_obj,_target),HX_CSTRING("_target")},
	{hx::fsFloat,(int)offsetof(FlxWaveSprite_obj,_targetOffset),HX_CSTRING("_targetOffset")},
	{hx::fsFloat,(int)offsetof(FlxWaveSprite_obj,_time),HX_CSTRING("_time")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("mode"),
	HX_CSTRING("speed"),
	HX_CSTRING("center"),
	HX_CSTRING("strength"),
	HX_CSTRING("_target"),
	HX_CSTRING("_targetOffset"),
	HX_CSTRING("_time"),
	HX_CSTRING("draw"),
	HX_CSTRING("calculateOffset"),
	HX_CSTRING("initPixels"),
	HX_CSTRING("set_strength"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxWaveSprite_obj::__mClass,"__mClass");
	HX_MARK_MEMBER_NAME(FlxWaveSprite_obj::BASE_STRENGTH,"BASE_STRENGTH");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxWaveSprite_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(FlxWaveSprite_obj::BASE_STRENGTH,"BASE_STRENGTH");
};

#endif

Class FlxWaveSprite_obj::__mClass;

void FlxWaveSprite_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.effects.FlxWaveSprite"), hx::TCanCast< FlxWaveSprite_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxWaveSprite_obj::__boot()
{
	BASE_STRENGTH= 0.06;
}

} // end namespace flixel
} // end namespace addons
} // end namespace effects
