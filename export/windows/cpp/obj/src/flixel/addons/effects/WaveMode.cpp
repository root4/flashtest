#include <hxcpp.h>

#ifndef INCLUDED_flixel_addons_effects_WaveMode
#include <flixel/addons/effects/WaveMode.h>
#endif
namespace flixel{
namespace addons{
namespace effects{

::flixel::addons::effects::WaveMode WaveMode_obj::ALL;

::flixel::addons::effects::WaveMode WaveMode_obj::BOTTOM;

::flixel::addons::effects::WaveMode WaveMode_obj::TOP;

HX_DEFINE_CREATE_ENUM(WaveMode_obj)

int WaveMode_obj::__FindIndex(::String inName)
{
	if (inName==HX_CSTRING("ALL")) return 0;
	if (inName==HX_CSTRING("BOTTOM")) return 2;
	if (inName==HX_CSTRING("TOP")) return 1;
	return super::__FindIndex(inName);
}

int WaveMode_obj::__FindArgCount(::String inName)
{
	if (inName==HX_CSTRING("ALL")) return 0;
	if (inName==HX_CSTRING("BOTTOM")) return 0;
	if (inName==HX_CSTRING("TOP")) return 0;
	return super::__FindArgCount(inName);
}

Dynamic WaveMode_obj::__Field(const ::String &inName,bool inCallProp)
{
	if (inName==HX_CSTRING("ALL")) return ALL;
	if (inName==HX_CSTRING("BOTTOM")) return BOTTOM;
	if (inName==HX_CSTRING("TOP")) return TOP;
	return super::__Field(inName,inCallProp);
}

static ::String sStaticFields[] = {
	HX_CSTRING("ALL"),
	HX_CSTRING("TOP"),
	HX_CSTRING("BOTTOM"),
	::String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(WaveMode_obj::ALL,"ALL");
	HX_MARK_MEMBER_NAME(WaveMode_obj::BOTTOM,"BOTTOM");
	HX_MARK_MEMBER_NAME(WaveMode_obj::TOP,"TOP");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatic(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(WaveMode_obj::__mClass,"__mClass");
	HX_VISIT_MEMBER_NAME(WaveMode_obj::ALL,"ALL");
	HX_VISIT_MEMBER_NAME(WaveMode_obj::BOTTOM,"BOTTOM");
	HX_VISIT_MEMBER_NAME(WaveMode_obj::TOP,"TOP");
};
#endif

static ::String sMemberFields[] = { ::String(null()) };
Class WaveMode_obj::__mClass;

Dynamic __Create_WaveMode_obj() { return new WaveMode_obj; }

void WaveMode_obj::__register()
{

hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.effects.WaveMode"), hx::TCanCast< WaveMode_obj >,sStaticFields,sMemberFields,
	&__Create_WaveMode_obj, &__Create,
	&super::__SGetClass(), &CreateWaveMode_obj, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatic
#endif
#ifdef HXCPP_SCRIPTABLE
    , 0
#endif
);
}

void WaveMode_obj::__boot()
{
hx::Static(ALL) = hx::CreateEnum< WaveMode_obj >(HX_CSTRING("ALL"),0);
hx::Static(BOTTOM) = hx::CreateEnum< WaveMode_obj >(HX_CSTRING("BOTTOM"),2);
hx::Static(TOP) = hx::CreateEnum< WaveMode_obj >(HX_CSTRING("TOP"),1);
}


} // end namespace flixel
} // end namespace addons
} // end namespace effects
