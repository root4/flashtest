#include <hxcpp.h>

#ifndef INCLUDED_FallingTile
#include <FallingTile.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif

Void FallingTile_obj::__construct(int x,int y)
{
HX_STACK_FRAME("FallingTile","new",0x4c668d67,"FallingTile.new","FallingTile.hx",19,0x5c2e5be9)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
{
	HX_STACK_LINE(20)
	super::__construct(x,y,null());
	HX_STACK_LINE(21)
	this->loadGraphic(HX_CSTRING("assets/images/Tile.png"),false,(int)16,(int)16,true,null());
	HX_STACK_LINE(22)
	this->timer = .25;
	HX_STACK_LINE(23)
	this->falling = false;
	HX_STACK_LINE(24)
	this->set_immovable(true);
	HX_STACK_LINE(25)
	this->maxVelocity->set((int)0,(int)30);
}
;
	return null();
}

//FallingTile_obj::~FallingTile_obj() { }

Dynamic FallingTile_obj::__CreateEmpty() { return  new FallingTile_obj; }
hx::ObjectPtr< FallingTile_obj > FallingTile_obj::__new(int x,int y)
{  hx::ObjectPtr< FallingTile_obj > result = new FallingTile_obj();
	result->__construct(x,y);
	return result;}

Dynamic FallingTile_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FallingTile_obj > result = new FallingTile_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

Void FallingTile_obj::update( ){
{
		HX_STACK_FRAME("FallingTile","update",0x84276762,"FallingTile.update","FallingTile.hx",28,0x5c2e5be9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(30)
		this->velocity->set_x((int)0);
		HX_STACK_LINE(31)
		if (((bool(this->falling) && bool((this->timer <= (int)0))))){
			HX_STACK_LINE(34)
			this->set_solid(true);
			HX_STACK_LINE(35)
			this->allowCollisions = (int)4369;
			HX_STACK_LINE(36)
			this->mass = 0.9;
			HX_STACK_LINE(37)
			this->set_immovable(false);
			HX_STACK_LINE(38)
			this->acceleration->set_y((int)10);
		}
		else{
			HX_STACK_LINE(41)
			if ((this->falling)){
				HX_STACK_LINE(43)
				this->timer = (this->timer - ::flixel::FlxG_obj::elapsed);
			}
		}
		HX_STACK_LINE(45)
		this->super::update();
	}
return null();
}


Void FallingTile_obj::start( ){
{
		HX_STACK_FRAME("FallingTile","start",0x080cc129,"FallingTile.start","FallingTile.hx",48,0x5c2e5be9)
		HX_STACK_THIS(this)
		HX_STACK_LINE(48)
		this->falling = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FallingTile_obj,start,(void))


FallingTile_obj::FallingTile_obj()
{
}

Dynamic FallingTile_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"timer") ) { return timer; }
		if (HX_FIELD_EQ(inName,"start") ) { return start_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"falling") ) { return falling; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FallingTile_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"timer") ) { timer=inValue.Cast< Float >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"falling") ) { falling=inValue.Cast< bool >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FallingTile_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("timer"));
	outFields->push(HX_CSTRING("falling"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsFloat,(int)offsetof(FallingTile_obj,timer),HX_CSTRING("timer")},
	{hx::fsBool,(int)offsetof(FallingTile_obj,falling),HX_CSTRING("falling")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("timer"),
	HX_CSTRING("falling"),
	HX_CSTRING("update"),
	HX_CSTRING("start"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FallingTile_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FallingTile_obj::__mClass,"__mClass");
};

#endif

Class FallingTile_obj::__mClass;

void FallingTile_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("FallingTile"), hx::TCanCast< FallingTile_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FallingTile_obj::__boot()
{
}

