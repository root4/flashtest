#include <hxcpp.h>

#ifndef INCLUDED_SettingState
#include <SettingState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif

Void SettingState_obj::__construct()
{
HX_STACK_FRAME("SettingState","new",0x9b0f1bf3,"SettingState.new","SettingState.hx",13,0x155be2dd)
HX_STACK_THIS(this)
{
	HX_STACK_LINE(13)
	super::__construct(null());
}
;
	return null();
}

//SettingState_obj::~SettingState_obj() { }

Dynamic SettingState_obj::__CreateEmpty() { return  new SettingState_obj; }
hx::ObjectPtr< SettingState_obj > SettingState_obj::__new()
{  hx::ObjectPtr< SettingState_obj > result = new SettingState_obj();
	result->__construct();
	return result;}

Dynamic SettingState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< SettingState_obj > result = new SettingState_obj();
	result->__construct();
	return result;}


SettingState_obj::SettingState_obj()
{
}

Dynamic SettingState_obj::__Field(const ::String &inName,bool inCallProp)
{
	return super::__Field(inName,inCallProp);
}

Dynamic SettingState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	return super::__SetField(inName,inValue,inCallProp);
}

void SettingState_obj::__GetFields(Array< ::String> &outFields)
{
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo *sMemberStorageInfo = 0;
#endif

static ::String sMemberFields[] = {
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(SettingState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(SettingState_obj::__mClass,"__mClass");
};

#endif

Class SettingState_obj::__mClass;

void SettingState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("SettingState"), hx::TCanCast< SettingState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void SettingState_obj::__boot()
{
}

