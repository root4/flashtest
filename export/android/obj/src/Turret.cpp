#include <hxcpp.h>

#ifndef INCLUDED_IMap
#include <IMap.h>
#endif
#ifndef INCLUDED_Turret
#include <Turret.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimation
#include <flixel/animation/FlxAnimation.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxAnimationController
#include <flixel/animation/FlxAnimationController.h>
#endif
#ifndef INCLUDED_flixel_animation_FlxBaseAnimation
#include <flixel/animation/FlxBaseAnimation.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_haxe_ds_IntMap
#include <haxe/ds/IntMap.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif

Void Turret_obj::__construct(int x,int y,::flixel::FlxState state,::String facingRight)
{
HX_STACK_FRAME("Turret","new",0xc90c6342,"Turret.new","Turret.hx",21,0xe8cb3a6e)
HX_STACK_THIS(this)
HX_STACK_ARG(x,"x")
HX_STACK_ARG(y,"y")
HX_STACK_ARG(state,"state")
HX_STACK_ARG(facingRight,"facingRight")
{
	HX_STACK_LINE(22)
	super::__construct(x,y,null());
	HX_STACK_LINE(23)
	this->loadGraphic(HX_CSTRING("assets/images/turret.png"),true,(int)16,(int)16,true,null());
	HX_STACK_LINE(24)
	this->animation->add(HX_CSTRING("fire"),Array_obj< int >::__new().Add((int)0).Add((int)1).Add((int)2).Add((int)3).Add((int)4).Add((int)5).Add((int)6).Add((int)7),(int)5,true);
	struct _Function_1_1{
		inline static Dynamic Block( ){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Turret.hx",25,0xe8cb3a6e)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("x") , true,false);
				__result->Add(HX_CSTRING("y") , false,false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(25)
	this->_facingFlip->set((int)16,_Function_1_1::Block());
	struct _Function_1_2{
		inline static Dynamic Block( ){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Turret.hx",26,0xe8cb3a6e)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("x") , false,false);
				__result->Add(HX_CSTRING("y") , false,false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(26)
	this->_facingFlip->set((int)1,_Function_1_2::Block());
	HX_STACK_LINE(27)
	this->allowCollisions = (int)0;
	HX_STACK_LINE(29)
	Float _g = ::Math_obj::random();		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(29)
	this->startTimer = _g;
	HX_STACK_LINE(30)
	this->startTimer = (this->startTimer * (int)2);
	HX_STACK_LINE(31)
	this->fireTimer = (int)1;
	HX_STACK_LINE(32)
	this->started = false;
	HX_STACK_LINE(33)
	::flixel::group::FlxGroup _g1 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(33)
	this->bullet = _g1;
	HX_STACK_LINE(34)
	this->state = state;
	HX_STACK_LINE(36)
	if (((facingRight == HX_CSTRING("True")))){
		HX_STACK_LINE(37)
		this->set_facing((int)16);
	}
	else{
		HX_STACK_LINE(39)
		this->set_facing((int)1);
	}
	HX_STACK_LINE(41)
	::haxe::Log_obj::trace(this->facing,hx::SourceInfo(HX_CSTRING("Turret.hx"),41,HX_CSTRING("Turret"),HX_CSTRING("new")));
}
;
	return null();
}

//Turret_obj::~Turret_obj() { }

Dynamic Turret_obj::__CreateEmpty() { return  new Turret_obj; }
hx::ObjectPtr< Turret_obj > Turret_obj::__new(int x,int y,::flixel::FlxState state,::String facingRight)
{  hx::ObjectPtr< Turret_obj > result = new Turret_obj();
	result->__construct(x,y,state,facingRight);
	return result;}

Dynamic Turret_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< Turret_obj > result = new Turret_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3]);
	return result;}

Void Turret_obj::update( ){
{
		HX_STACK_FRAME("Turret","update",0x17d66c67,"Turret.update","Turret.hx",44,0xe8cb3a6e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(45)
		this->fireTimer = (this->fireTimer - ::flixel::FlxG_obj::elapsed);
		HX_STACK_LINE(46)
		if ((!(this->started))){
			HX_STACK_LINE(47)
			if (((this->startTimer >= (int)0))){
				HX_STACK_LINE(48)
				this->startTimer = (this->startTimer - ::flixel::FlxG_obj::elapsed);
			}
			else{
				HX_STACK_LINE(50)
				if (((this->startTimer <= (int)0))){
					HX_STACK_LINE(52)
					this->start();
				}
			}
		}
		else{
			struct _Function_2_1{
				inline static ::flixel::animation::FlxAnimation Block( hx::ObjectPtr< ::Turret_obj > __this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","Turret.hx",56,0xe8cb3a6e)
					{
						HX_STACK_LINE(56)
						::flixel::animation::FlxAnimationController _this = __this->animation;		HX_STACK_VAR(_this,"_this");
						HX_STACK_LINE(56)
						::flixel::animation::FlxAnimation anim = null();		HX_STACK_VAR(anim,"anim");
						HX_STACK_LINE(56)
						if (((bool((bool((_this->_curAnim != null())) && bool((_this->_curAnim->delay > (int)0)))) && bool(((bool(_this->_curAnim->looped) || bool(!(_this->_curAnim->finished)))))))){
							HX_STACK_LINE(56)
							anim = _this->_curAnim;
						}
						HX_STACK_LINE(56)
						return anim;
					}
					return null();
				}
			};
			HX_STACK_LINE(56)
			if (((bool(((_Function_2_1::Block(this))->curIndex == (int)1)) && bool((this->fireTimer <= (int)0))))){
				HX_STACK_LINE(58)
				this->fireTimer = (int)1;
				HX_STACK_LINE(59)
				this->fire();
			}
		}
		HX_STACK_LINE(63)
		this->bullet->update();
		HX_STACK_LINE(64)
		this->super::update();
	}
return null();
}


Void Turret_obj::start( ){
{
		HX_STACK_FRAME("Turret","start",0x5a3803c4,"Turret.start","Turret.hx",66,0xe8cb3a6e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(67)
		this->animation->play(HX_CSTRING("fire"),null(),null());
		HX_STACK_LINE(68)
		this->started = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Turret_obj,start,(void))

Void Turret_obj::fire( ){
{
		HX_STACK_FRAME("Turret","fire",0x1c83c694,"Turret.fire","Turret.hx",72,0xe8cb3a6e)
		HX_STACK_THIS(this)
		HX_STACK_LINE(73)
		::flixel::FlxSprite sprite;		HX_STACK_VAR(sprite,"sprite");
		HX_STACK_LINE(74)
		::flixel::FlxSprite _g = ::flixel::FlxSprite_obj::__new(this->x,(this->y + (int)8),null());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(74)
		sprite = _g;
		HX_STACK_LINE(75)
		sprite->makeGraphic((int)3,(int)1,(int)-16744320,null(),null());
		HX_STACK_LINE(76)
		if (((this->facing == (int)16))){
			HX_STACK_LINE(78)
			sprite->velocity->set_x((int)100);
			HX_STACK_LINE(79)
			sprite->setPosition((this->x + (int)16),(this->y + (int)8));
		}
		else{
			HX_STACK_LINE(82)
			sprite->velocity->set_x((int)-100);
		}
		HX_STACK_LINE(86)
		sprite->allowCollisions = (int)4369;
		HX_STACK_LINE(87)
		this->bullet->add(sprite);
		HX_STACK_LINE(88)
		this->state->add(this->bullet);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(Turret_obj,fire,(void))

::flixel::group::FlxGroup Turret_obj::getBullets( ){
	HX_STACK_FRAME("Turret","getBullets",0xfb375b59,"Turret.getBullets","Turret.hx",92,0xe8cb3a6e)
	HX_STACK_THIS(this)
	HX_STACK_LINE(92)
	return this->bullet;
}


HX_DEFINE_DYNAMIC_FUNC0(Turret_obj,getBullets,return )


Turret_obj::Turret_obj()
{
}

void Turret_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(Turret);
	HX_MARK_MEMBER_NAME(startTimer,"startTimer");
	HX_MARK_MEMBER_NAME(started,"started");
	HX_MARK_MEMBER_NAME(bullet,"bullet");
	HX_MARK_MEMBER_NAME(state,"state");
	HX_MARK_MEMBER_NAME(fireTimer,"fireTimer");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void Turret_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(startTimer,"startTimer");
	HX_VISIT_MEMBER_NAME(started,"started");
	HX_VISIT_MEMBER_NAME(bullet,"bullet");
	HX_VISIT_MEMBER_NAME(state,"state");
	HX_VISIT_MEMBER_NAME(fireTimer,"fireTimer");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic Turret_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"fire") ) { return fire_dyn(); }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"state") ) { return state; }
		if (HX_FIELD_EQ(inName,"start") ) { return start_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"bullet") ) { return bullet; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"started") ) { return started; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"fireTimer") ) { return fireTimer; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"startTimer") ) { return startTimer; }
		if (HX_FIELD_EQ(inName,"getBullets") ) { return getBullets_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic Turret_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 5:
		if (HX_FIELD_EQ(inName,"state") ) { state=inValue.Cast< ::flixel::FlxState >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"bullet") ) { bullet=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"started") ) { started=inValue.Cast< bool >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"fireTimer") ) { fireTimer=inValue.Cast< Float >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"startTimer") ) { startTimer=inValue.Cast< Float >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void Turret_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("startTimer"));
	outFields->push(HX_CSTRING("started"));
	outFields->push(HX_CSTRING("bullet"));
	outFields->push(HX_CSTRING("state"));
	outFields->push(HX_CSTRING("fireTimer"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsFloat,(int)offsetof(Turret_obj,startTimer),HX_CSTRING("startTimer")},
	{hx::fsBool,(int)offsetof(Turret_obj,started),HX_CSTRING("started")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(Turret_obj,bullet),HX_CSTRING("bullet")},
	{hx::fsObject /*::flixel::FlxState*/ ,(int)offsetof(Turret_obj,state),HX_CSTRING("state")},
	{hx::fsFloat,(int)offsetof(Turret_obj,fireTimer),HX_CSTRING("fireTimer")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("startTimer"),
	HX_CSTRING("started"),
	HX_CSTRING("bullet"),
	HX_CSTRING("state"),
	HX_CSTRING("fireTimer"),
	HX_CSTRING("update"),
	HX_CSTRING("start"),
	HX_CSTRING("fire"),
	HX_CSTRING("getBullets"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(Turret_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(Turret_obj::__mClass,"__mClass");
};

#endif

Class Turret_obj::__mClass;

void Turret_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("Turret"), hx::TCanCast< Turret_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void Turret_obj::__boot()
{
}

