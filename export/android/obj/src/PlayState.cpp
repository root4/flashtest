#include <hxcpp.h>

#ifndef INCLUDED_FallingTile
#include <FallingTile.h>
#endif
#ifndef INCLUDED_PlayState
#include <PlayState.h>
#endif
#ifndef INCLUDED_Player
#include <Player.h>
#endif
#ifndef INCLUDED_Spikes
#include <Spikes.h>
#endif
#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_Turret
#include <Turret.h>
#endif
#ifndef INCLUDED_Xml
#include <Xml.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxCamera
#include <flixel/FlxCamera.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_addons_display_FlxStarField2D
#include <flixel/addons/display/FlxStarField2D.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStarField
#include <flixel/addons/display/_FlxStarField/FlxStarField.h>
#endif
#ifndef INCLUDED_flixel_addons_editors_ogmo_FlxOgmoLoader
#include <flixel/addons/editors/ogmo/FlxOgmoLoader.h>
#endif
#ifndef INCLUDED_flixel_effects_particles_FlxEmitter
#include <flixel/effects/particles/FlxEmitter.h>
#endif
#ifndef INCLUDED_flixel_effects_particles_FlxParticle
#include <flixel/effects/particles/FlxParticle.h>
#endif
#ifndef INCLUDED_flixel_effects_particles_FlxTypedEmitter
#include <flixel/effects/particles/FlxTypedEmitter.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_input_touch_FlxTouch
#include <flixel/input/touch/FlxTouch.h>
#endif
#ifndef INCLUDED_flixel_input_touch_FlxTouchManager
#include <flixel/input/touch/FlxTouchManager.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxBasic
#include <flixel/interfaces/IFlxBasic.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxInput
#include <flixel/interfaces/IFlxInput.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxParticle
#include <flixel/interfaces/IFlxParticle.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxSprite
#include <flixel/interfaces/IFlxSprite.h>
#endif
#ifndef INCLUDED_flixel_text_FlxText
#include <flixel/text/FlxText.h>
#endif
#ifndef INCLUDED_flixel_tile_FlxTilemap
#include <flixel/tile/FlxTilemap.h>
#endif
#ifndef INCLUDED_flixel_util_FlxCollision
#include <flixel/util/FlxCollision.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRect
#include <flixel/util/FlxRect.h>
#endif
#ifndef INCLUDED_flixel_util_FlxTimer
#include <flixel/util/FlxTimer.h>
#endif
#ifndef INCLUDED_haxe_Log
#include <haxe/Log.h>
#endif
#ifndef INCLUDED_openfl_display_BlendMode
#include <openfl/display/BlendMode.h>
#endif

Void PlayState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("PlayState","new",0xf8bf96cf,"PlayState.new","PlayState.hx",32,0xb30d7781)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(32)
	super::__construct(MaxSize);
}
;
	return null();
}

//PlayState_obj::~PlayState_obj() { }

Dynamic PlayState_obj::__CreateEmpty() { return  new PlayState_obj; }
hx::ObjectPtr< PlayState_obj > PlayState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< PlayState_obj > result = new PlayState_obj();
	result->__construct(MaxSize);
	return result;}

Dynamic PlayState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< PlayState_obj > result = new PlayState_obj();
	result->__construct(inArgs[0]);
	return result;}

Void PlayState_obj::create( ){
{
		HX_STACK_FRAME("PlayState","create",0x82220fed,"PlayState.create","PlayState.hx",71,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(72)
		this->super::create();
		HX_STACK_LINE(73)
		::flixel::addons::editors::ogmo::FlxOgmoLoader _g = ::flixel::addons::editors::ogmo::FlxOgmoLoader_obj::__new(HX_CSTRING("assets/data/Level1.oel"));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(73)
		this->loader = _g;
		HX_STACK_LINE(74)
		::flixel::tile::FlxTilemap _g1 = this->loader->loadTilemap(HX_CSTRING("assets/images/pickleTile.png"),(int)16,(int)16,HX_CSTRING("ground"));		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(74)
		this->midgroundMap = _g1;
		HX_STACK_LINE(75)
		::flixel::tile::FlxTilemap _g2 = this->loader->loadTilemap(HX_CSTRING("assets/images/ships.png"),(int)32,(int)16,HX_CSTRING("background"));		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(75)
		this->backgroundMap = _g2;
		HX_STACK_LINE(76)
		this->backgroundMap->allowCollisions = (int)0;
		HX_STACK_LINE(77)
		::flixel::tile::FlxTilemap _g3 = this->loader->loadTilemap(HX_CSTRING("assets/images/pickleTile.png"),(int)16,(int)16,HX_CSTRING("foreground"));		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(77)
		this->foregroundMap = _g3;
		HX_STACK_LINE(78)
		this->foregroundMap->allowCollisions = (int)0;
		HX_STACK_LINE(79)
		::flixel::group::FlxGroup _g4 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(79)
		this->tileGroup = _g4;
		HX_STACK_LINE(80)
		Array< ::Dynamic > _g5 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(80)
		this->turretGroup = _g5;
		HX_STACK_LINE(81)
		::flixel::group::FlxGroup _g6 = ::flixel::group::FlxGroup_obj::__new(null());		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(81)
		this->spikeGroup = _g6;
		HX_STACK_LINE(83)
		::Player _g7 = ::Player_obj::__new((int)20,(int)20);		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(83)
		this->player = _g7;
		HX_STACK_LINE(85)
		this->loader->loadEntities(this->placeTiles_dyn(),HX_CSTRING("entities"));
		HX_STACK_LINE(86)
		::flixel::effects::particles::FlxEmitter _g8 = ::flixel::effects::particles::FlxEmitter_obj::__new(this->player->x,this->player->y,null());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(86)
		this->particleEmitter = _g8;
		HX_STACK_LINE(87)
		this->particleEmitter->acceleration->set_y((int)200);
		HX_STACK_LINE(88)
		this->particleEmitter->bounce = .3;
		HX_STACK_LINE(90)
		{
			HX_STACK_LINE(90)
			int _g9 = (int)0;		HX_STACK_VAR(_g9,"_g9");
			HX_STACK_LINE(90)
			while((true)){
				HX_STACK_LINE(90)
				if ((!(((_g9 < (int)50))))){
					HX_STACK_LINE(90)
					break;
				}
				HX_STACK_LINE(90)
				int i = (_g9)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(92)
				::flixel::effects::particles::FlxParticle _g91 = ::flixel::effects::particles::FlxParticle_obj::__new();		HX_STACK_VAR(_g91,"_g91");
				HX_STACK_LINE(92)
				this->particle = _g91;
				HX_STACK_LINE(93)
				this->particle->makeGraphic((int)1,(int)1,(int)-65536,null(),null());
				HX_STACK_LINE(94)
				this->particle->allowCollisions = (int)4369;
				HX_STACK_LINE(96)
				this->particleEmitter->add(this->particle);
			}
		}
		HX_STACK_LINE(100)
		this->score = (int)0;
		HX_STACK_LINE(101)
		::flixel::text::FlxText _g10 = ::flixel::text::FlxText_obj::__new((int)0,(int)0,(int)100,(HX_CSTRING("Score: ") + this->score),null(),null());		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(101)
		this->scoreText = _g10;
		HX_STACK_LINE(102)
		this->add(this->scoreText);
		HX_STACK_LINE(104)
		::flixel::FlxG_obj::camera->bgColor = (int)-12105913;
		HX_STACK_LINE(118)
		Float _g11 = this->midgroundMap->get_width();		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(118)
		int _g12 = ::Std_obj::_int(_g11);		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(118)
		Float _g13 = this->midgroundMap->get_height();		HX_STACK_VAR(_g13,"_g13");
		HX_STACK_LINE(118)
		Float _g14 = (_g13 + (int)64);		HX_STACK_VAR(_g14,"_g14");
		HX_STACK_LINE(118)
		int _g15 = ::Std_obj::_int(_g14);		HX_STACK_VAR(_g15,"_g15");
		HX_STACK_LINE(118)
		::flixel::addons::display::FlxStarField2D _g16 = ::flixel::addons::display::FlxStarField2D_obj::__new((int)0,(int)0,_g12,_g15,null());		HX_STACK_VAR(_g16,"_g16");
		HX_STACK_LINE(118)
		this->star = _g16;
		HX_STACK_LINE(120)
		this->star->setStarSpeed((int)25,(int)100);
		HX_STACK_LINE(124)
		::flixel::FlxSprite _g17 = ::flixel::FlxSprite_obj::__new((int)0,(int)0,null());		HX_STACK_VAR(_g17,"_g17");
		HX_STACK_LINE(124)
		this->darkness = _g17;
		HX_STACK_LINE(125)
		Float _g18 = this->midgroundMap->get_width();		HX_STACK_VAR(_g18,"_g18");
		HX_STACK_LINE(125)
		int _g19 = ::Std_obj::_int(_g18);		HX_STACK_VAR(_g19,"_g19");
		HX_STACK_LINE(125)
		Float _g20 = this->midgroundMap->get_height();		HX_STACK_VAR(_g20,"_g20");
		HX_STACK_LINE(125)
		Float _g21 = (_g20 + (int)64);		HX_STACK_VAR(_g21,"_g21");
		HX_STACK_LINE(125)
		int _g22 = ::Std_obj::_int(_g21);		HX_STACK_VAR(_g22,"_g22");
		HX_STACK_LINE(125)
		this->darkness->makeGraphic(_g19,_g22,(int)1996488704,true,null());
		HX_STACK_LINE(126)
		this->darkness->set_blend(::openfl::display::BlendMode_obj::MULTIPLY);
		HX_STACK_LINE(127)
		::flixel::FlxSprite _g23 = ::flixel::FlxSprite_obj::__new(this->player->x,this->player->y,null());		HX_STACK_VAR(_g23,"_g23");
		HX_STACK_LINE(127)
		this->light = _g23;
		HX_STACK_LINE(128)
		this->light->loadGraphic(HX_CSTRING("assets/images/light.png"),null(),null(),null(),null(),null());
		HX_STACK_LINE(129)
		this->light->set_blend(::openfl::display::BlendMode_obj::SCREEN);
		HX_STACK_LINE(132)
		::flixel::FlxSprite _g24 = ::flixel::FlxSprite_obj::__new((int)100,(int)180,null());		HX_STACK_VAR(_g24,"_g24");
		HX_STACK_LINE(132)
		this->right = _g24;
		HX_STACK_LINE(133)
		this->right->loadGraphic(HX_CSTRING("assets/images/right.png"),false,(int)75,(int)61,null(),null());
		HX_STACK_LINE(134)
		this->right->set_visible(true);
		HX_STACK_LINE(135)
		this->right->set_alpha(.5);
		HX_STACK_LINE(136)
		this->right->scrollFactor->set((int)0,(int)0);
		HX_STACK_LINE(139)
		::flixel::FlxSprite _g25 = ::flixel::FlxSprite_obj::__new((int)20,(int)180,null());		HX_STACK_VAR(_g25,"_g25");
		HX_STACK_LINE(139)
		this->left = _g25;
		HX_STACK_LINE(140)
		this->left->loadGraphic(HX_CSTRING("assets/images/left.png"),false,(int)75,(int)61,null(),null());
		HX_STACK_LINE(141)
		this->left->set_visible(true);
		HX_STACK_LINE(142)
		this->left->set_alpha(.5);
		HX_STACK_LINE(143)
		this->left->scrollFactor->set((int)0,(int)0);
		HX_STACK_LINE(146)
		::flixel::FlxSprite _g26 = ::flixel::FlxSprite_obj::__new((int)325,(int)180,null());		HX_STACK_VAR(_g26,"_g26");
		HX_STACK_LINE(146)
		this->up = _g26;
		HX_STACK_LINE(147)
		this->up->loadGraphic(HX_CSTRING("assets/images/up.png"),false,(int)80,(int)80,null(),null());
		HX_STACK_LINE(148)
		this->up->set_visible(true);
		HX_STACK_LINE(149)
		this->up->scale->set(.8,.8);
		HX_STACK_LINE(150)
		this->up->set_alpha(.5);
		HX_STACK_LINE(151)
		this->up->updateHitbox();
		HX_STACK_LINE(152)
		this->up->scrollFactor->set((int)0,(int)0);
		HX_STACK_LINE(154)
		::flixel::text::FlxText _g27 = ::flixel::text::FlxText_obj::__new((int)0,(int)0,(int)70,HX_CSTRING("Test!"),null(),null());		HX_STACK_VAR(_g27,"_g27");
		HX_STACK_LINE(154)
		this->touchL = _g27;
		HX_STACK_LINE(155)
		this->touchL->scrollFactor->set((int)0,(int)0);
		HX_STACK_LINE(157)
		::flixel::text::FlxText _g28 = ::flixel::text::FlxText_obj::__new((int)0,(int)50,(int)70,HX_CSTRING("Test!@"),null(),null());		HX_STACK_VAR(_g28,"_g28");
		HX_STACK_LINE(157)
		this->touchR = _g28;
		HX_STACK_LINE(158)
		this->touchR->scrollFactor->set((int)0,(int)0);
		HX_STACK_LINE(160)
		this->backgroundMap->set_pixelPerfectRender(false);
		HX_STACK_LINE(161)
		this->backgroundMap->scrollFactor->set(.6,(int)1);
		HX_STACK_LINE(163)
		this->add(this->star);
		HX_STACK_LINE(164)
		this->add(this->backgroundMap);
		HX_STACK_LINE(165)
		this->add(this->darkness);
		HX_STACK_LINE(166)
		this->add(this->spikeGroup);
		HX_STACK_LINE(167)
		this->add(this->midgroundMap);
		HX_STACK_LINE(168)
		this->add(this->player);
		HX_STACK_LINE(169)
		this->add(this->particleEmitter);
		HX_STACK_LINE(171)
		this->add(this->right);
		HX_STACK_LINE(172)
		this->add(this->left);
		HX_STACK_LINE(173)
		this->add(this->up);
		HX_STACK_LINE(174)
		this->add(this->touchL);
		HX_STACK_LINE(175)
		this->add(this->touchR);
		HX_STACK_LINE(176)
		this->add(this->foregroundMap);
		HX_STACK_LINE(177)
		{
			HX_STACK_LINE(177)
			int _g110 = (int)0;		HX_STACK_VAR(_g110,"_g110");
			HX_STACK_LINE(177)
			int _g9 = this->turretGroup->length;		HX_STACK_VAR(_g9,"_g9");
			HX_STACK_LINE(177)
			while((true)){
				HX_STACK_LINE(177)
				if ((!(((_g110 < _g9))))){
					HX_STACK_LINE(177)
					break;
				}
				HX_STACK_LINE(177)
				int i = (_g110)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(179)
				this->add(this->turretGroup->__get(i).StaticCast< ::Turret >());
			}
		}
		HX_STACK_LINE(181)
		this->add(this->tileGroup);
	}
return null();
}


Void PlayState_obj::moveUp( ){
{
		HX_STACK_FRAME("PlayState","moveUp",0xd2bf7cbd,"PlayState.moveUp","PlayState.hx",196,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(196)
		this->player->jump = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,moveUp,(void))

Void PlayState_obj::moveLeft( ){
{
		HX_STACK_FRAME("PlayState","moveLeft",0x992afae9,"PlayState.moveLeft","PlayState.hx",198,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(198)
		this->player->moveL = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,moveLeft,(void))

Void PlayState_obj::moveRight( ){
{
		HX_STACK_FRAME("PlayState","moveRight",0xe37d9eba,"PlayState.moveRight","PlayState.hx",200,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(200)
		this->player->moveR = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,moveRight,(void))

Void PlayState_obj::destroy( ){
{
		HX_STACK_FRAME("PlayState","destroy",0x6ec756e9,"PlayState.destroy","PlayState.hx",208,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(208)
		this->super::destroy();
	}
return null();
}


Void PlayState_obj::update( ){
{
		HX_STACK_FRAME("PlayState","update",0x8d182efa,"PlayState.update","PlayState.hx",215,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(216)
		this->right->updateHitbox_dyn();
		HX_STACK_LINE(217)
		this->left->updateHitbox_dyn();
		HX_STACK_LINE(218)
		this->up->updateHitbox_dyn();
		HX_STACK_LINE(219)
		this->player->getInput(hx::ObjectPtr<OBJ_>(this));
		HX_STACK_LINE(220)
		::flixel::FlxG_obj::camera->follow(this->player,(int)0,null(),null());
		HX_STACK_LINE(222)
		{
			HX_STACK_LINE(222)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(222)
			int _g = this->turretGroup->length;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(222)
			while((true)){
				HX_STACK_LINE(222)
				if ((!(((_g1 < _g))))){
					HX_STACK_LINE(222)
					break;
				}
				HX_STACK_LINE(222)
				int i = (_g1)++;		HX_STACK_VAR(i,"i");
				HX_STACK_LINE(224)
				::flixel::FlxG_obj::overlap(this->turretGroup->__get(i).StaticCast< ::Turret >(),this->player,null(),::flixel::FlxObject_obj::separate_dyn());
				HX_STACK_LINE(225)
				::flixel::group::FlxGroup _g2 = this->turretGroup->__get(i).StaticCast< ::Turret >()->getBullets();		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(225)
				::flixel::FlxG_obj::overlap(_g2,this->player,this->hitBullet_dyn(),null());
				HX_STACK_LINE(226)
				{
					HX_STACK_LINE(226)
					::flixel::FlxBasic ObjectOrGroup1 = this->turretGroup->__get(i).StaticCast< ::Turret >()->getBullets();		HX_STACK_VAR(ObjectOrGroup1,"ObjectOrGroup1");
					HX_STACK_LINE(226)
					::flixel::FlxG_obj::overlap(ObjectOrGroup1,this->midgroundMap,this->removeBullet_dyn(),::flixel::FlxObject_obj::separate_dyn());
				}
			}
		}
		HX_STACK_LINE(229)
		bool _g5;		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(229)
		{
			HX_STACK_LINE(229)
			::Player _this = this->player;		HX_STACK_VAR(_this,"_this");
			HX_STACK_LINE(229)
			Float _g1 = _this->get_width();		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(229)
			Float _g2 = (_this->x + _g1);		HX_STACK_VAR(_g2,"_g2");
			struct _Function_2_1{
				inline static Float Block( ){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",229,0xb30d7781)
					{
						HX_STACK_LINE(229)
						::flixel::util::FlxRect _this1 = ::flixel::FlxG_obj::worldBounds;		HX_STACK_VAR(_this1,"_this1");
						HX_STACK_LINE(229)
						return (_this1->x + _this1->width);
					}
					return null();
				}
			};
			struct _Function_2_2{
				inline static bool Block( ::flixel::FlxObject _this){
					HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",229,0xb30d7781)
					{
						HX_STACK_LINE(229)
						Float _g3 = _this->get_height();		HX_STACK_VAR(_g3,"_g3");
						HX_STACK_LINE(229)
						Float _g4 = (_this->y + _g3);		HX_STACK_VAR(_g4,"_g4");
						HX_STACK_LINE(229)
						return (_g4 > ::flixel::FlxG_obj::worldBounds->y);
					}
					return null();
				}
			};
			HX_STACK_LINE(229)
			if (((  (((  (((_g2 > ::flixel::FlxG_obj::worldBounds->x))) ? bool((_this->x < _Function_2_1::Block())) : bool(false) ))) ? bool(_Function_2_2::Block(_this)) : bool(false) ))){
				struct _Function_3_1{
					inline static Float Block( ){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","PlayState.hx",229,0xb30d7781)
						{
							HX_STACK_LINE(229)
							::flixel::util::FlxRect _this1 = ::flixel::FlxG_obj::worldBounds;		HX_STACK_VAR(_this1,"_this1");
							HX_STACK_LINE(229)
							return (_this1->y + _this1->height);
						}
						return null();
					}
				};
				HX_STACK_LINE(229)
				_g5 = (_this->y < _Function_3_1::Block());
			}
			else{
				HX_STACK_LINE(229)
				_g5 = false;
			}
		}
		HX_STACK_LINE(229)
		if (((  (((_g5 == false))) ? bool((this->player->isAlive == true)) : bool(false) ))){
			HX_STACK_LINE(231)
			this->killPlayer();
		}
		HX_STACK_LINE(234)
		::flixel::FlxG_obj::overlap(this->particleEmitter,this->midgroundMap,null(),::flixel::FlxObject_obj::separate_dyn());
		HX_STACK_LINE(235)
		::flixel::FlxG_obj::overlap(this->midgroundMap,this->player,null(),::flixel::FlxObject_obj::separate_dyn());
		HX_STACK_LINE(238)
		if ((::flixel::FlxG_obj::overlap(this->tileGroup,this->player,this->touchTile_dyn(),::flixel::FlxObject_obj::separate_dyn()))){
			HX_STACK_LINE(239)
			::flixel::FlxG_obj::camera->shake(0.002,null(),null(),null(),null());
		}
		HX_STACK_LINE(242)
		::flixel::FlxG_obj::overlap(this->spikeGroup,this->player,this->activateSpikes_dyn(),null());
		HX_STACK_LINE(245)
		this->touchR->set_text(HX_CSTRING("no"));
		HX_STACK_LINE(246)
		this->touchL->set_text(HX_CSTRING("no"));
		HX_STACK_LINE(248)
		{
			HX_STACK_LINE(248)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(248)
			Array< ::Dynamic > _g1 = ::flixel::FlxG_obj::touches->list;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(248)
			while((true)){
				HX_STACK_LINE(248)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(248)
					break;
				}
				HX_STACK_LINE(248)
				::flixel::input::touch::FlxTouch touch = _g1->__get(_g).StaticCast< ::flixel::input::touch::FlxTouch >();		HX_STACK_VAR(touch,"touch");
				HX_STACK_LINE(248)
				++(_g);
				HX_STACK_LINE(250)
				if ((touch->overlaps(this->right,null()))){
					HX_STACK_LINE(251)
					this->touchR->set_text(HX_CSTRING("Right"));
				}
				HX_STACK_LINE(255)
				if ((touch->overlaps(this->left,null()))){
					HX_STACK_LINE(256)
					this->touchL->set_text(HX_CSTRING("Left"));
				}
			}
		}
		HX_STACK_LINE(263)
		this->super::update();
		HX_STACK_LINE(265)
		::flixel::FlxG_obj::touches->reset_dyn();
	}
return null();
}


Void PlayState_obj::killPlayer( ){
{
		HX_STACK_FRAME("PlayState","killPlayer",0x88e03f10,"PlayState.killPlayer","PlayState.hx",270,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_LINE(271)
		::haxe::Log_obj::trace(HX_CSTRING("dead"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),271,HX_CSTRING("PlayState"),HX_CSTRING("killPlayer")));
		HX_STACK_LINE(272)
		if ((this->player->isAlive)){
			HX_STACK_LINE(274)
			this->remove(this->player,true);
			HX_STACK_LINE(275)
			this->player->isAlive = false;
			HX_STACK_LINE(276)
			{
				HX_STACK_LINE(276)
				::flixel::effects::particles::FlxEmitter _this = this->particleEmitter;		HX_STACK_VAR(_this,"_this");
				HX_STACK_LINE(276)
				_this->set_x(this->player->x);
				HX_STACK_LINE(276)
				_this->set_y(this->player->y);
			}
			HX_STACK_LINE(277)
			this->particleEmitter->start(true,(int)3,0.1,(int)50,(int)4);
			HX_STACK_LINE(279)
			::haxe::Log_obj::trace(this->player->isAlive,hx::SourceInfo(HX_CSTRING("PlayState.hx"),279,HX_CSTRING("PlayState"),HX_CSTRING("killPlayer")));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(PlayState_obj,killPlayer,(void))

Void PlayState_obj::activateSpikes( ::Spikes thisSpike,::Player thisPlayer){
{
		HX_STACK_FRAME("PlayState","activateSpikes",0xa22bc1f1,"PlayState.activateSpikes","PlayState.hx",285,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(thisSpike,"thisSpike")
		HX_STACK_ARG(thisPlayer,"thisPlayer")
		HX_STACK_LINE(285)
		if ((this->player->isAlive)){
			HX_STACK_LINE(287)
			thisSpike->start();
			HX_STACK_LINE(288)
			if ((::flixel::util::FlxCollision_obj::pixelPerfectCheck(thisSpike,thisPlayer,null(),null()))){
				HX_STACK_LINE(290)
				::haxe::Log_obj::trace(HX_CSTRING("Dead"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),290,HX_CSTRING("PlayState"),HX_CSTRING("activateSpikes")));
				HX_STACK_LINE(292)
				this->remove(this->player,false);
				HX_STACK_LINE(293)
				this->player->isAlive = false;
				HX_STACK_LINE(294)
				{
					HX_STACK_LINE(294)
					::flixel::effects::particles::FlxEmitter _this = this->particleEmitter;		HX_STACK_VAR(_this,"_this");
					HX_STACK_LINE(294)
					_this->set_x(this->player->x);
					HX_STACK_LINE(294)
					_this->set_y(this->player->y);
				}
				HX_STACK_LINE(295)
				this->particleEmitter->start(true,(int)3,0.1,(int)50,(int)4);
				HX_STACK_LINE(297)
				::haxe::Log_obj::trace(this->player->isAlive,hx::SourceInfo(HX_CSTRING("PlayState.hx"),297,HX_CSTRING("PlayState"),HX_CSTRING("activateSpikes")));
			}
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(PlayState_obj,activateSpikes,(void))

Void PlayState_obj::placeTiles( ::String entityName,::Xml entityData){
{
		HX_STACK_FRAME("PlayState","placeTiles",0x70dd3caf,"PlayState.placeTiles","PlayState.hx",307,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(entityName,"entityName")
		HX_STACK_ARG(entityData,"entityData")
		HX_STACK_LINE(309)
		::String _g = entityData->get(HX_CSTRING("x"));		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(309)
		int x = ::Std_obj::parseInt(_g);		HX_STACK_VAR(x,"x");
		HX_STACK_LINE(310)
		::String _g1 = entityData->get(HX_CSTRING("y"));		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(310)
		int y = ::Std_obj::parseInt(_g1);		HX_STACK_VAR(y,"y");
		HX_STACK_LINE(311)
		if (((entityName == HX_CSTRING("falling")))){
			HX_STACK_LINE(312)
			::FallingTile _g2 = ::FallingTile_obj::__new(x,y);		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(312)
			this->tileGroup->add(_g2);
		}
		HX_STACK_LINE(314)
		if (((entityName == HX_CSTRING("turret")))){
			HX_STACK_LINE(316)
			::haxe::Log_obj::trace(HX_CSTRING("turret"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),316,HX_CSTRING("PlayState"),HX_CSTRING("placeTiles")));
			HX_STACK_LINE(317)
			::String _g3 = entityData->get(HX_CSTRING("facingRight"));		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(317)
			::Turret _g4 = ::Turret_obj::__new(x,y,hx::ObjectPtr<OBJ_>(this),_g3);		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(317)
			this->turretGroup->push(_g4);
		}
		HX_STACK_LINE(319)
		if (((entityName == HX_CSTRING("spike")))){
			HX_STACK_LINE(321)
			::haxe::Log_obj::trace(HX_CSTRING("spike"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),321,HX_CSTRING("PlayState"),HX_CSTRING("placeTiles")));
			HX_STACK_LINE(322)
			::Spikes _g5 = ::Spikes_obj::__new(x,y);		HX_STACK_VAR(_g5,"_g5");
			HX_STACK_LINE(322)
			this->spikeGroup->add(_g5);
		}
		HX_STACK_LINE(324)
		if (((entityName == HX_CSTRING("player")))){
			HX_STACK_LINE(326)
			::haxe::Log_obj::trace(HX_CSTRING("player"),hx::SourceInfo(HX_CSTRING("PlayState.hx"),326,HX_CSTRING("PlayState"),HX_CSTRING("placeTiles")));
			HX_STACK_LINE(327)
			this->player->setPosition(x,y);
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(PlayState_obj,placeTiles,(void))

Void PlayState_obj::removeBullet( ::flixel::FlxObject thisBullet,::flixel::FlxObject thisPlayer){
{
		HX_STACK_FRAME("PlayState","removeBullet",0x5ef3a157,"PlayState.removeBullet","PlayState.hx",335,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(thisBullet,"thisBullet")
		HX_STACK_ARG(thisPlayer,"thisPlayer")
		HX_STACK_LINE(335)
		thisBullet->kill();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(PlayState_obj,removeBullet,(void))

Void PlayState_obj::hitBullet( ::flixel::FlxObject thisBullet,::Player thisPlayer){
{
		HX_STACK_FRAME("PlayState","hitBullet",0x44f510a4,"PlayState.hitBullet","PlayState.hx",339,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(thisBullet,"thisBullet")
		HX_STACK_ARG(thisPlayer,"thisPlayer")
		HX_STACK_LINE(340)
		thisBullet->kill();
		HX_STACK_LINE(341)
		if ((thisPlayer->isAlive)){
			HX_STACK_LINE(343)
			this->remove(thisPlayer,true);
			HX_STACK_LINE(344)
			thisPlayer->isAlive = false;
			HX_STACK_LINE(345)
			{
				HX_STACK_LINE(345)
				::flixel::effects::particles::FlxEmitter _this = this->particleEmitter;		HX_STACK_VAR(_this,"_this");
				HX_STACK_LINE(345)
				_this->set_x(this->player->x);
				HX_STACK_LINE(345)
				_this->set_y(this->player->y);
			}
			HX_STACK_LINE(346)
			this->particleEmitter->start(true,(int)3,0.1,(int)50,(int)4);
			HX_STACK_LINE(348)
			::haxe::Log_obj::trace(thisPlayer->isAlive,hx::SourceInfo(HX_CSTRING("PlayState.hx"),348,HX_CSTRING("PlayState"),HX_CSTRING("hitBullet")));
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(PlayState_obj,hitBullet,(void))

Void PlayState_obj::touchTile( ::FallingTile thisTile,::Player thisPlayer){
{
		HX_STACK_FRAME("PlayState","touchTile",0x4ebc1e3c,"PlayState.touchTile","PlayState.hx",353,0xb30d7781)
		HX_STACK_THIS(this)
		HX_STACK_ARG(thisTile,"thisTile")
		HX_STACK_ARG(thisPlayer,"thisPlayer")
		HX_STACK_LINE(353)
		thisTile->start();
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(PlayState_obj,touchTile,(void))


PlayState_obj::PlayState_obj()
{
}

void PlayState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(PlayState);
	HX_MARK_MEMBER_NAME(player,"player");
	HX_MARK_MEMBER_NAME(floor,"floor");
	HX_MARK_MEMBER_NAME(loader,"loader");
	HX_MARK_MEMBER_NAME(midgroundMap,"midgroundMap");
	HX_MARK_MEMBER_NAME(backgroundMap,"backgroundMap");
	HX_MARK_MEMBER_NAME(foregroundMap,"foregroundMap");
	HX_MARK_MEMBER_NAME(tileGroup,"tileGroup");
	HX_MARK_MEMBER_NAME(turretGroup,"turretGroup");
	HX_MARK_MEMBER_NAME(spikeGroup,"spikeGroup");
	HX_MARK_MEMBER_NAME(score,"score");
	HX_MARK_MEMBER_NAME(scoreText,"scoreText");
	HX_MARK_MEMBER_NAME(star,"star");
	HX_MARK_MEMBER_NAME(darkness,"darkness");
	HX_MARK_MEMBER_NAME(light,"light");
	HX_MARK_MEMBER_NAME(falling,"falling");
	HX_MARK_MEMBER_NAME(particleEmitter,"particleEmitter");
	HX_MARK_MEMBER_NAME(particle,"particle");
	HX_MARK_MEMBER_NAME(right,"right");
	HX_MARK_MEMBER_NAME(left,"left");
	HX_MARK_MEMBER_NAME(up,"up");
	HX_MARK_MEMBER_NAME(buffer,"buffer");
	HX_MARK_MEMBER_NAME(touchR,"touchR");
	HX_MARK_MEMBER_NAME(touchL,"touchL");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void PlayState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(player,"player");
	HX_VISIT_MEMBER_NAME(floor,"floor");
	HX_VISIT_MEMBER_NAME(loader,"loader");
	HX_VISIT_MEMBER_NAME(midgroundMap,"midgroundMap");
	HX_VISIT_MEMBER_NAME(backgroundMap,"backgroundMap");
	HX_VISIT_MEMBER_NAME(foregroundMap,"foregroundMap");
	HX_VISIT_MEMBER_NAME(tileGroup,"tileGroup");
	HX_VISIT_MEMBER_NAME(turretGroup,"turretGroup");
	HX_VISIT_MEMBER_NAME(spikeGroup,"spikeGroup");
	HX_VISIT_MEMBER_NAME(score,"score");
	HX_VISIT_MEMBER_NAME(scoreText,"scoreText");
	HX_VISIT_MEMBER_NAME(star,"star");
	HX_VISIT_MEMBER_NAME(darkness,"darkness");
	HX_VISIT_MEMBER_NAME(light,"light");
	HX_VISIT_MEMBER_NAME(falling,"falling");
	HX_VISIT_MEMBER_NAME(particleEmitter,"particleEmitter");
	HX_VISIT_MEMBER_NAME(particle,"particle");
	HX_VISIT_MEMBER_NAME(right,"right");
	HX_VISIT_MEMBER_NAME(left,"left");
	HX_VISIT_MEMBER_NAME(up,"up");
	HX_VISIT_MEMBER_NAME(buffer,"buffer");
	HX_VISIT_MEMBER_NAME(touchR,"touchR");
	HX_VISIT_MEMBER_NAME(touchL,"touchL");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic PlayState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"up") ) { return up; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"star") ) { return star; }
		if (HX_FIELD_EQ(inName,"left") ) { return left; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"floor") ) { return floor; }
		if (HX_FIELD_EQ(inName,"score") ) { return score; }
		if (HX_FIELD_EQ(inName,"light") ) { return light; }
		if (HX_FIELD_EQ(inName,"right") ) { return right; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { return player; }
		if (HX_FIELD_EQ(inName,"loader") ) { return loader; }
		if (HX_FIELD_EQ(inName,"buffer") ) { return buffer; }
		if (HX_FIELD_EQ(inName,"touchR") ) { return touchR; }
		if (HX_FIELD_EQ(inName,"touchL") ) { return touchL; }
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"moveUp") ) { return moveUp_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"falling") ) { return falling; }
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"darkness") ) { return darkness; }
		if (HX_FIELD_EQ(inName,"particle") ) { return particle; }
		if (HX_FIELD_EQ(inName,"moveLeft") ) { return moveLeft_dyn(); }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tileGroup") ) { return tileGroup; }
		if (HX_FIELD_EQ(inName,"scoreText") ) { return scoreText; }
		if (HX_FIELD_EQ(inName,"moveRight") ) { return moveRight_dyn(); }
		if (HX_FIELD_EQ(inName,"hitBullet") ) { return hitBullet_dyn(); }
		if (HX_FIELD_EQ(inName,"touchTile") ) { return touchTile_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"spikeGroup") ) { return spikeGroup; }
		if (HX_FIELD_EQ(inName,"killPlayer") ) { return killPlayer_dyn(); }
		if (HX_FIELD_EQ(inName,"placeTiles") ) { return placeTiles_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"turretGroup") ) { return turretGroup; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"midgroundMap") ) { return midgroundMap; }
		if (HX_FIELD_EQ(inName,"removeBullet") ) { return removeBullet_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"backgroundMap") ) { return backgroundMap; }
		if (HX_FIELD_EQ(inName,"foregroundMap") ) { return foregroundMap; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"activateSpikes") ) { return activateSpikes_dyn(); }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"particleEmitter") ) { return particleEmitter; }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic PlayState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 2:
		if (HX_FIELD_EQ(inName,"up") ) { up=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"star") ) { star=inValue.Cast< ::flixel::addons::display::FlxStarField2D >(); return inValue; }
		if (HX_FIELD_EQ(inName,"left") ) { left=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"floor") ) { floor=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"score") ) { score=inValue.Cast< int >(); return inValue; }
		if (HX_FIELD_EQ(inName,"light") ) { light=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"right") ) { right=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"player") ) { player=inValue.Cast< ::Player >(); return inValue; }
		if (HX_FIELD_EQ(inName,"loader") ) { loader=inValue.Cast< ::flixel::addons::editors::ogmo::FlxOgmoLoader >(); return inValue; }
		if (HX_FIELD_EQ(inName,"buffer") ) { buffer=inValue.Cast< ::flixel::util::FlxTimer >(); return inValue; }
		if (HX_FIELD_EQ(inName,"touchR") ) { touchR=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		if (HX_FIELD_EQ(inName,"touchL") ) { touchL=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"falling") ) { falling=inValue.Cast< ::flixel::tile::FlxTilemap >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"darkness") ) { darkness=inValue.Cast< ::flixel::FlxSprite >(); return inValue; }
		if (HX_FIELD_EQ(inName,"particle") ) { particle=inValue.Cast< ::flixel::effects::particles::FlxParticle >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"tileGroup") ) { tileGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		if (HX_FIELD_EQ(inName,"scoreText") ) { scoreText=inValue.Cast< ::flixel::text::FlxText >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"spikeGroup") ) { spikeGroup=inValue.Cast< ::flixel::group::FlxGroup >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"turretGroup") ) { turretGroup=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 12:
		if (HX_FIELD_EQ(inName,"midgroundMap") ) { midgroundMap=inValue.Cast< ::flixel::tile::FlxTilemap >(); return inValue; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"backgroundMap") ) { backgroundMap=inValue.Cast< ::flixel::tile::FlxTilemap >(); return inValue; }
		if (HX_FIELD_EQ(inName,"foregroundMap") ) { foregroundMap=inValue.Cast< ::flixel::tile::FlxTilemap >(); return inValue; }
		break;
	case 15:
		if (HX_FIELD_EQ(inName,"particleEmitter") ) { particleEmitter=inValue.Cast< ::flixel::effects::particles::FlxEmitter >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void PlayState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("player"));
	outFields->push(HX_CSTRING("floor"));
	outFields->push(HX_CSTRING("loader"));
	outFields->push(HX_CSTRING("midgroundMap"));
	outFields->push(HX_CSTRING("backgroundMap"));
	outFields->push(HX_CSTRING("foregroundMap"));
	outFields->push(HX_CSTRING("tileGroup"));
	outFields->push(HX_CSTRING("turretGroup"));
	outFields->push(HX_CSTRING("spikeGroup"));
	outFields->push(HX_CSTRING("score"));
	outFields->push(HX_CSTRING("scoreText"));
	outFields->push(HX_CSTRING("star"));
	outFields->push(HX_CSTRING("darkness"));
	outFields->push(HX_CSTRING("light"));
	outFields->push(HX_CSTRING("falling"));
	outFields->push(HX_CSTRING("particleEmitter"));
	outFields->push(HX_CSTRING("particle"));
	outFields->push(HX_CSTRING("right"));
	outFields->push(HX_CSTRING("left"));
	outFields->push(HX_CSTRING("up"));
	outFields->push(HX_CSTRING("buffer"));
	outFields->push(HX_CSTRING("touchR"));
	outFields->push(HX_CSTRING("touchL"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::Player*/ ,(int)offsetof(PlayState_obj,player),HX_CSTRING("player")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,floor),HX_CSTRING("floor")},
	{hx::fsObject /*::flixel::addons::editors::ogmo::FlxOgmoLoader*/ ,(int)offsetof(PlayState_obj,loader),HX_CSTRING("loader")},
	{hx::fsObject /*::flixel::tile::FlxTilemap*/ ,(int)offsetof(PlayState_obj,midgroundMap),HX_CSTRING("midgroundMap")},
	{hx::fsObject /*::flixel::tile::FlxTilemap*/ ,(int)offsetof(PlayState_obj,backgroundMap),HX_CSTRING("backgroundMap")},
	{hx::fsObject /*::flixel::tile::FlxTilemap*/ ,(int)offsetof(PlayState_obj,foregroundMap),HX_CSTRING("foregroundMap")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,tileGroup),HX_CSTRING("tileGroup")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(PlayState_obj,turretGroup),HX_CSTRING("turretGroup")},
	{hx::fsObject /*::flixel::group::FlxGroup*/ ,(int)offsetof(PlayState_obj,spikeGroup),HX_CSTRING("spikeGroup")},
	{hx::fsInt,(int)offsetof(PlayState_obj,score),HX_CSTRING("score")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,scoreText),HX_CSTRING("scoreText")},
	{hx::fsObject /*::flixel::addons::display::FlxStarField2D*/ ,(int)offsetof(PlayState_obj,star),HX_CSTRING("star")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,darkness),HX_CSTRING("darkness")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,light),HX_CSTRING("light")},
	{hx::fsObject /*::flixel::tile::FlxTilemap*/ ,(int)offsetof(PlayState_obj,falling),HX_CSTRING("falling")},
	{hx::fsObject /*::flixel::effects::particles::FlxEmitter*/ ,(int)offsetof(PlayState_obj,particleEmitter),HX_CSTRING("particleEmitter")},
	{hx::fsObject /*::flixel::effects::particles::FlxParticle*/ ,(int)offsetof(PlayState_obj,particle),HX_CSTRING("particle")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,right),HX_CSTRING("right")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,left),HX_CSTRING("left")},
	{hx::fsObject /*::flixel::FlxSprite*/ ,(int)offsetof(PlayState_obj,up),HX_CSTRING("up")},
	{hx::fsObject /*::flixel::util::FlxTimer*/ ,(int)offsetof(PlayState_obj,buffer),HX_CSTRING("buffer")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,touchR),HX_CSTRING("touchR")},
	{hx::fsObject /*::flixel::text::FlxText*/ ,(int)offsetof(PlayState_obj,touchL),HX_CSTRING("touchL")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("player"),
	HX_CSTRING("floor"),
	HX_CSTRING("loader"),
	HX_CSTRING("midgroundMap"),
	HX_CSTRING("backgroundMap"),
	HX_CSTRING("foregroundMap"),
	HX_CSTRING("tileGroup"),
	HX_CSTRING("turretGroup"),
	HX_CSTRING("spikeGroup"),
	HX_CSTRING("score"),
	HX_CSTRING("scoreText"),
	HX_CSTRING("star"),
	HX_CSTRING("darkness"),
	HX_CSTRING("light"),
	HX_CSTRING("falling"),
	HX_CSTRING("particleEmitter"),
	HX_CSTRING("particle"),
	HX_CSTRING("right"),
	HX_CSTRING("left"),
	HX_CSTRING("up"),
	HX_CSTRING("buffer"),
	HX_CSTRING("touchR"),
	HX_CSTRING("touchL"),
	HX_CSTRING("create"),
	HX_CSTRING("moveUp"),
	HX_CSTRING("moveLeft"),
	HX_CSTRING("moveRight"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	HX_CSTRING("killPlayer"),
	HX_CSTRING("activateSpikes"),
	HX_CSTRING("placeTiles"),
	HX_CSTRING("removeBullet"),
	HX_CSTRING("hitBullet"),
	HX_CSTRING("touchTile"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(PlayState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(PlayState_obj::__mClass,"__mClass");
};

#endif

Class PlayState_obj::__mClass;

void PlayState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("PlayState"), hx::TCanCast< PlayState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void PlayState_obj::__boot()
{
}

