#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShape
#include <flixel/addons/display/shapes/FlxShape.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_openfl_display_BitmapData
#include <openfl/display/BitmapData.h>
#endif
#ifndef INCLUDED_openfl_display_BlendMode
#include <openfl/display/BlendMode.h>
#endif
#ifndef INCLUDED_openfl_display_IBitmapDrawable
#include <openfl/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl_geom_ColorTransform
#include <openfl/geom/ColorTransform.h>
#endif
#ifndef INCLUDED_openfl_geom_Matrix
#include <openfl/geom/Matrix.h>
#endif
#ifndef INCLUDED_openfl_geom_Rectangle
#include <openfl/geom/Rectangle.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace shapes{

Void FlxShape_obj::__construct(Float X,Float Y,Float CanvasWidth,Float CanvasHeight,Dynamic LineStyle_,Dynamic FillStyle_,hx::Null< Float >  __o_TrueWidth,hx::Null< Float >  __o_TrueHeight)
{
HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","new",0x99784848,"flixel.addons.display.shapes.FlxShape.new","flixel/addons/display/shapes/FlxShape.hx",17,0x1d167428)
HX_STACK_THIS(this)
HX_STACK_ARG(X,"X")
HX_STACK_ARG(Y,"Y")
HX_STACK_ARG(CanvasWidth,"CanvasWidth")
HX_STACK_ARG(CanvasHeight,"CanvasHeight")
HX_STACK_ARG(LineStyle_,"LineStyle_")
HX_STACK_ARG(FillStyle_,"FillStyle_")
HX_STACK_ARG(__o_TrueWidth,"TrueWidth")
HX_STACK_ARG(__o_TrueHeight,"TrueHeight")
Float TrueWidth = __o_TrueWidth.Default(0);
Float TrueHeight = __o_TrueHeight.Default(0);
{
	HX_STACK_LINE(23)
	this->shapeDirty = false;
	HX_STACK_LINE(42)
	super::__construct(X,Y,null());
	HX_STACK_LINE(44)
	this->shape_id = HX_CSTRING("");
	HX_STACK_LINE(46)
	if (((CanvasWidth < (int)1))){
		HX_STACK_LINE(46)
		CanvasWidth = (int)1;
	}
	HX_STACK_LINE(47)
	if (((CanvasHeight < (int)1))){
		HX_STACK_LINE(47)
		CanvasHeight = (int)1;
	}
	HX_STACK_LINE(49)
	this->set_width(CanvasWidth);
	HX_STACK_LINE(50)
	this->set_height(CanvasHeight);
	HX_STACK_LINE(52)
	Float _g = this->get_width();		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(52)
	int _g1 = ::Std_obj::_int(_g);		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(52)
	Float _g2 = this->get_height();		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(52)
	int _g3 = ::Std_obj::_int(_g2);		HX_STACK_VAR(_g3,"_g3");
	HX_STACK_LINE(52)
	this->makeGraphic(_g1,_g3,(int)0,true,null());
	HX_STACK_LINE(54)
	{
		HX_STACK_LINE(54)
		this->lineStyle = LineStyle_;
		HX_STACK_LINE(54)
		this->shapeDirty = true;
		HX_STACK_LINE(54)
		this->lineStyle;
	}
	HX_STACK_LINE(55)
	{
		HX_STACK_LINE(55)
		this->fillStyle = FillStyle_;
		HX_STACK_LINE(55)
		this->shapeDirty = true;
		HX_STACK_LINE(55)
		this->fillStyle;
	}
	struct _Function_1_1{
		inline static Dynamic Block( ){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/shapes/FlxShape.hx",59,0x1d167428)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("matrix") , null(),false);
				__result->Add(HX_CSTRING("colorTransform") , null(),false);
				__result->Add(HX_CSTRING("blendMode") , ::openfl::display::BlendMode_obj::NORMAL,false);
				__result->Add(HX_CSTRING("clipRect") , null(),false);
				__result->Add(HX_CSTRING("smoothing") , true,false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(59)
	this->_drawStyle = _Function_1_1::Block();
	HX_STACK_LINE(61)
	if (((bool((TrueWidth != (int)0)) && bool((TrueHeight != (int)0))))){
		HX_STACK_LINE(63)
		if (((bool((TrueWidth < CanvasWidth)) && bool((TrueHeight < CanvasHeight))))){
			HX_STACK_LINE(64)
			this->fixBoundaries(TrueWidth,TrueHeight);
		}
	}
	HX_STACK_LINE(67)
	this->shapeDirty = true;
}
;
	return null();
}

//FlxShape_obj::~FlxShape_obj() { }

Dynamic FlxShape_obj::__CreateEmpty() { return  new FlxShape_obj; }
hx::ObjectPtr< FlxShape_obj > FlxShape_obj::__new(Float X,Float Y,Float CanvasWidth,Float CanvasHeight,Dynamic LineStyle_,Dynamic FillStyle_,hx::Null< Float >  __o_TrueWidth,hx::Null< Float >  __o_TrueHeight)
{  hx::ObjectPtr< FlxShape_obj > result = new FlxShape_obj();
	result->__construct(X,Y,CanvasWidth,CanvasHeight,LineStyle_,FillStyle_,__o_TrueWidth,__o_TrueHeight);
	return result;}

Dynamic FlxShape_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxShape_obj > result = new FlxShape_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4],inArgs[5],inArgs[6],inArgs[7]);
	return result;}

Void FlxShape_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","destroy",0x63a8b8e2,"flixel.addons.display.shapes.FlxShape.destroy","flixel/addons/display/shapes/FlxShape.hx",71,0x1d167428)
		HX_STACK_THIS(this)
		HX_STACK_LINE(72)
		{
			HX_STACK_LINE(72)
			this->lineStyle = null();
			HX_STACK_LINE(72)
			this->shapeDirty = true;
			HX_STACK_LINE(72)
			this->lineStyle;
		}
		HX_STACK_LINE(73)
		{
			HX_STACK_LINE(73)
			this->fillStyle = null();
			HX_STACK_LINE(73)
			this->shapeDirty = true;
			HX_STACK_LINE(73)
			this->fillStyle;
		}
		HX_STACK_LINE(74)
		this->super::destroy();
	}
return null();
}


Void FlxShape_obj::drawSpecificShape( ::openfl::geom::Matrix matrix){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","drawSpecificShape",0x2fc8bc53,"flixel.addons.display.shapes.FlxShape.drawSpecificShape","flixel/addons/display/shapes/FlxShape.hx",78,0x1d167428)
		HX_STACK_THIS(this)
		HX_STACK_ARG(matrix,"matrix")
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShape_obj,drawSpecificShape,(void))

Void FlxShape_obj::redrawShape( ){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","redrawShape",0x444fc652,"flixel.addons.display.shapes.FlxShape.redrawShape","flixel/addons/display/shapes/FlxShape.hx",84,0x1d167428)
		HX_STACK_THIS(this)
		HX_STACK_LINE(85)
		::openfl::geom::Rectangle _g = this->get_pixels()->get_rect();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(85)
		this->get_pixels()->fillRect(_g,(int)0);
		HX_STACK_LINE(86)
		if (((this->lineStyle->__Field(HX_CSTRING("thickness"),true) > (int)1))){
			HX_STACK_LINE(88)
			::openfl::geom::Matrix matrix = this->getStrokeOffsetMatrix(this->_matrix);		HX_STACK_VAR(matrix,"matrix");
			HX_STACK_LINE(89)
			this->drawSpecificShape(matrix);
		}
		else{
			HX_STACK_LINE(92)
			this->drawSpecificShape(null());
		}
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(FlxShape_obj,redrawShape,(void))

Void FlxShape_obj::draw( ){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","draw",0xa9349f1c,"flixel.addons.display.shapes.FlxShape.draw","flixel/addons/display/shapes/FlxShape.hx",96,0x1d167428)
		HX_STACK_THIS(this)
		HX_STACK_LINE(97)
		if ((this->shapeDirty)){
			HX_STACK_LINE(99)
			this->redrawShape();
			HX_STACK_LINE(100)
			this->shapeDirty = false;
		}
		HX_STACK_LINE(102)
		this->super::draw();
	}
return null();
}


Void FlxShape_obj::fixBoundaries( Float trueWidth,Float trueHeight){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","fixBoundaries",0x7367f7e5,"flixel.addons.display.shapes.FlxShape.fixBoundaries","flixel/addons/display/shapes/FlxShape.hx",112,0x1d167428)
		HX_STACK_THIS(this)
		HX_STACK_ARG(trueWidth,"trueWidth")
		HX_STACK_ARG(trueHeight,"trueHeight")
		HX_STACK_LINE(113)
		this->set_width(trueWidth);
		HX_STACK_LINE(114)
		this->set_height(trueHeight);
		HX_STACK_LINE(116)
		Float strokeBuffer = this->lineStyle->__Field(HX_CSTRING("thickness"),true);		HX_STACK_VAR(strokeBuffer,"strokeBuffer");
		HX_STACK_LINE(119)
		this->offset->set_x((Float(strokeBuffer) / Float((int)2)));
		HX_STACK_LINE(120)
		this->offset->set_y((Float(strokeBuffer) / Float((int)2)));
		HX_STACK_LINE(122)
		this->shapeDirty = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(FlxShape_obj,fixBoundaries,(void))

Dynamic FlxShape_obj::set_lineStyle( Dynamic ls){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","set_lineStyle",0xed12c868,"flixel.addons.display.shapes.FlxShape.set_lineStyle","flixel/addons/display/shapes/FlxShape.hx",126,0x1d167428)
	HX_STACK_THIS(this)
	HX_STACK_ARG(ls,"ls")
	HX_STACK_LINE(127)
	this->lineStyle = ls;
	HX_STACK_LINE(128)
	this->shapeDirty = true;
	HX_STACK_LINE(129)
	return this->lineStyle;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShape_obj,set_lineStyle,return )

Dynamic FlxShape_obj::set_fillStyle( Dynamic fs){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","set_fillStyle",0x74beeb79,"flixel.addons.display.shapes.FlxShape.set_fillStyle","flixel/addons/display/shapes/FlxShape.hx",133,0x1d167428)
	HX_STACK_THIS(this)
	HX_STACK_ARG(fs,"fs")
	HX_STACK_LINE(134)
	this->fillStyle = fs;
	HX_STACK_LINE(135)
	this->shapeDirty = true;
	HX_STACK_LINE(136)
	return this->fillStyle;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShape_obj,set_fillStyle,return )

::openfl::geom::Matrix FlxShape_obj::getStrokeOffsetMatrix( ::openfl::geom::Matrix matrix){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShape","getStrokeOffsetMatrix",0xde9f7a2a,"flixel.addons.display.shapes.FlxShape.getStrokeOffsetMatrix","flixel/addons/display/shapes/FlxShape.hx",140,0x1d167428)
	HX_STACK_THIS(this)
	HX_STACK_ARG(matrix,"matrix")
	HX_STACK_LINE(141)
	Float buffer = (Float(this->lineStyle->__Field(HX_CSTRING("thickness"),true)) / Float((int)2));		HX_STACK_VAR(buffer,"buffer");
	HX_STACK_LINE(142)
	matrix->identity();
	HX_STACK_LINE(143)
	matrix->translate(buffer,buffer);
	HX_STACK_LINE(144)
	return matrix;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShape_obj,getStrokeOffsetMatrix,return )


FlxShape_obj::FlxShape_obj()
{
}

void FlxShape_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxShape);
	HX_MARK_MEMBER_NAME(lineStyle,"lineStyle");
	HX_MARK_MEMBER_NAME(fillStyle,"fillStyle");
	HX_MARK_MEMBER_NAME(shape_id,"shape_id");
	HX_MARK_MEMBER_NAME(shapeDirty,"shapeDirty");
	HX_MARK_MEMBER_NAME(_drawStyle,"_drawStyle");
	::flixel::FlxSprite_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxShape_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(lineStyle,"lineStyle");
	HX_VISIT_MEMBER_NAME(fillStyle,"fillStyle");
	HX_VISIT_MEMBER_NAME(shape_id,"shape_id");
	HX_VISIT_MEMBER_NAME(shapeDirty,"shapeDirty");
	HX_VISIT_MEMBER_NAME(_drawStyle,"_drawStyle");
	::flixel::FlxSprite_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxShape_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"shape_id") ) { return shape_id; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"lineStyle") ) { return lineStyle; }
		if (HX_FIELD_EQ(inName,"fillStyle") ) { return fillStyle; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"shapeDirty") ) { return shapeDirty; }
		if (HX_FIELD_EQ(inName,"_drawStyle") ) { return _drawStyle; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"redrawShape") ) { return redrawShape_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"fixBoundaries") ) { return fixBoundaries_dyn(); }
		if (HX_FIELD_EQ(inName,"set_lineStyle") ) { return set_lineStyle_dyn(); }
		if (HX_FIELD_EQ(inName,"set_fillStyle") ) { return set_fillStyle_dyn(); }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"drawSpecificShape") ) { return drawSpecificShape_dyn(); }
		break;
	case 21:
		if (HX_FIELD_EQ(inName,"getStrokeOffsetMatrix") ) { return getStrokeOffsetMatrix_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxShape_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 8:
		if (HX_FIELD_EQ(inName,"shape_id") ) { shape_id=inValue.Cast< ::String >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"lineStyle") ) { if (inCallProp) return set_lineStyle(inValue);lineStyle=inValue.Cast< Dynamic >(); return inValue; }
		if (HX_FIELD_EQ(inName,"fillStyle") ) { if (inCallProp) return set_fillStyle(inValue);fillStyle=inValue.Cast< Dynamic >(); return inValue; }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"shapeDirty") ) { shapeDirty=inValue.Cast< bool >(); return inValue; }
		if (HX_FIELD_EQ(inName,"_drawStyle") ) { _drawStyle=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxShape_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("lineStyle"));
	outFields->push(HX_CSTRING("fillStyle"));
	outFields->push(HX_CSTRING("shape_id"));
	outFields->push(HX_CSTRING("shapeDirty"));
	outFields->push(HX_CSTRING("_drawStyle"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxShape_obj,lineStyle),HX_CSTRING("lineStyle")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxShape_obj,fillStyle),HX_CSTRING("fillStyle")},
	{hx::fsString,(int)offsetof(FlxShape_obj,shape_id),HX_CSTRING("shape_id")},
	{hx::fsBool,(int)offsetof(FlxShape_obj,shapeDirty),HX_CSTRING("shapeDirty")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxShape_obj,_drawStyle),HX_CSTRING("_drawStyle")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("lineStyle"),
	HX_CSTRING("fillStyle"),
	HX_CSTRING("shape_id"),
	HX_CSTRING("shapeDirty"),
	HX_CSTRING("_drawStyle"),
	HX_CSTRING("destroy"),
	HX_CSTRING("drawSpecificShape"),
	HX_CSTRING("redrawShape"),
	HX_CSTRING("draw"),
	HX_CSTRING("fixBoundaries"),
	HX_CSTRING("set_lineStyle"),
	HX_CSTRING("set_fillStyle"),
	HX_CSTRING("getStrokeOffsetMatrix"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxShape_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxShape_obj::__mClass,"__mClass");
};

#endif

Class FlxShape_obj::__mClass;

void FlxShape_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.shapes.FlxShape"), hx::TCanCast< FlxShape_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxShape_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes
