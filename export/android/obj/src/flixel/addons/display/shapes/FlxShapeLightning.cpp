#include <hxcpp.h>

#ifndef INCLUDED_Std
#include <Std.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShape
#include <flixel/addons/display/shapes/FlxShape.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeLightning
#include <flixel/addons/display/shapes/FlxShapeLightning.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeLine
#include <flixel/addons/display/shapes/FlxShapeLine.h>
#endif
#ifndef INCLUDED_flixel_addons_display_shapes_LineSegment
#include <flixel/addons/display/shapes/LineSegment.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_system_debug_LogStyle
#include <flixel/system/debug/LogStyle.h>
#endif
#ifndef INCLUDED_flixel_system_frontEnds_LogFrontEnd
#include <flixel/system/frontEnds/LogFrontEnd.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxPoint
#include <flixel/util/FlxPool_flixel_util_FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxVector
#include <flixel/util/FlxPool_flixel_util_FlxVector.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_flixel_util_FlxSpriteUtil
#include <flixel/util/FlxSpriteUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxVector
#include <flixel/util/FlxVector.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
#ifndef INCLUDED_openfl_display_BitmapData
#include <openfl/display/BitmapData.h>
#endif
#ifndef INCLUDED_openfl_display_CapsStyle
#include <openfl/display/CapsStyle.h>
#endif
#ifndef INCLUDED_openfl_display_IBitmapDrawable
#include <openfl/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl_display_JointStyle
#include <openfl/display/JointStyle.h>
#endif
#ifndef INCLUDED_openfl_display_LineScaleMode
#include <openfl/display/LineScaleMode.h>
#endif
#ifndef INCLUDED_openfl_filters_BitmapFilter
#include <openfl/filters/BitmapFilter.h>
#endif
#ifndef INCLUDED_openfl_filters_GlowFilter
#include <openfl/filters/GlowFilter.h>
#endif
#ifndef INCLUDED_openfl_geom_Matrix
#include <openfl/geom/Matrix.h>
#endif
#ifndef INCLUDED_openfl_geom_Point
#include <openfl/geom/Point.h>
#endif
#ifndef INCLUDED_openfl_geom_Rectangle
#include <openfl/geom/Rectangle.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace shapes{

Void FlxShapeLightning_obj::__construct(Float X,Float Y,::flixel::util::FlxPoint A,::flixel::util::FlxPoint B,Dynamic Style,hx::Null< bool >  __o_UseDefaults)
{
HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","new",0xbf0334c6,"flixel.addons.display.shapes.FlxShapeLightning.new","flixel/addons/display/shapes/FlxShapeLightning.hx",20,0xdb65696a)
HX_STACK_THIS(this)
HX_STACK_ARG(X,"X")
HX_STACK_ARG(Y,"Y")
HX_STACK_ARG(A,"A")
HX_STACK_ARG(B,"B")
HX_STACK_ARG(Style,"Style")
HX_STACK_ARG(__o_UseDefaults,"UseDefaults")
bool UseDefaults = __o_UseDefaults.Default(true);
{
	HX_STACK_LINE(36)
	this->filterDirty = false;
	HX_STACK_LINE(50)
	this->shape_id = HX_CSTRING("lightning");
	HX_STACK_LINE(51)
	{
		HX_STACK_LINE(51)
		this->lightningStyle = Style;
		HX_STACK_LINE(51)
		this->shapeDirty = true;
		HX_STACK_LINE(51)
		this->lightningStyle;
	}
	HX_STACK_LINE(53)
	::flixel::util::FlxVector v;		HX_STACK_VAR(v,"v");
	HX_STACK_LINE(53)
	{
		HX_STACK_LINE(53)
		::flixel::util::FlxVector vector = hx::TCast< ::flixel::util::FlxVector >::cast(::flixel::util::FlxVector_obj::_pool->get()->set((A->x - B->x),(A->y - B->y)));		HX_STACK_VAR(vector,"vector");
		HX_STACK_LINE(53)
		vector->_inPool = false;
		HX_STACK_LINE(53)
		v = vector;
	}
	HX_STACK_LINE(54)
	Float _g = ::Math_obj::sqrt(((v->x * v->x) + (v->y * v->y)));		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(54)
	this->magnitude = _g;
	HX_STACK_LINE(56)
	if (((bool((this->lightningStyle->__Field(HX_CSTRING("halo_colors"),true) == null())) && bool(UseDefaults)))){
		HX_STACK_LINE(57)
		this->lightningStyle->__FieldRef(HX_CSTRING("halo_colors")) = Array_obj< int >::__new().Add((int)-7820562).Add((int)-11184692).Add((int)-13417336);
	}
	HX_STACK_LINE(59)
	Array< ::Dynamic > _g1 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(59)
	this->list_segs = _g1;
	HX_STACK_LINE(60)
	Array< ::Dynamic > _g2 = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(60)
	this->list_branch = _g2;
	HX_STACK_LINE(62)
	Float w = ::Math_obj::abs((A->x - B->x));		HX_STACK_VAR(w,"w");
	HX_STACK_LINE(63)
	Float h = ::Math_obj::abs((B->y - B->y));		HX_STACK_VAR(h,"h");
	struct _Function_1_1{
		inline static Dynamic Block( ){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/shapes/FlxShapeLightning.hx",65,0xdb65696a)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("thickness") , (int)1,false);
				__result->Add(HX_CSTRING("color") , (int)-1,false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(65)
	Dynamic testStyle = _Function_1_1::Block();		HX_STACK_VAR(testStyle,"testStyle");
	HX_STACK_LINE(66)
	super::__construct(X,Y,A,B,testStyle);
	HX_STACK_LINE(69)
	this->calculate(A,B,this->lightningStyle->__Field(HX_CSTRING("displacement"),true),(int)0);
	HX_STACK_LINE(71)
	if ((A->_weak)){
		HX_STACK_LINE(71)
		A->put();
	}
	HX_STACK_LINE(72)
	if ((B->_weak)){
		HX_STACK_LINE(72)
		B->put();
	}
}
;
	return null();
}

//FlxShapeLightning_obj::~FlxShapeLightning_obj() { }

Dynamic FlxShapeLightning_obj::__CreateEmpty() { return  new FlxShapeLightning_obj; }
hx::ObjectPtr< FlxShapeLightning_obj > FlxShapeLightning_obj::__new(Float X,Float Y,::flixel::util::FlxPoint A,::flixel::util::FlxPoint B,Dynamic Style,hx::Null< bool >  __o_UseDefaults)
{  hx::ObjectPtr< FlxShapeLightning_obj > result = new FlxShapeLightning_obj();
	result->__construct(X,Y,A,B,Style,__o_UseDefaults);
	return result;}

Dynamic FlxShapeLightning_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxShapeLightning_obj > result = new FlxShapeLightning_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4],inArgs[5]);
	return result;}

Void FlxShapeLightning_obj::addSegment( ::flixel::util::FlxPoint A,::flixel::util::FlxPoint B){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","addSegment",0x92378fec,"flixel.addons.display.shapes.FlxShapeLightning.addSegment","flixel/addons/display/shapes/FlxShapeLightning.hx",76,0xdb65696a)
		HX_STACK_THIS(this)
		HX_STACK_ARG(A,"A")
		HX_STACK_ARG(B,"B")
		HX_STACK_LINE(77)
		::flixel::addons::display::shapes::LineSegment _g = ::flixel::addons::display::shapes::LineSegment_obj::__new(A,B);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(77)
		this->list_segs->push(_g);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC2(FlxShapeLightning_obj,addSegment,(void))

Void FlxShapeLightning_obj::calculate( ::flixel::util::FlxPoint A,::flixel::util::FlxPoint B,Float Displacement,int Iteration){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","calculate",0x654a4a4c,"flixel.addons.display.shapes.FlxShapeLightning.calculate","flixel/addons/display/shapes/FlxShapeLightning.hx",81,0xdb65696a)
		HX_STACK_THIS(this)
		HX_STACK_ARG(A,"A")
		HX_STACK_ARG(B,"B")
		HX_STACK_ARG(Displacement,"Displacement")
		HX_STACK_ARG(Iteration,"Iteration")
		HX_STACK_LINE(82)
		if (((Displacement < this->lightningStyle->__Field(HX_CSTRING("detail"),true)))){
			HX_STACK_LINE(84)
			::flixel::addons::display::shapes::LineSegment _g = ::flixel::addons::display::shapes::LineSegment_obj::__new(A,B);		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(84)
			this->list_segs->push(_g);
		}
		else{
			HX_STACK_LINE(88)
			::flixel::util::FlxPoint mid;		HX_STACK_VAR(mid,"mid");
			HX_STACK_LINE(88)
			{
				HX_STACK_LINE(88)
				Float X = (int)0;		HX_STACK_VAR(X,"X");
				HX_STACK_LINE(88)
				Float Y = (int)0;		HX_STACK_VAR(Y,"Y");
				HX_STACK_LINE(88)
				::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set(X,Y);		HX_STACK_VAR(point,"point");
				HX_STACK_LINE(88)
				point->_inPool = false;
				HX_STACK_LINE(88)
				mid = point;
			}
			HX_STACK_LINE(89)
			mid->set_x((Float(((A->x + B->x))) / Float((int)2)));
			HX_STACK_LINE(90)
			mid->set_y((Float(((A->y + B->y))) / Float((int)2)));
			HX_STACK_LINE(91)
			Float dispX = ::flixel::util::FlxRandom_obj::floatRanged(-0.5,0.5,null());		HX_STACK_VAR(dispX,"dispX");
			HX_STACK_LINE(92)
			Float dispY = ::flixel::util::FlxRandom_obj::floatRanged(-0.5,0.5,null());		HX_STACK_VAR(dispY,"dispY");
			HX_STACK_LINE(93)
			{
				HX_STACK_LINE(93)
				::flixel::util::FlxPoint _g = mid;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(93)
				_g->set_x((_g->x + (dispX * Displacement)));
			}
			HX_STACK_LINE(94)
			{
				HX_STACK_LINE(94)
				::flixel::util::FlxPoint _g = mid;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(94)
				_g->set_y((_g->y + (dispY * Displacement)));
			}
			HX_STACK_LINE(95)
			this->calculate(A,mid,(Float(Displacement) / Float((int)2)),Iteration);
			HX_STACK_LINE(96)
			this->calculate(B,mid,(Float(Displacement) / Float((int)2)),Iteration);
		}
		HX_STACK_LINE(98)
		this->shapeDirty = true;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC4(FlxShapeLightning_obj,calculate,(void))

Dynamic FlxShapeLightning_obj::set_lightningStyle( Dynamic Style){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","set_lightningStyle",0xae9e695e,"flixel.addons.display.shapes.FlxShapeLightning.set_lightningStyle","flixel/addons/display/shapes/FlxShapeLightning.hx",102,0xdb65696a)
	HX_STACK_THIS(this)
	HX_STACK_ARG(Style,"Style")
	HX_STACK_LINE(103)
	this->lightningStyle = Style;
	HX_STACK_LINE(104)
	this->shapeDirty = true;
	HX_STACK_LINE(105)
	return this->lightningStyle;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShapeLightning_obj,set_lightningStyle,return )

Dynamic FlxShapeLightning_obj::copyLineStyle( Dynamic ls){
	HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","copyLineStyle",0xf6d14e0e,"flixel.addons.display.shapes.FlxShapeLightning.copyLineStyle","flixel/addons/display/shapes/FlxShapeLightning.hx",109,0xdb65696a)
	HX_STACK_THIS(this)
	HX_STACK_ARG(ls,"ls")
	struct _Function_1_1{
		inline static Dynamic Block( Dynamic &ls){
			HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/shapes/FlxShapeLightning.hx",111,0xdb65696a)
			{
				hx::Anon __result = hx::Anon_obj::Create();
				__result->Add(HX_CSTRING("thickness") , ls->__Field(HX_CSTRING("thickness"),true),false);
				__result->Add(HX_CSTRING("color") , ls->__Field(HX_CSTRING("color"),true),false);
				__result->Add(HX_CSTRING("pixelHinting") , ls->__Field(HX_CSTRING("pixelHinting"),true),false);
				__result->Add(HX_CSTRING("scaleMode") , ls->__Field(HX_CSTRING("scaleMode"),true),false);
				__result->Add(HX_CSTRING("capsStyle") , ls->__Field(HX_CSTRING("capsStyle"),true),false);
				__result->Add(HX_CSTRING("jointStyle") , ls->__Field(HX_CSTRING("jointStyle"),true),false);
				__result->Add(HX_CSTRING("miterLimit") , ls->__Field(HX_CSTRING("miterLimit"),true),false);
				return __result;
			}
			return null();
		}
	};
	HX_STACK_LINE(111)
	Dynamic ls2 = _Function_1_1::Block(ls);		HX_STACK_VAR(ls2,"ls2");
	HX_STACK_LINE(120)
	return ls2;
}


HX_DEFINE_DYNAMIC_FUNC1(FlxShapeLightning_obj,copyLineStyle,return )

Void FlxShapeLightning_obj::drawSpecificShape( ::openfl::geom::Matrix matrix){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","drawSpecificShape",0x7df14951,"flixel.addons.display.shapes.FlxShapeLightning.drawSpecificShape","flixel/addons/display/shapes/FlxShapeLightning.hx",124,0xdb65696a)
		HX_STACK_THIS(this)
		HX_STACK_ARG(matrix,"matrix")
		HX_STACK_LINE(125)
		Float up = (int)9999;		HX_STACK_VAR(up,"up");
		HX_STACK_LINE(126)
		Float left = (int)9999;		HX_STACK_VAR(left,"left");
		HX_STACK_LINE(127)
		Float down = (int)0;		HX_STACK_VAR(down,"down");
		HX_STACK_LINE(128)
		Float right = (int)0;		HX_STACK_VAR(right,"right");
		HX_STACK_LINE(130)
		::flixel::addons::display::shapes::LineSegment l;		HX_STACK_VAR(l,"l");
		HX_STACK_LINE(131)
		{
			HX_STACK_LINE(131)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(131)
			Array< ::Dynamic > _g1 = this->list_segs;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(131)
			while((true)){
				HX_STACK_LINE(131)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(131)
					break;
				}
				HX_STACK_LINE(131)
				::flixel::addons::display::shapes::LineSegment l1 = _g1->__get(_g).StaticCast< ::flixel::addons::display::shapes::LineSegment >();		HX_STACK_VAR(l1,"l1");
				HX_STACK_LINE(131)
				++(_g);
				HX_STACK_LINE(133)
				if (((l1->a->x < left))){
					HX_STACK_LINE(133)
					left = l1->a->x;
				}
				HX_STACK_LINE(134)
				if (((l1->b->x < left))){
					HX_STACK_LINE(134)
					left = l1->b->x;
				}
				HX_STACK_LINE(135)
				if (((l1->a->y < up))){
					HX_STACK_LINE(135)
					up = l1->a->y;
				}
				HX_STACK_LINE(136)
				if (((l1->b->y < up))){
					HX_STACK_LINE(136)
					up = l1->b->y;
				}
				HX_STACK_LINE(137)
				if (((l1->a->x > right))){
					HX_STACK_LINE(137)
					right = l1->a->x;
				}
				HX_STACK_LINE(138)
				if (((l1->b->x > right))){
					HX_STACK_LINE(138)
					right = l1->b->x;
				}
				HX_STACK_LINE(139)
				if (((l1->a->y > down))){
					HX_STACK_LINE(139)
					down = l1->a->y;
				}
				HX_STACK_LINE(140)
				if (((l1->b->y > down))){
					HX_STACK_LINE(140)
					down = l1->b->y;
				}
			}
		}
		HX_STACK_LINE(143)
		::flixel::FlxG_obj::log->advanced(((((HX_CSTRING("ul = (") + left) + HX_CSTRING(",")) + up) + HX_CSTRING(")")),::flixel::system::debug::LogStyle_obj::NORMAL,null());
		HX_STACK_LINE(144)
		::flixel::FlxG_obj::log->advanced(((((HX_CSTRING("lr = (") + down) + HX_CSTRING(",")) + right) + HX_CSTRING(")")),::flixel::system::debug::LogStyle_obj::NORMAL,null());
		HX_STACK_LINE(146)
		Float strokeBuffer = this->lightningStyle->__Field(HX_CSTRING("thickness"),true);		HX_STACK_VAR(strokeBuffer,"strokeBuffer");
		HX_STACK_LINE(149)
		Float trueWidth = ::Math_obj::abs((this->point->x - this->point2->x));		HX_STACK_VAR(trueWidth,"trueWidth");
		HX_STACK_LINE(150)
		Float trueHeight = ::Math_obj::abs((this->point->y - this->point2->y));		HX_STACK_VAR(trueHeight,"trueHeight");
		HX_STACK_LINE(153)
		Float newWidth = (right - left);		HX_STACK_VAR(newWidth,"newWidth");
		HX_STACK_LINE(154)
		Float newHeight = (down - up);		HX_STACK_VAR(newHeight,"newHeight");
		HX_STACK_LINE(157)
		int canvasWidth = ::Std_obj::_int((newWidth + strokeBuffer));		HX_STACK_VAR(canvasWidth,"canvasWidth");
		HX_STACK_LINE(158)
		int canvasHeight = ::Std_obj::_int((newHeight + strokeBuffer));		HX_STACK_VAR(canvasHeight,"canvasHeight");
		HX_STACK_LINE(160)
		this->offset->set_x((int)0);
		HX_STACK_LINE(161)
		this->offset->set_y((int)0);
		HX_STACK_LINE(162)
		this->set_width(canvasWidth);
		HX_STACK_LINE(163)
		this->set_height(canvasHeight);
		HX_STACK_LINE(165)
		int _g = this->get_pixels()->get_width();		HX_STACK_VAR(_g,"_g");
		struct _Function_1_1{
			inline static bool Block( hx::ObjectPtr< ::flixel::addons::display::shapes::FlxShapeLightning_obj > __this,int &canvasHeight){
				HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/shapes/FlxShapeLightning.hx",165,0xdb65696a)
				{
					HX_STACK_LINE(165)
					int _g1 = __this->get_pixels()->get_height();		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(165)
					return (canvasHeight != _g1);
				}
				return null();
			}
		};
		HX_STACK_LINE(165)
		if (((  ((!(((canvasWidth != _g))))) ? bool(_Function_1_1::Block(this,canvasHeight)) : bool(true) ))){
			HX_STACK_LINE(166)
			this->makeGraphic(canvasWidth,canvasHeight,(int)0,true,null());
		}
		else{
			HX_STACK_LINE(168)
			::openfl::geom::Rectangle _g2 = this->get_pixels()->get_rect();		HX_STACK_VAR(_g2,"_g2");
			HX_STACK_LINE(168)
			this->get_pixels()->fillRect(_g2,(int)0);
		}
		HX_STACK_LINE(170)
		this->_matrix->identity();
		HX_STACK_LINE(172)
		int dw = (int)0;		HX_STACK_VAR(dw,"dw");
		HX_STACK_LINE(173)
		int dh = (int)0;		HX_STACK_VAR(dh,"dh");
		HX_STACK_LINE(176)
		if (((left < (int)0))){
			HX_STACK_LINE(176)
			int _g3 = ::Std_obj::_int((-(left) + (Float(strokeBuffer) / Float((int)2))));		HX_STACK_VAR(_g3,"_g3");
			HX_STACK_LINE(176)
			dw = _g3;
		}
		HX_STACK_LINE(177)
		if (((up < (int)0))){
			HX_STACK_LINE(177)
			int _g4 = ::Std_obj::_int((-(up) + (Float(strokeBuffer) / Float((int)2))));		HX_STACK_VAR(_g4,"_g4");
			HX_STACK_LINE(177)
			dh = _g4;
		}
		HX_STACK_LINE(179)
		{
			HX_STACK_LINE(179)
			int _g1 = (int)0;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(179)
			Array< ::Dynamic > _g11 = this->list_segs;		HX_STACK_VAR(_g11,"_g11");
			HX_STACK_LINE(179)
			while((true)){
				HX_STACK_LINE(179)
				if ((!(((_g1 < _g11->length))))){
					HX_STACK_LINE(179)
					break;
				}
				HX_STACK_LINE(179)
				::flixel::addons::display::shapes::LineSegment l1 = _g11->__get(_g1).StaticCast< ::flixel::addons::display::shapes::LineSegment >();		HX_STACK_VAR(l1,"l1");
				HX_STACK_LINE(179)
				++(_g1);
				HX_STACK_LINE(180)
				::flixel::util::FlxSpriteUtil_obj::drawLine(hx::ObjectPtr<OBJ_>(this),(l1->a->x + dw),(l1->a->y + dh),(l1->b->x + dw),(l1->b->y + dh),this->lineStyle,null());
			}
		}
		HX_STACK_LINE(183)
		bool fillStyle_hasFill = false;		HX_STACK_VAR(fillStyle_hasFill,"fillStyle_hasFill");
		HX_STACK_LINE(185)
		this->set_width(trueWidth);
		HX_STACK_LINE(186)
		this->set_height(trueHeight);
		HX_STACK_LINE(188)
		this->offset->set_x(dw);
		HX_STACK_LINE(189)
		this->offset->set_y(dh);
		HX_STACK_LINE(191)
		this->shapeDirty = true;
		HX_STACK_LINE(192)
		this->filterDirty = true;
	}
return null();
}


Void FlxShapeLightning_obj::fixBoundaries( Float trueWidth,Float trueHeight){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","fixBoundaries",0x21caade3,"flixel.addons.display.shapes.FlxShapeLightning.fixBoundaries","flixel/addons/display/shapes/FlxShapeLightning.hx",196,0xdb65696a)
		HX_STACK_THIS(this)
		HX_STACK_ARG(trueWidth,"trueWidth")
		HX_STACK_ARG(trueHeight,"trueHeight")
	}
return null();
}


Void FlxShapeLightning_obj::draw( ){
{
		HX_STACK_FRAME("flixel.addons.display.shapes.FlxShapeLightning","draw",0x5d38a0de,"flixel.addons.display.shapes.FlxShapeLightning.draw","flixel/addons/display/shapes/FlxShapeLightning.hx",202,0xdb65696a)
		HX_STACK_THIS(this)
		HX_STACK_LINE(203)
		this->super::draw();
		HX_STACK_LINE(205)
		if ((this->filterDirty)){
			HX_STACK_LINE(207)
			if (((this->lightningStyle->__Field(HX_CSTRING("halo_colors"),true) == null()))){
				HX_STACK_LINE(209)
				this->filterDirty = false;
				HX_STACK_LINE(210)
				return null();
			}
			HX_STACK_LINE(213)
			int sizeInc = (this->lightningStyle->__Field(HX_CSTRING("halo_colors"),true)->__Field(HX_CSTRING("length"),true) * (int)3);		HX_STACK_VAR(sizeInc,"sizeInc");
			HX_STACK_LINE(215)
			int i = (int)0;		HX_STACK_VAR(i,"i");
			HX_STACK_LINE(216)
			Array< ::Dynamic > a = Array_obj< ::Dynamic >::__new();		HX_STACK_VAR(a,"a");
			HX_STACK_LINE(217)
			{
				HX_STACK_LINE(217)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(217)
				Array< int > _g1 = this->lightningStyle->__Field(HX_CSTRING("halo_colors"),true);		HX_STACK_VAR(_g1,"_g1");
				HX_STACK_LINE(217)
				while((true)){
					HX_STACK_LINE(217)
					if ((!(((_g < _g1->length))))){
						HX_STACK_LINE(217)
						break;
					}
					HX_STACK_LINE(217)
					int halo_color = _g1->__get(_g);		HX_STACK_VAR(halo_color,"halo_color");
					HX_STACK_LINE(217)
					++(_g);
					HX_STACK_LINE(219)
					::openfl::filters::GlowFilter _g2 = ::openfl::filters::GlowFilter_obj::__new(halo_color,(1.0 - (0.15 * i)),(int)3,(int)3,null(),null(),null(),null());		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(219)
					a->push(_g2);
					HX_STACK_LINE(220)
					(i)++;
				}
			}
			HX_STACK_LINE(223)
			{
				HX_STACK_LINE(223)
				int _g = (int)0;		HX_STACK_VAR(_g,"_g");
				HX_STACK_LINE(223)
				while((true)){
					HX_STACK_LINE(223)
					if ((!(((_g < a->length))))){
						HX_STACK_LINE(223)
						break;
					}
					HX_STACK_LINE(223)
					::openfl::filters::GlowFilter gf = a->__get(_g).StaticCast< ::openfl::filters::GlowFilter >();		HX_STACK_VAR(gf,"gf");
					HX_STACK_LINE(223)
					++(_g);
					HX_STACK_LINE(225)
					::openfl::display::BitmapData pixels2 = this->get_pixels()->clone();		HX_STACK_VAR(pixels2,"pixels2");
					HX_STACK_LINE(226)
					::openfl::display::BitmapData _g1 = this->get_pixels();		HX_STACK_VAR(_g1,"_g1");
					HX_STACK_LINE(226)
					::openfl::geom::Rectangle _g2 = this->get_pixels()->get_rect();		HX_STACK_VAR(_g2,"_g2");
					HX_STACK_LINE(226)
					pixels2->applyFilter(_g1,_g2,this->_flashPointZero,gf);
					HX_STACK_LINE(229)
					Float w = this->get_width();		HX_STACK_VAR(w,"w");
					HX_STACK_LINE(230)
					Float h = this->get_height();		HX_STACK_VAR(h,"h");
					HX_STACK_LINE(231)
					Float ox = this->offset->x;		HX_STACK_VAR(ox,"ox");
					HX_STACK_LINE(232)
					Float oy = this->offset->y;		HX_STACK_VAR(oy,"oy");
					HX_STACK_LINE(235)
					this->set_pixels(pixels2);
					HX_STACK_LINE(238)
					this->set_width(w);
					HX_STACK_LINE(239)
					this->set_height(h);
					HX_STACK_LINE(240)
					this->offset->set_x(ox);
					HX_STACK_LINE(241)
					this->offset->set_y(oy);
				}
			}
			HX_STACK_LINE(244)
			this->filterDirty = false;
		}
	}
return null();
}



FlxShapeLightning_obj::FlxShapeLightning_obj()
{
}

void FlxShapeLightning_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxShapeLightning);
	HX_MARK_MEMBER_NAME(lightningStyle,"lightningStyle");
	HX_MARK_MEMBER_NAME(halo_cols,"halo_cols");
	HX_MARK_MEMBER_NAME(detail,"detail");
	HX_MARK_MEMBER_NAME(magnitude,"magnitude");
	HX_MARK_MEMBER_NAME(list_segs,"list_segs");
	HX_MARK_MEMBER_NAME(list_branch,"list_branch");
	HX_MARK_MEMBER_NAME(filterDirty,"filterDirty");
	::flixel::addons::display::shapes::FlxShapeLine_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxShapeLightning_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(lightningStyle,"lightningStyle");
	HX_VISIT_MEMBER_NAME(halo_cols,"halo_cols");
	HX_VISIT_MEMBER_NAME(detail,"detail");
	HX_VISIT_MEMBER_NAME(magnitude,"magnitude");
	HX_VISIT_MEMBER_NAME(list_segs,"list_segs");
	HX_VISIT_MEMBER_NAME(list_branch,"list_branch");
	HX_VISIT_MEMBER_NAME(filterDirty,"filterDirty");
	::flixel::addons::display::shapes::FlxShapeLine_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxShapeLightning_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"draw") ) { return draw_dyn(); }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"detail") ) { return detail; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"halo_cols") ) { return halo_cols; }
		if (HX_FIELD_EQ(inName,"magnitude") ) { return magnitude; }
		if (HX_FIELD_EQ(inName,"list_segs") ) { return list_segs; }
		if (HX_FIELD_EQ(inName,"calculate") ) { return calculate_dyn(); }
		break;
	case 10:
		if (HX_FIELD_EQ(inName,"addSegment") ) { return addSegment_dyn(); }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"list_branch") ) { return list_branch; }
		if (HX_FIELD_EQ(inName,"filterDirty") ) { return filterDirty; }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"copyLineStyle") ) { return copyLineStyle_dyn(); }
		if (HX_FIELD_EQ(inName,"fixBoundaries") ) { return fixBoundaries_dyn(); }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"lightningStyle") ) { return lightningStyle; }
		break;
	case 17:
		if (HX_FIELD_EQ(inName,"drawSpecificShape") ) { return drawSpecificShape_dyn(); }
		break;
	case 18:
		if (HX_FIELD_EQ(inName,"set_lightningStyle") ) { return set_lightningStyle_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxShapeLightning_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"detail") ) { detail=inValue.Cast< Float >(); return inValue; }
		break;
	case 9:
		if (HX_FIELD_EQ(inName,"halo_cols") ) { halo_cols=inValue.Cast< Dynamic >(); return inValue; }
		if (HX_FIELD_EQ(inName,"magnitude") ) { magnitude=inValue.Cast< Float >(); return inValue; }
		if (HX_FIELD_EQ(inName,"list_segs") ) { list_segs=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		break;
	case 11:
		if (HX_FIELD_EQ(inName,"list_branch") ) { list_branch=inValue.Cast< Array< ::Dynamic > >(); return inValue; }
		if (HX_FIELD_EQ(inName,"filterDirty") ) { filterDirty=inValue.Cast< bool >(); return inValue; }
		break;
	case 14:
		if (HX_FIELD_EQ(inName,"lightningStyle") ) { if (inCallProp) return set_lightningStyle(inValue);lightningStyle=inValue.Cast< Dynamic >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxShapeLightning_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("lightningStyle"));
	outFields->push(HX_CSTRING("halo_cols"));
	outFields->push(HX_CSTRING("detail"));
	outFields->push(HX_CSTRING("magnitude"));
	outFields->push(HX_CSTRING("list_segs"));
	outFields->push(HX_CSTRING("list_branch"));
	outFields->push(HX_CSTRING("filterDirty"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxShapeLightning_obj,lightningStyle),HX_CSTRING("lightningStyle")},
	{hx::fsObject /*Dynamic*/ ,(int)offsetof(FlxShapeLightning_obj,halo_cols),HX_CSTRING("halo_cols")},
	{hx::fsFloat,(int)offsetof(FlxShapeLightning_obj,detail),HX_CSTRING("detail")},
	{hx::fsFloat,(int)offsetof(FlxShapeLightning_obj,magnitude),HX_CSTRING("magnitude")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(FlxShapeLightning_obj,list_segs),HX_CSTRING("list_segs")},
	{hx::fsObject /*Array< ::Dynamic >*/ ,(int)offsetof(FlxShapeLightning_obj,list_branch),HX_CSTRING("list_branch")},
	{hx::fsBool,(int)offsetof(FlxShapeLightning_obj,filterDirty),HX_CSTRING("filterDirty")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("lightningStyle"),
	HX_CSTRING("halo_cols"),
	HX_CSTRING("detail"),
	HX_CSTRING("magnitude"),
	HX_CSTRING("list_segs"),
	HX_CSTRING("list_branch"),
	HX_CSTRING("filterDirty"),
	HX_CSTRING("addSegment"),
	HX_CSTRING("calculate"),
	HX_CSTRING("set_lightningStyle"),
	HX_CSTRING("copyLineStyle"),
	HX_CSTRING("drawSpecificShape"),
	HX_CSTRING("fixBoundaries"),
	HX_CSTRING("draw"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxShapeLightning_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxShapeLightning_obj::__mClass,"__mClass");
};

#endif

Class FlxShapeLightning_obj::__mClass;

void FlxShapeLightning_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.shapes.FlxShapeLightning"), hx::TCanCast< FlxShapeLightning_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxShapeLightning_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes
