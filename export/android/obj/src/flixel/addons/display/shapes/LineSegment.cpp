#include <hxcpp.h>

#ifndef INCLUDED_flixel_addons_display_shapes_LineSegment
#include <flixel/addons/display/shapes/LineSegment.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxPoint
#include <flixel/util/FlxPool_flixel_util_FlxPoint.h>
#endif
namespace flixel{
namespace addons{
namespace display{
namespace shapes{

Void LineSegment_obj::__construct(::flixel::util::FlxPoint A,::flixel::util::FlxPoint B)
{
HX_STACK_FRAME("flixel.addons.display.shapes.LineSegment","new",0xb47f0e6a,"flixel.addons.display.shapes.LineSegment.new","flixel/addons/display/shapes/FlxShapeLightning.hx",267,0xdb65696a)
HX_STACK_THIS(this)
HX_STACK_ARG(A,"A")
HX_STACK_ARG(B,"B")
{
	HX_STACK_LINE(268)
	::flixel::util::FlxPoint _g;		HX_STACK_VAR(_g,"_g");
	HX_STACK_LINE(268)
	{
		HX_STACK_LINE(268)
		::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set(A->x,A->y);		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(268)
		point->_inPool = false;
		HX_STACK_LINE(268)
		_g = point;
	}
	HX_STACK_LINE(268)
	this->a = _g;
	HX_STACK_LINE(269)
	::flixel::util::FlxPoint _g1;		HX_STACK_VAR(_g1,"_g1");
	HX_STACK_LINE(269)
	{
		HX_STACK_LINE(269)
		::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set(B->x,B->y);		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(269)
		point->_inPool = false;
		HX_STACK_LINE(269)
		_g1 = point;
	}
	HX_STACK_LINE(269)
	this->b = _g1;
	HX_STACK_LINE(270)
	if ((A->_weak)){
		HX_STACK_LINE(270)
		A->put();
	}
	HX_STACK_LINE(271)
	if ((B->_weak)){
		HX_STACK_LINE(271)
		B->put();
	}
}
;
	return null();
}

//LineSegment_obj::~LineSegment_obj() { }

Dynamic LineSegment_obj::__CreateEmpty() { return  new LineSegment_obj; }
hx::ObjectPtr< LineSegment_obj > LineSegment_obj::__new(::flixel::util::FlxPoint A,::flixel::util::FlxPoint B)
{  hx::ObjectPtr< LineSegment_obj > result = new LineSegment_obj();
	result->__construct(A,B);
	return result;}

Dynamic LineSegment_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< LineSegment_obj > result = new LineSegment_obj();
	result->__construct(inArgs[0],inArgs[1]);
	return result;}

::flixel::addons::display::shapes::LineSegment LineSegment_obj::copy( ){
	HX_STACK_FRAME("flixel.addons.display.shapes.LineSegment","copy",0x336fc66b,"flixel.addons.display.shapes.LineSegment.copy","flixel/addons/display/shapes/FlxShapeLightning.hx",276,0xdb65696a)
	HX_STACK_THIS(this)
	HX_STACK_LINE(276)
	return ::flixel::addons::display::shapes::LineSegment_obj::__new(this->a,this->b);
}


HX_DEFINE_DYNAMIC_FUNC0(LineSegment_obj,copy,return )


LineSegment_obj::LineSegment_obj()
{
}

void LineSegment_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(LineSegment);
	HX_MARK_MEMBER_NAME(a,"a");
	HX_MARK_MEMBER_NAME(b,"b");
	HX_MARK_END_CLASS();
}

void LineSegment_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(a,"a");
	HX_VISIT_MEMBER_NAME(b,"b");
}

Dynamic LineSegment_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 1:
		if (HX_FIELD_EQ(inName,"a") ) { return a; }
		if (HX_FIELD_EQ(inName,"b") ) { return b; }
		break;
	case 4:
		if (HX_FIELD_EQ(inName,"copy") ) { return copy_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic LineSegment_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 1:
		if (HX_FIELD_EQ(inName,"a") ) { a=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
		if (HX_FIELD_EQ(inName,"b") ) { b=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void LineSegment_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("a"));
	outFields->push(HX_CSTRING("b"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(LineSegment_obj,a),HX_CSTRING("a")},
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(LineSegment_obj,b),HX_CSTRING("b")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("a"),
	HX_CSTRING("b"),
	HX_CSTRING("copy"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(LineSegment_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(LineSegment_obj::__mClass,"__mClass");
};

#endif

Class LineSegment_obj::__mClass;

void LineSegment_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.shapes.LineSegment"), hx::TCanCast< LineSegment_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void LineSegment_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes
