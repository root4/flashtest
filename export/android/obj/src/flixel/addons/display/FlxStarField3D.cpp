#include <hxcpp.h>

#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_addons_display_FlxStarField3D
#include <flixel/addons/display/FlxStarField3D.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStar
#include <flixel/addons/display/_FlxStarField/FlxStar.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStarField
#include <flixel/addons/display/_FlxStarField/FlxStarField.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_util_FlxDestroyUtil
#include <flixel/util/FlxDestroyUtil.h>
#endif
#ifndef INCLUDED_flixel_util_FlxGradient
#include <flixel/util/FlxGradient.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPool_flixel_util_FlxPoint
#include <flixel/util/FlxPool_flixel_util_FlxPoint.h>
#endif
#ifndef INCLUDED_flixel_util_FlxRandom
#include <flixel/util/FlxRandom.h>
#endif
#ifndef INCLUDED_hxMath
#include <hxMath.h>
#endif
namespace flixel{
namespace addons{
namespace display{

Void FlxStarField3D_obj::__construct(hx::Null< int >  __o_X,hx::Null< int >  __o_Y,hx::Null< int >  __o_Width,hx::Null< int >  __o_Height,hx::Null< int >  __o_StarAmount)
{
HX_STACK_FRAME("flixel.addons.display.FlxStarField3D","new",0xfdd9332e,"flixel.addons.display.FlxStarField3D.new","flixel/addons/display/FlxStarField.hx",69,0xb2ef3fd2)
HX_STACK_THIS(this)
HX_STACK_ARG(__o_X,"X")
HX_STACK_ARG(__o_Y,"Y")
HX_STACK_ARG(__o_Width,"Width")
HX_STACK_ARG(__o_Height,"Height")
HX_STACK_ARG(__o_StarAmount,"StarAmount")
int X = __o_X.Default(0);
int Y = __o_Y.Default(0);
int Width = __o_Width.Default(0);
int Height = __o_Height.Default(0);
int StarAmount = __o_StarAmount.Default(300);
{
	HX_STACK_LINE(70)
	super::__construct(X,Y,Width,Height,StarAmount);
	HX_STACK_LINE(71)
	::flixel::util::FlxPoint _g2;		HX_STACK_VAR(_g2,"_g2");
	HX_STACK_LINE(71)
	{
		HX_STACK_LINE(71)
		Float _g = this->get_width();		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(71)
		Float X1 = (Float(_g) / Float((int)2));		HX_STACK_VAR(X1,"X1");
		HX_STACK_LINE(71)
		Float _g1 = this->get_height();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(71)
		Float Y1 = (Float(_g1) / Float((int)2));		HX_STACK_VAR(Y1,"Y1");
		HX_STACK_LINE(71)
		::flixel::util::FlxPoint point = ::flixel::util::FlxPoint_obj::_pool->get()->set(X1,Y1);		HX_STACK_VAR(point,"point");
		HX_STACK_LINE(71)
		point->_inPool = false;
		HX_STACK_LINE(71)
		_g2 = point;
	}
	HX_STACK_LINE(71)
	this->center = _g2;
	HX_STACK_LINE(72)
	{
		HX_STACK_LINE(72)
		Array< int > _g3 = ::flixel::util::FlxGradient_obj::createGradientArray((int)1,(int)300,Array_obj< int >::__new().Add((int)-14079703).Add((int)-1),null(),null(),null());		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(72)
		this->_depthColors = _g3;
	}
	HX_STACK_LINE(73)
	this->setStarSpeed((int)0,(int)200);
}
;
	return null();
}

//FlxStarField3D_obj::~FlxStarField3D_obj() { }

Dynamic FlxStarField3D_obj::__CreateEmpty() { return  new FlxStarField3D_obj; }
hx::ObjectPtr< FlxStarField3D_obj > FlxStarField3D_obj::__new(hx::Null< int >  __o_X,hx::Null< int >  __o_Y,hx::Null< int >  __o_Width,hx::Null< int >  __o_Height,hx::Null< int >  __o_StarAmount)
{  hx::ObjectPtr< FlxStarField3D_obj > result = new FlxStarField3D_obj();
	result->__construct(__o_X,__o_Y,__o_Width,__o_Height,__o_StarAmount);
	return result;}

Dynamic FlxStarField3D_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< FlxStarField3D_obj > result = new FlxStarField3D_obj();
	result->__construct(inArgs[0],inArgs[1],inArgs[2],inArgs[3],inArgs[4]);
	return result;}

Void FlxStarField3D_obj::destroy( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxStarField3D","destroy",0xa9f00ec8,"flixel.addons.display.FlxStarField3D.destroy","flixel/addons/display/FlxStarField.hx",77,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(78)
		::flixel::util::FlxPoint _g = ::flixel::util::FlxDestroyUtil_obj::put(this->center);		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(78)
		this->center = _g;
		HX_STACK_LINE(79)
		this->super::destroy();
	}
return null();
}


Void FlxStarField3D_obj::update( ){
{
		HX_STACK_FRAME("flixel.addons.display.FlxStarField3D","update",0x59b357fb,"flixel.addons.display.FlxStarField3D.update","flixel/addons/display/FlxStarField.hx",83,0xb2ef3fd2)
		HX_STACK_THIS(this)
		HX_STACK_LINE(84)
		{
			HX_STACK_LINE(84)
			int _g = (int)0;		HX_STACK_VAR(_g,"_g");
			HX_STACK_LINE(84)
			Array< ::Dynamic > _g1 = this->_stars;		HX_STACK_VAR(_g1,"_g1");
			HX_STACK_LINE(84)
			while((true)){
				HX_STACK_LINE(84)
				if ((!(((_g < _g1->length))))){
					HX_STACK_LINE(84)
					break;
				}
				HX_STACK_LINE(84)
				::flixel::addons::display::_FlxStarField::FlxStar star = _g1->__get(_g).StaticCast< ::flixel::addons::display::_FlxStarField::FlxStar >();		HX_STACK_VAR(star,"star");
				HX_STACK_LINE(84)
				++(_g);
				HX_STACK_LINE(86)
				hx::MultEq(star->d,1.1);
				HX_STACK_LINE(87)
				Float _g2 = ::Math_obj::cos(star->r);		HX_STACK_VAR(_g2,"_g2");
				HX_STACK_LINE(87)
				Float _g11 = (_g2 * star->d);		HX_STACK_VAR(_g11,"_g11");
				HX_STACK_LINE(87)
				Float _g21 = (_g11 * star->speed);		HX_STACK_VAR(_g21,"_g21");
				HX_STACK_LINE(87)
				Float _g3 = (_g21 * ::flixel::FlxG_obj::elapsed);		HX_STACK_VAR(_g3,"_g3");
				HX_STACK_LINE(87)
				Float _g4 = (this->center->x + _g3);		HX_STACK_VAR(_g4,"_g4");
				HX_STACK_LINE(87)
				star->x = _g4;
				HX_STACK_LINE(88)
				Float _g5 = ::Math_obj::sin(star->r);		HX_STACK_VAR(_g5,"_g5");
				HX_STACK_LINE(88)
				Float _g6 = (_g5 * star->d);		HX_STACK_VAR(_g6,"_g6");
				HX_STACK_LINE(88)
				Float _g7 = (_g6 * star->speed);		HX_STACK_VAR(_g7,"_g7");
				HX_STACK_LINE(88)
				Float _g8 = (_g7 * ::flixel::FlxG_obj::elapsed);		HX_STACK_VAR(_g8,"_g8");
				HX_STACK_LINE(88)
				Float _g9 = (this->center->y + _g8);		HX_STACK_VAR(_g9,"_g9");
				HX_STACK_LINE(88)
				star->y = _g9;
				struct _Function_3_1{
					inline static bool Block( hx::ObjectPtr< ::flixel::addons::display::FlxStarField3D_obj > __this,::flixel::addons::display::_FlxStarField::FlxStar &star){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/FlxStarField.hx",90,0xb2ef3fd2)
						{
							HX_STACK_LINE(90)
							Float _g10 = __this->get_width();		HX_STACK_VAR(_g10,"_g10");
							HX_STACK_LINE(90)
							return (star->x > _g10);
						}
						return null();
					}
				};
				struct _Function_3_2{
					inline static bool Block( hx::ObjectPtr< ::flixel::addons::display::FlxStarField3D_obj > __this,::flixel::addons::display::_FlxStarField::FlxStar &star){
						HX_STACK_FRAME("*","closure",0x5bdab937,"*.closure","flixel/addons/display/FlxStarField.hx",90,0xb2ef3fd2)
						{
							HX_STACK_LINE(90)
							Float _g111 = __this->get_height();		HX_STACK_VAR(_g111,"_g111");
							HX_STACK_LINE(90)
							return (star->y > _g111);
						}
						return null();
					}
				};
				HX_STACK_LINE(90)
				if (((  ((!(((  ((!(((  ((!(((star->x < (int)0))))) ? bool(_Function_3_1::Block(this,star)) : bool(true) ))))) ? bool((star->y < (int)0)) : bool(true) ))))) ? bool(_Function_3_2::Block(this,star)) : bool(true) ))){
					HX_STACK_LINE(92)
					star->d = (int)1;
					HX_STACK_LINE(93)
					int _g12 = ::flixel::util::FlxRandom_obj::_internalSeed = (int(hx::Mod((::flixel::util::FlxRandom_obj::_internalSeed * (int)48271),(int)2147483647)) & int((int)2147483647));		HX_STACK_VAR(_g12,"_g12");
					HX_STACK_LINE(93)
					Float _g13 = (Float(_g12) / Float((int)2147483647));		HX_STACK_VAR(_g13,"_g13");
					HX_STACK_LINE(93)
					Float _g14 = (_g13 * ::Math_obj::PI);		HX_STACK_VAR(_g14,"_g14");
					HX_STACK_LINE(93)
					Float _g15 = (_g14 * (int)2);		HX_STACK_VAR(_g15,"_g15");
					HX_STACK_LINE(93)
					star->r = _g15;
					HX_STACK_LINE(94)
					star->x = (int)0;
					HX_STACK_LINE(95)
					star->y = (int)0;
					HX_STACK_LINE(96)
					Float _g16 = ::flixel::util::FlxRandom_obj::floatRanged(this->_minSpeed,this->_maxSpeed,null());		HX_STACK_VAR(_g16,"_g16");
					HX_STACK_LINE(96)
					star->speed = _g16;
					HX_STACK_LINE(98)
					this->_stars[star->index] = star;
				}
			}
		}
		HX_STACK_LINE(102)
		this->super::update();
	}
return null();
}



FlxStarField3D_obj::FlxStarField3D_obj()
{
}

void FlxStarField3D_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(FlxStarField3D);
	HX_MARK_MEMBER_NAME(center,"center");
	::flixel::addons::display::_FlxStarField::FlxStarField_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void FlxStarField3D_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(center,"center");
	::flixel::addons::display::_FlxStarField::FlxStarField_obj::__Visit(HX_VISIT_ARG);
}

Dynamic FlxStarField3D_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"center") ) { return center; }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic FlxStarField3D_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 6:
		if (HX_FIELD_EQ(inName,"center") ) { center=inValue.Cast< ::flixel::util::FlxPoint >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void FlxStarField3D_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("center"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::util::FlxPoint*/ ,(int)offsetof(FlxStarField3D_obj,center),HX_CSTRING("center")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("center"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(FlxStarField3D_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(FlxStarField3D_obj::__mClass,"__mClass");
};

#endif

Class FlxStarField3D_obj::__mClass;

void FlxStarField3D_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("flixel.addons.display.FlxStarField3D"), hx::TCanCast< FlxStarField3D_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void FlxStarField3D_obj::__boot()
{
}

} // end namespace flixel
} // end namespace addons
} // end namespace display
