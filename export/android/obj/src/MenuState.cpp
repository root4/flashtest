#include <hxcpp.h>

#ifndef INCLUDED_MenuState
#include <MenuState.h>
#endif
#ifndef INCLUDED_PlayState
#include <PlayState.h>
#endif
#ifndef INCLUDED_SettingState
#include <SettingState.h>
#endif
#ifndef INCLUDED_flixel_FlxBasic
#include <flixel/FlxBasic.h>
#endif
#ifndef INCLUDED_flixel_FlxG
#include <flixel/FlxG.h>
#endif
#ifndef INCLUDED_flixel_FlxGame
#include <flixel/FlxGame.h>
#endif
#ifndef INCLUDED_flixel_FlxObject
#include <flixel/FlxObject.h>
#endif
#ifndef INCLUDED_flixel_FlxSprite
#include <flixel/FlxSprite.h>
#endif
#ifndef INCLUDED_flixel_FlxState
#include <flixel/FlxState.h>
#endif
#ifndef INCLUDED_flixel_addons_display_FlxStarField2D
#include <flixel/addons/display/FlxStarField2D.h>
#endif
#ifndef INCLUDED_flixel_addons_display__FlxStarField_FlxStarField
#include <flixel/addons/display/_FlxStarField/FlxStarField.h>
#endif
#ifndef INCLUDED_flixel_group_FlxGroup
#include <flixel/group/FlxGroup.h>
#endif
#ifndef INCLUDED_flixel_group_FlxTypedGroup
#include <flixel/group/FlxTypedGroup.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxDestroyable
#include <flixel/interfaces/IFlxDestroyable.h>
#endif
#ifndef INCLUDED_flixel_interfaces_IFlxPooled
#include <flixel/interfaces/IFlxPooled.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxButton
#include <flixel/ui/FlxButton.h>
#endif
#ifndef INCLUDED_flixel_ui_FlxTypedButton
#include <flixel/ui/FlxTypedButton.h>
#endif
#ifndef INCLUDED_flixel_util_FlxPoint
#include <flixel/util/FlxPoint.h>
#endif
#ifndef INCLUDED_openfl_display_DisplayObject
#include <openfl/display/DisplayObject.h>
#endif
#ifndef INCLUDED_openfl_display_DisplayObjectContainer
#include <openfl/display/DisplayObjectContainer.h>
#endif
#ifndef INCLUDED_openfl_display_IBitmapDrawable
#include <openfl/display/IBitmapDrawable.h>
#endif
#ifndef INCLUDED_openfl_display_InteractiveObject
#include <openfl/display/InteractiveObject.h>
#endif
#ifndef INCLUDED_openfl_display_Sprite
#include <openfl/display/Sprite.h>
#endif
#ifndef INCLUDED_openfl_events_EventDispatcher
#include <openfl/events/EventDispatcher.h>
#endif
#ifndef INCLUDED_openfl_events_IEventDispatcher
#include <openfl/events/IEventDispatcher.h>
#endif
#ifndef INCLUDED_openfl_system_System
#include <openfl/system/System.h>
#endif

Void MenuState_obj::__construct(Dynamic MaxSize)
{
HX_STACK_FRAME("MenuState","new",0xe563b1c4,"MenuState.new","MenuState.hx",17,0xdfbcb22c)
HX_STACK_THIS(this)
HX_STACK_ARG(MaxSize,"MaxSize")
{
	HX_STACK_LINE(17)
	super::__construct(MaxSize);
}
;
	return null();
}

//MenuState_obj::~MenuState_obj() { }

Dynamic MenuState_obj::__CreateEmpty() { return  new MenuState_obj; }
hx::ObjectPtr< MenuState_obj > MenuState_obj::__new(Dynamic MaxSize)
{  hx::ObjectPtr< MenuState_obj > result = new MenuState_obj();
	result->__construct(MaxSize);
	return result;}

Dynamic MenuState_obj::__Create(hx::DynamicArray inArgs)
{  hx::ObjectPtr< MenuState_obj > result = new MenuState_obj();
	result->__construct(inArgs[0]);
	return result;}

Void MenuState_obj::create( ){
{
		HX_STACK_FRAME("MenuState","create",0xe57b7c18,"MenuState.create","MenuState.hx",29,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(30)
		this->super::create();
		HX_STACK_LINE(33)
		::flixel::ui::FlxButton _g = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Start"),this->gotoPlayState_dyn());		HX_STACK_VAR(_g,"_g");
		HX_STACK_LINE(33)
		this->start = _g;
		HX_STACK_LINE(34)
		{
			HX_STACK_LINE(34)
			::flixel::ui::FlxButton _this = this->start;		HX_STACK_VAR(_this,"_this");
			HX_STACK_LINE(34)
			_this->origin->set((_this->frameWidth * 0.5),(_this->frameHeight * 0.5));
		}
		HX_STACK_LINE(35)
		Float _g1 = this->start->get_width();		HX_STACK_VAR(_g1,"_g1");
		HX_STACK_LINE(35)
		Float _g2 = (::flixel::FlxG_obj::width - _g1);		HX_STACK_VAR(_g2,"_g2");
		HX_STACK_LINE(35)
		Float _g3 = (Float(_g2) / Float((int)2));		HX_STACK_VAR(_g3,"_g3");
		HX_STACK_LINE(35)
		this->start->setPosition(_g3,(int)50);
		HX_STACK_LINE(37)
		::flixel::ui::FlxButton _g4 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Settings"),this->gotoSettingState_dyn());		HX_STACK_VAR(_g4,"_g4");
		HX_STACK_LINE(37)
		this->settings = _g4;
		HX_STACK_LINE(38)
		{
			HX_STACK_LINE(38)
			::flixel::ui::FlxButton _this = this->settings;		HX_STACK_VAR(_this,"_this");
			HX_STACK_LINE(38)
			_this->origin->set((_this->frameWidth * 0.5),(_this->frameHeight * 0.5));
		}
		HX_STACK_LINE(39)
		Float _g5 = this->settings->get_width();		HX_STACK_VAR(_g5,"_g5");
		HX_STACK_LINE(39)
		Float _g6 = (::flixel::FlxG_obj::width - _g5);		HX_STACK_VAR(_g6,"_g6");
		HX_STACK_LINE(39)
		Float _g7 = (Float(_g6) / Float((int)2));		HX_STACK_VAR(_g7,"_g7");
		HX_STACK_LINE(39)
		this->settings->setPosition(_g7,(int)100);
		HX_STACK_LINE(41)
		::flixel::ui::FlxButton _g8 = ::flixel::ui::FlxButton_obj::__new((int)0,(int)0,HX_CSTRING("Exit"),this->gotoExit_dyn());		HX_STACK_VAR(_g8,"_g8");
		HX_STACK_LINE(41)
		this->exit = _g8;
		HX_STACK_LINE(42)
		{
			HX_STACK_LINE(42)
			::flixel::ui::FlxButton _this = this->exit;		HX_STACK_VAR(_this,"_this");
			HX_STACK_LINE(42)
			_this->origin->set((_this->frameWidth * 0.5),(_this->frameHeight * 0.5));
		}
		HX_STACK_LINE(43)
		Float _g9 = this->exit->get_width();		HX_STACK_VAR(_g9,"_g9");
		HX_STACK_LINE(43)
		Float _g10 = (::flixel::FlxG_obj::width - _g9);		HX_STACK_VAR(_g10,"_g10");
		HX_STACK_LINE(43)
		Float _g11 = (Float(_g10) / Float((int)2));		HX_STACK_VAR(_g11,"_g11");
		HX_STACK_LINE(43)
		this->exit->setPosition(_g11,(int)150);
		HX_STACK_LINE(46)
		::flixel::addons::display::FlxStarField2D _g12 = ::flixel::addons::display::FlxStarField2D_obj::__new((int)0,(int)0,::flixel::FlxG_obj::width,::flixel::FlxG_obj::height,(int)100);		HX_STACK_VAR(_g12,"_g12");
		HX_STACK_LINE(46)
		this->stars = _g12;
		HX_STACK_LINE(49)
		this->add(this->stars);
		HX_STACK_LINE(50)
		this->add(this->start);
		HX_STACK_LINE(51)
		this->add(this->exit);
		HX_STACK_LINE(52)
		this->add(this->settings);
	}
return null();
}


Void MenuState_obj::gotoPlayState( ){
{
		HX_STACK_FRAME("MenuState","gotoPlayState",0xdf3d23be,"MenuState.gotoPlayState","MenuState.hx",57,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(57)
		::flixel::FlxState State = ::PlayState_obj::__new(null());		HX_STACK_VAR(State,"State");
		HX_STACK_LINE(57)
		::flixel::FlxG_obj::game->_requestedState = State;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,gotoPlayState,(void))

Void MenuState_obj::gotoSettingState( ){
{
		HX_STACK_FRAME("MenuState","gotoSettingState",0xebdeb8c0,"MenuState.gotoSettingState","MenuState.hx",60,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(60)
		::flixel::FlxState State = ::SettingState_obj::__new();		HX_STACK_VAR(State,"State");
		HX_STACK_LINE(60)
		::flixel::FlxG_obj::game->_requestedState = State;
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,gotoSettingState,(void))

Void MenuState_obj::gotoExit( ){
{
		HX_STACK_FRAME("MenuState","gotoExit",0x30625e7d,"MenuState.gotoExit","MenuState.hx",63,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(63)
		::openfl::system::System_obj::exit((int)0);
	}
return null();
}


HX_DEFINE_DYNAMIC_FUNC0(MenuState_obj,gotoExit,(void))

Void MenuState_obj::destroy( ){
{
		HX_STACK_FRAME("MenuState","destroy",0xf9ac905e,"MenuState.destroy","MenuState.hx",71,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(74)
		this->exit->destroy();
		HX_STACK_LINE(75)
		this->exit = null();
		HX_STACK_LINE(76)
		this->start->destroy();
		HX_STACK_LINE(77)
		this->start = null();
		HX_STACK_LINE(78)
		this->settings->destroy();
		HX_STACK_LINE(79)
		this->settings = null();
		HX_STACK_LINE(80)
		this->super::destroy();
	}
return null();
}


Void MenuState_obj::update( ){
{
		HX_STACK_FRAME("MenuState","update",0xf0719b25,"MenuState.update","MenuState.hx",88,0xdfbcb22c)
		HX_STACK_THIS(this)
		HX_STACK_LINE(88)
		this->super::update();
	}
return null();
}



MenuState_obj::MenuState_obj()
{
}

void MenuState_obj::__Mark(HX_MARK_PARAMS)
{
	HX_MARK_BEGIN_CLASS(MenuState);
	HX_MARK_MEMBER_NAME(start,"start");
	HX_MARK_MEMBER_NAME(settings,"settings");
	HX_MARK_MEMBER_NAME(exit,"exit");
	HX_MARK_MEMBER_NAME(stars,"stars");
	::flixel::FlxState_obj::__Mark(HX_MARK_ARG);
	HX_MARK_END_CLASS();
}

void MenuState_obj::__Visit(HX_VISIT_PARAMS)
{
	HX_VISIT_MEMBER_NAME(start,"start");
	HX_VISIT_MEMBER_NAME(settings,"settings");
	HX_VISIT_MEMBER_NAME(exit,"exit");
	HX_VISIT_MEMBER_NAME(stars,"stars");
	::flixel::FlxState_obj::__Visit(HX_VISIT_ARG);
}

Dynamic MenuState_obj::__Field(const ::String &inName,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"exit") ) { return exit; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"start") ) { return start; }
		if (HX_FIELD_EQ(inName,"stars") ) { return stars; }
		break;
	case 6:
		if (HX_FIELD_EQ(inName,"create") ) { return create_dyn(); }
		if (HX_FIELD_EQ(inName,"update") ) { return update_dyn(); }
		break;
	case 7:
		if (HX_FIELD_EQ(inName,"destroy") ) { return destroy_dyn(); }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"settings") ) { return settings; }
		if (HX_FIELD_EQ(inName,"gotoExit") ) { return gotoExit_dyn(); }
		break;
	case 13:
		if (HX_FIELD_EQ(inName,"gotoPlayState") ) { return gotoPlayState_dyn(); }
		break;
	case 16:
		if (HX_FIELD_EQ(inName,"gotoSettingState") ) { return gotoSettingState_dyn(); }
	}
	return super::__Field(inName,inCallProp);
}

Dynamic MenuState_obj::__SetField(const ::String &inName,const Dynamic &inValue,bool inCallProp)
{
	switch(inName.length) {
	case 4:
		if (HX_FIELD_EQ(inName,"exit") ) { exit=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		break;
	case 5:
		if (HX_FIELD_EQ(inName,"start") ) { start=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
		if (HX_FIELD_EQ(inName,"stars") ) { stars=inValue.Cast< ::flixel::addons::display::FlxStarField2D >(); return inValue; }
		break;
	case 8:
		if (HX_FIELD_EQ(inName,"settings") ) { settings=inValue.Cast< ::flixel::ui::FlxButton >(); return inValue; }
	}
	return super::__SetField(inName,inValue,inCallProp);
}

void MenuState_obj::__GetFields(Array< ::String> &outFields)
{
	outFields->push(HX_CSTRING("start"));
	outFields->push(HX_CSTRING("settings"));
	outFields->push(HX_CSTRING("exit"));
	outFields->push(HX_CSTRING("stars"));
	super::__GetFields(outFields);
};

static ::String sStaticFields[] = {
	String(null()) };

#if HXCPP_SCRIPTABLE
static hx::StorageInfo sMemberStorageInfo[] = {
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,start),HX_CSTRING("start")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,settings),HX_CSTRING("settings")},
	{hx::fsObject /*::flixel::ui::FlxButton*/ ,(int)offsetof(MenuState_obj,exit),HX_CSTRING("exit")},
	{hx::fsObject /*::flixel::addons::display::FlxStarField2D*/ ,(int)offsetof(MenuState_obj,stars),HX_CSTRING("stars")},
	{ hx::fsUnknown, 0, null()}
};
#endif

static ::String sMemberFields[] = {
	HX_CSTRING("start"),
	HX_CSTRING("settings"),
	HX_CSTRING("exit"),
	HX_CSTRING("stars"),
	HX_CSTRING("create"),
	HX_CSTRING("gotoPlayState"),
	HX_CSTRING("gotoSettingState"),
	HX_CSTRING("gotoExit"),
	HX_CSTRING("destroy"),
	HX_CSTRING("update"),
	String(null()) };

static void sMarkStatics(HX_MARK_PARAMS) {
	HX_MARK_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
};

#ifdef HXCPP_VISIT_ALLOCS
static void sVisitStatics(HX_VISIT_PARAMS) {
	HX_VISIT_MEMBER_NAME(MenuState_obj::__mClass,"__mClass");
};

#endif

Class MenuState_obj::__mClass;

void MenuState_obj::__register()
{
	hx::Static(__mClass) = hx::RegisterClass(HX_CSTRING("MenuState"), hx::TCanCast< MenuState_obj> ,sStaticFields,sMemberFields,
	&__CreateEmpty, &__Create,
	&super::__SGetClass(), 0, sMarkStatics
#ifdef HXCPP_VISIT_ALLOCS
    , sVisitStatics
#endif
#ifdef HXCPP_SCRIPTABLE
    , sMemberStorageInfo
#endif
);
}

void MenuState_obj::__boot()
{
}

