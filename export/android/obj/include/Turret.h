#ifndef INCLUDED_Turret
#define INCLUDED_Turret

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS0(Turret)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)


class HXCPP_CLASS_ATTRIBUTES  Turret_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef Turret_obj OBJ_;
		Turret_obj();
		Void __construct(int x,int y,::flixel::FlxState state,::String facingRight);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< Turret_obj > __new(int x,int y,::flixel::FlxState state,::String facingRight);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~Turret_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("Turret"); }

		Float startTimer;
		bool started;
		::flixel::group::FlxGroup bullet;
		::flixel::FlxState state;
		Float fireTimer;
		virtual Void update( );

		virtual Void start( );
		Dynamic start_dyn();

		virtual Void fire( );
		Dynamic fire_dyn();

		virtual ::flixel::group::FlxGroup getBullets( );
		Dynamic getBullets_dyn();

};


#endif /* INCLUDED_Turret */ 
