#ifndef INCLUDED_PlayState
#define INCLUDED_PlayState

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxState.h>
HX_DECLARE_CLASS0(FallingTile)
HX_DECLARE_CLASS0(PlayState)
HX_DECLARE_CLASS0(Player)
HX_DECLARE_CLASS0(Spikes)
HX_DECLARE_CLASS0(Turret)
HX_DECLARE_CLASS0(Xml)
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS1(flixel,FlxState)
HX_DECLARE_CLASS3(flixel,addons,display,FlxStarField2D)
HX_DECLARE_CLASS4(flixel,addons,display,_FlxStarField,FlxStarField)
HX_DECLARE_CLASS4(flixel,addons,editors,ogmo,FlxOgmoLoader)
HX_DECLARE_CLASS3(flixel,effects,particles,FlxEmitter)
HX_DECLARE_CLASS3(flixel,effects,particles,FlxParticle)
HX_DECLARE_CLASS3(flixel,effects,particles,FlxTypedEmitter)
HX_DECLARE_CLASS2(flixel,group,FlxGroup)
HX_DECLARE_CLASS2(flixel,group,FlxTypedGroup)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxBasic)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxParticle)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxSprite)
HX_DECLARE_CLASS2(flixel,text,FlxText)
HX_DECLARE_CLASS2(flixel,tile,FlxTilemap)
HX_DECLARE_CLASS2(flixel,util,FlxTimer)


class HXCPP_CLASS_ATTRIBUTES  PlayState_obj : public ::flixel::FlxState_obj{
	public:
		typedef ::flixel::FlxState_obj super;
		typedef PlayState_obj OBJ_;
		PlayState_obj();
		Void __construct(Dynamic MaxSize);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< PlayState_obj > __new(Dynamic MaxSize);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~PlayState_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("PlayState"); }

		::Player player;
		::flixel::FlxSprite floor;
		::flixel::addons::editors::ogmo::FlxOgmoLoader loader;
		::flixel::tile::FlxTilemap midgroundMap;
		::flixel::tile::FlxTilemap backgroundMap;
		::flixel::tile::FlxTilemap foregroundMap;
		::flixel::group::FlxGroup tileGroup;
		Array< ::Dynamic > turretGroup;
		::flixel::group::FlxGroup spikeGroup;
		int score;
		::flixel::text::FlxText scoreText;
		::flixel::addons::display::FlxStarField2D star;
		::flixel::FlxSprite darkness;
		::flixel::FlxSprite light;
		::flixel::tile::FlxTilemap falling;
		::flixel::effects::particles::FlxEmitter particleEmitter;
		::flixel::effects::particles::FlxParticle particle;
		::flixel::FlxSprite right;
		::flixel::FlxSprite left;
		::flixel::FlxSprite up;
		::flixel::util::FlxTimer buffer;
		::flixel::text::FlxText touchR;
		::flixel::text::FlxText touchL;
		virtual Void create( );

		virtual Void moveUp( );
		Dynamic moveUp_dyn();

		virtual Void moveLeft( );
		Dynamic moveLeft_dyn();

		virtual Void moveRight( );
		Dynamic moveRight_dyn();

		virtual Void destroy( );

		virtual Void update( );

		virtual Void killPlayer( );
		Dynamic killPlayer_dyn();

		virtual Void activateSpikes( ::Spikes thisSpike,::Player thisPlayer);
		Dynamic activateSpikes_dyn();

		virtual Void placeTiles( ::String entityName,::Xml entityData);
		Dynamic placeTiles_dyn();

		virtual Void removeBullet( ::flixel::FlxObject thisBullet,::flixel::FlxObject thisPlayer);
		Dynamic removeBullet_dyn();

		virtual Void hitBullet( ::flixel::FlxObject thisBullet,::Player thisPlayer);
		Dynamic hitBullet_dyn();

		virtual Void touchTile( ::FallingTile thisTile,::Player thisPlayer);
		Dynamic touchTile_dyn();

};


#endif /* INCLUDED_PlayState */ 
