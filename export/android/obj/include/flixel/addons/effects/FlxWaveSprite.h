#ifndef INCLUDED_flixel_addons_effects_FlxWaveSprite
#define INCLUDED_flixel_addons_effects_FlxWaveSprite

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/FlxSprite.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS3(flixel,addons,effects,FlxWaveSprite)
HX_DECLARE_CLASS3(flixel,addons,effects,WaveMode)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
namespace flixel{
namespace addons{
namespace effects{


class HXCPP_CLASS_ATTRIBUTES  FlxWaveSprite_obj : public ::flixel::FlxSprite_obj{
	public:
		typedef ::flixel::FlxSprite_obj super;
		typedef FlxWaveSprite_obj OBJ_;
		FlxWaveSprite_obj();
		Void __construct(::flixel::FlxSprite Target,::flixel::addons::effects::WaveMode Mode,hx::Null< int >  __o_Strength,hx::Null< int >  __o_Center,hx::Null< Float >  __o_Speed);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxWaveSprite_obj > __new(::flixel::FlxSprite Target,::flixel::addons::effects::WaveMode Mode,hx::Null< int >  __o_Strength,hx::Null< int >  __o_Center,hx::Null< Float >  __o_Speed);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxWaveSprite_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxWaveSprite"); }

		::flixel::addons::effects::WaveMode mode;
		Float speed;
		int center;
		int strength;
		::flixel::FlxSprite _target;
		Float _targetOffset;
		Float _time;
		virtual Void draw( );

		virtual Float calculateOffset( Float p);
		Dynamic calculateOffset_dyn();

		virtual Void initPixels( );
		Dynamic initPixels_dyn();

		virtual int set_strength( int value);
		Dynamic set_strength_dyn();

		static Float BASE_STRENGTH;
};

} // end namespace flixel
} // end namespace addons
} // end namespace effects

#endif /* INCLUDED_flixel_addons_effects_FlxWaveSprite */ 
