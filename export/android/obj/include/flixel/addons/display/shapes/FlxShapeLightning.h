#ifndef INCLUDED_flixel_addons_display_shapes_FlxShapeLightning
#define INCLUDED_flixel_addons_display_shapes_FlxShapeLightning

#ifndef HXCPP_H
#include <hxcpp.h>
#endif

#include <flixel/addons/display/shapes/FlxShapeLine.h>
HX_DECLARE_CLASS1(flixel,FlxBasic)
HX_DECLARE_CLASS1(flixel,FlxObject)
HX_DECLARE_CLASS1(flixel,FlxSprite)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShape)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShapeLightning)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,FlxShapeLine)
HX_DECLARE_CLASS4(flixel,addons,display,shapes,LineSegment)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxDestroyable)
HX_DECLARE_CLASS2(flixel,interfaces,IFlxPooled)
HX_DECLARE_CLASS2(flixel,util,FlxPoint)
HX_DECLARE_CLASS2(openfl,geom,Matrix)
namespace flixel{
namespace addons{
namespace display{
namespace shapes{


class HXCPP_CLASS_ATTRIBUTES  FlxShapeLightning_obj : public ::flixel::addons::display::shapes::FlxShapeLine_obj{
	public:
		typedef ::flixel::addons::display::shapes::FlxShapeLine_obj super;
		typedef FlxShapeLightning_obj OBJ_;
		FlxShapeLightning_obj();
		Void __construct(Float X,Float Y,::flixel::util::FlxPoint A,::flixel::util::FlxPoint B,Dynamic Style,hx::Null< bool >  __o_UseDefaults);

	public:
		inline void *operator new( size_t inSize, bool inContainer=true)
			{ return hx::Object::operator new(inSize,inContainer); }
		static hx::ObjectPtr< FlxShapeLightning_obj > __new(Float X,Float Y,::flixel::util::FlxPoint A,::flixel::util::FlxPoint B,Dynamic Style,hx::Null< bool >  __o_UseDefaults);
		static Dynamic __CreateEmpty();
		static Dynamic __Create(hx::DynamicArray inArgs);
		//~FlxShapeLightning_obj();

		HX_DO_RTTI;
		static void __boot();
		static void __register();
		void __Mark(HX_MARK_PARAMS);
		void __Visit(HX_VISIT_PARAMS);
		::String __ToString() const { return HX_CSTRING("FlxShapeLightning"); }

		Dynamic lightningStyle;
		Dynamic halo_cols;
		Float detail;
		Float magnitude;
		Array< ::Dynamic > list_segs;
		Array< ::Dynamic > list_branch;
		bool filterDirty;
		virtual Void addSegment( ::flixel::util::FlxPoint A,::flixel::util::FlxPoint B);
		Dynamic addSegment_dyn();

		virtual Void calculate( ::flixel::util::FlxPoint A,::flixel::util::FlxPoint B,Float Displacement,int Iteration);
		Dynamic calculate_dyn();

		virtual Dynamic set_lightningStyle( Dynamic Style);
		Dynamic set_lightningStyle_dyn();

		virtual Dynamic copyLineStyle( Dynamic ls);
		Dynamic copyLineStyle_dyn();

		virtual Void drawSpecificShape( ::openfl::geom::Matrix matrix);

		virtual Void fixBoundaries( Float trueWidth,Float trueHeight);

		virtual Void draw( );

};

} // end namespace flixel
} // end namespace addons
} // end namespace display
} // end namespace shapes

#endif /* INCLUDED_flixel_addons_display_shapes_FlxShapeLightning */ 
