package;


import haxe.Timer;
import haxe.Unserializer;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.MovieClip;
import openfl.events.Event;
import openfl.text.Font;
import openfl.media.Sound;
import openfl.net.URLRequest;
import openfl.utils.ByteArray;
import openfl.Assets;

#if (flash || js)
import openfl.display.Loader;
import openfl.events.Event;
import openfl.net.URLLoader;
#end

#if sys
import sys.FileSystem;
#end

#if ios
import openfl.utils.SystemPath;
#end


@:access(openfl.media.Sound)
class DefaultAssetLibrary extends AssetLibrary {
	
	
	public var className (default, null) = new Map <String, Dynamic> ();
	public var path (default, null) = new Map <String, String> ();
	public var type (default, null) = new Map <String, AssetType> ();
	
	private var lastModified:Float;
	private var timer:Timer;
	
	
	public function new () {
		
		super ();
		
		#if flash
		
		className.set ("assets/data/data-goes-here.txt", __ASSET__assets_data_data_goes_here_txt);
		type.set ("assets/data/data-goes-here.txt", AssetType.TEXT);
		className.set ("assets/data/Level1.oel", __ASSET__assets_data_level1_oel);
		type.set ("assets/data/Level1.oel", AssetType.TEXT);
		className.set ("assets/images/blade.wav", __ASSET__assets_images_blade_wav);
		type.set ("assets/images/blade.wav", AssetType.SOUND);
		className.set ("assets/images/bullet.png", __ASSET__assets_images_bullet_png);
		type.set ("assets/images/bullet.png", AssetType.IMAGE);
		className.set ("assets/images/crumbling.wav", __ASSET__assets_images_crumbling_wav);
		type.set ("assets/images/crumbling.wav", AssetType.SOUND);
		className.set ("assets/images/door.png", __ASSET__assets_images_door_png);
		type.set ("assets/images/door.png", AssetType.IMAGE);
		className.set ("assets/images/enemy.png", __ASSET__assets_images_enemy_png);
		type.set ("assets/images/enemy.png", AssetType.IMAGE);
		className.set ("assets/images/enemy_hit.wav", __ASSET__assets_images_enemy_hit_wav);
		type.set ("assets/images/enemy_hit.wav", AssetType.SOUND);
		className.set ("assets/images/final root games.png", __ASSET__assets_images_final_root_games_png);
		type.set ("assets/images/final root games.png", AssetType.IMAGE);
		className.set ("assets/images/goomba.png", __ASSET__assets_images_goomba_png);
		type.set ("assets/images/goomba.png", AssetType.IMAGE);
		className.set ("assets/images/goomba_hit.wav", __ASSET__assets_images_goomba_hit_wav);
		type.set ("assets/images/goomba_hit.wav", AssetType.SOUND);
		className.set ("assets/images/hurt.wav", __ASSET__assets_images_hurt_wav);
		type.set ("assets/images/hurt.wav", AssetType.SOUND);
		className.set ("assets/images/images-go-here.txt", __ASSET__assets_images_images_go_here_txt);
		type.set ("assets/images/images-go-here.txt", AssetType.TEXT);
		className.set ("assets/images/key.png", __ASSET__assets_images_key_png);
		type.set ("assets/images/key.png", AssetType.IMAGE);
		className.set ("assets/images/key.wav", __ASSET__assets_images_key_wav);
		type.set ("assets/images/key.wav", AssetType.SOUND);
		className.set ("assets/images/left.png", __ASSET__assets_images_left_png);
		type.set ("assets/images/left.png", AssetType.IMAGE);
		className.set ("assets/images/light.png", __ASSET__assets_images_light_png);
		type.set ("assets/images/light.png", AssetType.IMAGE);
		className.set ("assets/images/particle.png", __ASSET__assets_images_particle_png);
		type.set ("assets/images/particle.png", AssetType.IMAGE);
		className.set ("assets/images/particle.wav", __ASSET__assets_images_particle_wav);
		type.set ("assets/images/particle.wav", AssetType.SOUND);
		className.set ("assets/images/pickleTile.png", __ASSET__assets_images_pickletile_png);
		type.set ("assets/images/pickleTile.png", AssetType.IMAGE);
		className.set ("assets/images/player.png", __ASSET__assets_images_player_png);
		type.set ("assets/images/player.png", AssetType.IMAGE);
		className.set ("assets/images/player1.png", __ASSET__assets_images_player1_png);
		type.set ("assets/images/player1.png", AssetType.IMAGE);
		className.set ("assets/images/right.png", __ASSET__assets_images_right_png);
		type.set ("assets/images/right.png", AssetType.IMAGE);
		className.set ("assets/images/ships.png", __ASSET__assets_images_ships_png);
		type.set ("assets/images/ships.png", AssetType.IMAGE);
		className.set ("assets/images/shoot.wav", __ASSET__assets_images_shoot_wav);
		type.set ("assets/images/shoot.wav", AssetType.SOUND);
		className.set ("assets/images/spike.png", __ASSET__assets_images_spike_png);
		type.set ("assets/images/spike.png", AssetType.IMAGE);
		className.set ("assets/images/squash.png", __ASSET__assets_images_squash_png);
		type.set ("assets/images/squash.png", AssetType.IMAGE);
		className.set ("assets/images/staticSpike.png", __ASSET__assets_images_staticspike_png);
		type.set ("assets/images/staticSpike.png", AssetType.IMAGE);
		className.set ("assets/images/Tile.png", __ASSET__assets_images_tile_png);
		type.set ("assets/images/Tile.png", AssetType.IMAGE);
		className.set ("assets/images/turret.png", __ASSET__assets_images_turret_png);
		type.set ("assets/images/turret.png", AssetType.IMAGE);
		className.set ("assets/images/up.png", __ASSET__assets_images_up_png);
		type.set ("assets/images/up.png", AssetType.IMAGE);
		className.set ("assets/images/upTile.png", __ASSET__assets_images_uptile_png);
		type.set ("assets/images/upTile.png", AssetType.IMAGE);
		className.set ("assets/images/yellowLight.png", __ASSET__assets_images_yellowlight_png);
		type.set ("assets/images/yellowLight.png", AssetType.IMAGE);
		className.set ("assets/music/main.wav", __ASSET__assets_music_main_wav);
		type.set ("assets/music/main.wav", AssetType.SOUND);
		className.set ("assets/music/music-goes-here.txt", __ASSET__assets_music_music_goes_here_txt);
		type.set ("assets/music/music-goes-here.txt", AssetType.TEXT);
		className.set ("assets/sounds/sounds-go-here.txt", __ASSET__assets_sounds_sounds_go_here_txt);
		type.set ("assets/sounds/sounds-go-here.txt", AssetType.TEXT);
		className.set ("assets/sounds/beep.mp3", __ASSET__assets_sounds_beep_mp3);
		type.set ("assets/sounds/beep.mp3", AssetType.MUSIC);
		className.set ("assets/sounds/flixel.mp3", __ASSET__assets_sounds_flixel_mp3);
		type.set ("assets/sounds/flixel.mp3", AssetType.MUSIC);
		className.set ("assets/sounds/beep.ogg", __ASSET__assets_sounds_beep_ogg);
		type.set ("assets/sounds/beep.ogg", AssetType.SOUND);
		className.set ("assets/sounds/flixel.ogg", __ASSET__assets_sounds_flixel_ogg);
		type.set ("assets/sounds/flixel.ogg", AssetType.SOUND);
		
		
		#elseif html5
		
		var id;
		id = "assets/data/data-goes-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/data/Level1.oel";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/images/blade.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/bullet.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/crumbling.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/door.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/enemy.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/enemy_hit.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/final root games.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/goomba.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/goomba_hit.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/hurt.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/images-go-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/images/key.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/key.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/left.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/light.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/particle.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/particle.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/pickleTile.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/player.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/player1.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/right.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/ships.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/shoot.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/images/spike.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/squash.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/staticSpike.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/Tile.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/turret.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/up.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/upTile.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/images/yellowLight.png";
		path.set (id, id);
		type.set (id, AssetType.IMAGE);
		id = "assets/music/main.wav";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/music/music-goes-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/sounds/sounds-go-here.txt";
		path.set (id, id);
		type.set (id, AssetType.TEXT);
		id = "assets/sounds/beep.mp3";
		path.set (id, id);
		type.set (id, AssetType.MUSIC);
		id = "assets/sounds/flixel.mp3";
		path.set (id, id);
		type.set (id, AssetType.MUSIC);
		id = "assets/sounds/beep.ogg";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		id = "assets/sounds/flixel.ogg";
		path.set (id, id);
		type.set (id, AssetType.SOUND);
		
		
		#else
		
		#if (windows || mac || linux)
		
		var useManifest = false;
		
		className.set ("assets/data/data-goes-here.txt", __ASSET__assets_data_data_goes_here_txt);
		type.set ("assets/data/data-goes-here.txt", AssetType.TEXT);
		
		className.set ("assets/data/Level1.oel", __ASSET__assets_data_level1_oel);
		type.set ("assets/data/Level1.oel", AssetType.TEXT);
		
		className.set ("assets/images/blade.wav", __ASSET__assets_images_blade_wav);
		type.set ("assets/images/blade.wav", AssetType.SOUND);
		
		className.set ("assets/images/bullet.png", __ASSET__assets_images_bullet_png);
		type.set ("assets/images/bullet.png", AssetType.IMAGE);
		
		className.set ("assets/images/crumbling.wav", __ASSET__assets_images_crumbling_wav);
		type.set ("assets/images/crumbling.wav", AssetType.SOUND);
		
		className.set ("assets/images/door.png", __ASSET__assets_images_door_png);
		type.set ("assets/images/door.png", AssetType.IMAGE);
		
		className.set ("assets/images/enemy.png", __ASSET__assets_images_enemy_png);
		type.set ("assets/images/enemy.png", AssetType.IMAGE);
		
		className.set ("assets/images/enemy_hit.wav", __ASSET__assets_images_enemy_hit_wav);
		type.set ("assets/images/enemy_hit.wav", AssetType.SOUND);
		
		className.set ("assets/images/final root games.png", __ASSET__assets_images_final_root_games_png);
		type.set ("assets/images/final root games.png", AssetType.IMAGE);
		
		className.set ("assets/images/goomba.png", __ASSET__assets_images_goomba_png);
		type.set ("assets/images/goomba.png", AssetType.IMAGE);
		
		className.set ("assets/images/goomba_hit.wav", __ASSET__assets_images_goomba_hit_wav);
		type.set ("assets/images/goomba_hit.wav", AssetType.SOUND);
		
		className.set ("assets/images/hurt.wav", __ASSET__assets_images_hurt_wav);
		type.set ("assets/images/hurt.wav", AssetType.SOUND);
		
		className.set ("assets/images/images-go-here.txt", __ASSET__assets_images_images_go_here_txt);
		type.set ("assets/images/images-go-here.txt", AssetType.TEXT);
		
		className.set ("assets/images/key.png", __ASSET__assets_images_key_png);
		type.set ("assets/images/key.png", AssetType.IMAGE);
		
		className.set ("assets/images/key.wav", __ASSET__assets_images_key_wav);
		type.set ("assets/images/key.wav", AssetType.SOUND);
		
		className.set ("assets/images/left.png", __ASSET__assets_images_left_png);
		type.set ("assets/images/left.png", AssetType.IMAGE);
		
		className.set ("assets/images/light.png", __ASSET__assets_images_light_png);
		type.set ("assets/images/light.png", AssetType.IMAGE);
		
		className.set ("assets/images/particle.png", __ASSET__assets_images_particle_png);
		type.set ("assets/images/particle.png", AssetType.IMAGE);
		
		className.set ("assets/images/particle.wav", __ASSET__assets_images_particle_wav);
		type.set ("assets/images/particle.wav", AssetType.SOUND);
		
		className.set ("assets/images/pickleTile.png", __ASSET__assets_images_pickletile_png);
		type.set ("assets/images/pickleTile.png", AssetType.IMAGE);
		
		className.set ("assets/images/player.png", __ASSET__assets_images_player_png);
		type.set ("assets/images/player.png", AssetType.IMAGE);
		
		className.set ("assets/images/player1.png", __ASSET__assets_images_player1_png);
		type.set ("assets/images/player1.png", AssetType.IMAGE);
		
		className.set ("assets/images/right.png", __ASSET__assets_images_right_png);
		type.set ("assets/images/right.png", AssetType.IMAGE);
		
		className.set ("assets/images/ships.png", __ASSET__assets_images_ships_png);
		type.set ("assets/images/ships.png", AssetType.IMAGE);
		
		className.set ("assets/images/shoot.wav", __ASSET__assets_images_shoot_wav);
		type.set ("assets/images/shoot.wav", AssetType.SOUND);
		
		className.set ("assets/images/spike.png", __ASSET__assets_images_spike_png);
		type.set ("assets/images/spike.png", AssetType.IMAGE);
		
		className.set ("assets/images/squash.png", __ASSET__assets_images_squash_png);
		type.set ("assets/images/squash.png", AssetType.IMAGE);
		
		className.set ("assets/images/staticSpike.png", __ASSET__assets_images_staticspike_png);
		type.set ("assets/images/staticSpike.png", AssetType.IMAGE);
		
		className.set ("assets/images/Tile.png", __ASSET__assets_images_tile_png);
		type.set ("assets/images/Tile.png", AssetType.IMAGE);
		
		className.set ("assets/images/turret.png", __ASSET__assets_images_turret_png);
		type.set ("assets/images/turret.png", AssetType.IMAGE);
		
		className.set ("assets/images/up.png", __ASSET__assets_images_up_png);
		type.set ("assets/images/up.png", AssetType.IMAGE);
		
		className.set ("assets/images/upTile.png", __ASSET__assets_images_uptile_png);
		type.set ("assets/images/upTile.png", AssetType.IMAGE);
		
		className.set ("assets/images/yellowLight.png", __ASSET__assets_images_yellowlight_png);
		type.set ("assets/images/yellowLight.png", AssetType.IMAGE);
		
		className.set ("assets/music/main.wav", __ASSET__assets_music_main_wav);
		type.set ("assets/music/main.wav", AssetType.SOUND);
		
		className.set ("assets/music/music-goes-here.txt", __ASSET__assets_music_music_goes_here_txt);
		type.set ("assets/music/music-goes-here.txt", AssetType.TEXT);
		
		className.set ("assets/sounds/sounds-go-here.txt", __ASSET__assets_sounds_sounds_go_here_txt);
		type.set ("assets/sounds/sounds-go-here.txt", AssetType.TEXT);
		
		className.set ("assets/sounds/beep.mp3", __ASSET__assets_sounds_beep_mp3);
		type.set ("assets/sounds/beep.mp3", AssetType.MUSIC);
		
		className.set ("assets/sounds/flixel.mp3", __ASSET__assets_sounds_flixel_mp3);
		type.set ("assets/sounds/flixel.mp3", AssetType.MUSIC);
		
		className.set ("assets/sounds/beep.ogg", __ASSET__assets_sounds_beep_ogg);
		type.set ("assets/sounds/beep.ogg", AssetType.SOUND);
		
		className.set ("assets/sounds/flixel.ogg", __ASSET__assets_sounds_flixel_ogg);
		type.set ("assets/sounds/flixel.ogg", AssetType.SOUND);
		
		
		if (useManifest) {
			
			loadManifest ();
			
			if (Sys.args ().indexOf ("-livereload") > -1) {
				
				var path = FileSystem.fullPath ("manifest");
				lastModified = FileSystem.stat (path).mtime.getTime ();
				
				timer = new Timer (2000);
				timer.run = function () {
					
					var modified = FileSystem.stat (path).mtime.getTime ();
					
					if (modified > lastModified) {
						
						lastModified = modified;
						loadManifest ();
						
						if (eventCallback != null) {
							
							eventCallback (this, "change");
							
						}
						
					}
					
				}
				
			}
			
		}
		
		#else
		
		loadManifest ();
		
		#end
		#end
		
	}
	
	
	public override function exists (id:String, type:AssetType):Bool {
		
		var assetType = this.type.get (id);
		
		#if pixi
		
		if (assetType == IMAGE) {
			
			return true;
			
		} else {
			
			return false;
			
		}
		
		#end
		
		if (assetType != null) {
			
			if (assetType == type || ((type == SOUND || type == MUSIC) && (assetType == MUSIC || assetType == SOUND))) {
				
				return true;
				
			}
			
			#if flash
			
			if ((assetType == BINARY || assetType == TEXT) && type == BINARY) {
				
				return true;
				
			} else if (path.exists (id)) {
				
				return true;
				
			}
			
			#else
			
			if (type == BINARY || type == null) {
				
				return true;
				
			}
			
			#end
			
		}
		
		return false;
		
	}
	
	
	public override function getBitmapData (id:String):BitmapData {
		
		#if pixi
		
		return BitmapData.fromImage (path.get (id));
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), BitmapData);
		
		#elseif openfl_html5
		
		return BitmapData.fromImage (ApplicationMain.images.get (path.get (id)));
		
		#elseif js
		
		return cast (ApplicationMain.loaders.get (path.get (id)).contentLoaderInfo.content, Bitmap).bitmapData;
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), BitmapData);
		else return BitmapData.load (path.get (id));
		
		#end
		
	}
	
	
	public override function getBytes (id:String):ByteArray {
		
		#if (flash)
		
		return cast (Type.createInstance (className.get (id), []), ByteArray);

		#elseif (js || openfl_html5 || pixi)
		
		var bytes:ByteArray = null;
		var data = ApplicationMain.urlLoaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			bytes = new ByteArray ();
			bytes.writeUTFBytes (data);
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}

		if (bytes != null) {
			
			bytes.position = 0;
			return bytes;
			
		} else {
			
			return null;
		}
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), ByteArray);
		else return ByteArray.readFile (path.get (id));
		
		#end
		
	}
	
	
	public override function getFont (id:String):Font {
		
		#if pixi
		
		return null;
		
		#elseif (flash || js)
		
		return cast (Type.createInstance (className.get (id), []), Font);
		
		#else
		
		if (className.exists(id)) {
			var fontClass = className.get(id);
			Font.registerFont(fontClass);
			return cast (Type.createInstance (fontClass, []), Font);
		} else return new Font (path.get (id));
		
		#end
		
	}
	
	
	public override function getMusic (id:String):Sound {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif openfl_html5
		
		var sound = new Sound ();
		sound.__buffer = true;
		sound.load (new URLRequest (path.get (id)));
		return sound; 
		
		#elseif js
		
		return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		else return new Sound (new URLRequest (path.get (id)), null, true);
		
		#end
		
	}
	
	
	public override function getPath (id:String):String {
		
		#if ios
		
		return SystemPath.applicationDirectory + "/assets/" + path.get (id);
		
		#else
		
		return path.get (id);
		
		#end
		
	}
	
	
	public override function getSound (id:String):Sound {
		
		#if pixi
		
		return null;
		
		#elseif (flash)
		
		return cast (Type.createInstance (className.get (id), []), Sound);
		
		#elseif js
		
		return new Sound (new URLRequest (path.get (id)));
		
		#else
		
		if (className.exists(id)) return cast (Type.createInstance (className.get (id), []), Sound);
		else return new Sound (new URLRequest (path.get (id)), null, type.get (id) == MUSIC);
		
		#end
		
	}
	
	
	public override function getText (id:String):String {
		
		#if js
		
		var bytes:ByteArray = null;
		var data = ApplicationMain.urlLoaders.get (path.get (id)).data;
		
		if (Std.is (data, String)) {
			
			return cast data;
			
		} else if (Std.is (data, ByteArray)) {
			
			bytes = cast data;
			
		} else {
			
			bytes = null;
			
		}
		
		if (bytes != null) {
			
			bytes.position = 0;
			return bytes.readUTFBytes (bytes.length);
			
		} else {
			
			return null;
		}
		
		#else
		
		var bytes = getBytes (id);
		
		if (bytes == null) {
			
			return null;
			
		} else {
			
			return bytes.readUTFBytes (bytes.length);
			
		}
		
		#end
		
	}
	
	
	public override function isLocal (id:String, type:AssetType):Bool {
		
		#if flash
		
		if (type != AssetType.MUSIC && type != AssetType.SOUND) {
			
			return className.exists (id);
			
		}
		
		#end
		
		return true;
		
	}
	
	
	public override function list (type:AssetType):Array<String> {
		
		var items = [];
		
		for (id in this.type.keys ()) {
			
			if (type == null || exists (id, type)) {
				
				items.push (id);
				
			}
			
		}
		
		return items;
		
	}
	
	
	public override function loadBitmapData (id:String, handler:BitmapData -> Void):Void {
		
		#if pixi
		
		handler (getBitmapData (id));
		
		#elseif (flash || js)
		
		if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBitmapData (id));
			
		}
		
		#else
		
		handler (getBitmapData (id));
		
		#end
		
	}
	
	
	public override function loadBytes (id:String, handler:ByteArray -> Void):Void {
		
		#if pixi
		
		handler (getBytes (id));
		
		#elseif (flash || js)
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				var bytes = new ByteArray ();
				bytes.writeUTFBytes (event.currentTarget.data);
				bytes.position = 0;
				
				handler (bytes);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getBytes (id));
			
		}
		
		#else
		
		handler (getBytes (id));
		
		#end
		
	}
	
	
	public override function loadFont (id:String, handler:Font -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getFont (id));
			
		//}
		
		#else
		
		handler (getFont (id));
		
		#end
		
	}
	
	
	#if (!flash && !html5)
	private function loadManifest ():Void {
		
		try {
			
			#if blackberry
			var bytes = ByteArray.readFile ("app/native/manifest");
			#elseif tizen
			var bytes = ByteArray.readFile ("../res/manifest");
			#elseif emscripten
			var bytes = ByteArray.readFile ("assets/manifest");
			#else
			var bytes = ByteArray.readFile ("manifest");
			#end
			
			if (bytes != null) {
				
				bytes.position = 0;
				
				if (bytes.length > 0) {
					
					var data = bytes.readUTFBytes (bytes.length);
					
					if (data != null && data.length > 0) {
						
						var manifest:Array<Dynamic> = Unserializer.run (data);
						
						for (asset in manifest) {
							
							if (!className.exists (asset.id)) {
								
								path.set (asset.id, asset.path);
								type.set (asset.id, Type.createEnum (AssetType, asset.type));
								
							}
							
						}
						
					}
					
				}
				
			} else {
				
				trace ("Warning: Could not load asset manifest (bytes was null)");
				
			}
		
		} catch (e:Dynamic) {
			
			trace ('Warning: Could not load asset manifest (${e})');
			
		}
		
	}
	#end
	
	
	public override function loadMusic (id:String, handler:Sound -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getMusic (id));
			
		//}
		
		#else
		
		handler (getMusic (id));
		
		#end
		
	}
	
	
	public override function loadSound (id:String, handler:Sound -> Void):Void {
		
		#if (flash || js)
		
		/*if (path.exists (id)) {
			
			var loader = new Loader ();
			loader.contentLoaderInfo.addEventListener (Event.COMPLETE, function (event) {
				
				handler (cast (event.currentTarget.content, Bitmap).bitmapData);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {*/
			
			handler (getSound (id));
			
		//}
		
		#else
		
		handler (getSound (id));
		
		#end
		
	}
	
	
	public override function loadText (id:String, handler:String -> Void):Void {
		
		#if js
		
		if (path.exists (id)) {
			
			var loader = new URLLoader ();
			loader.addEventListener (Event.COMPLETE, function (event:Event) {
				
				handler (event.currentTarget.data);
				
			});
			loader.load (new URLRequest (path.get (id)));
			
		} else {
			
			handler (getText (id));
			
		}
		
		#else
		
		var callback = function (bytes:ByteArray):Void {
			
			if (bytes == null) {
				
				handler (null);
				
			} else {
				
				handler (bytes.readUTFBytes (bytes.length));
				
			}
			
		}
		
		loadBytes (id, callback);
		
		#end
		
	}
	
	
}


#if pixi
#elseif flash

@:keep class __ASSET__assets_data_data_goes_here_txt extends null { }
@:keep class __ASSET__assets_data_level1_oel extends null { }
@:keep class __ASSET__assets_images_blade_wav extends null { }
@:keep class __ASSET__assets_images_bullet_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_crumbling_wav extends null { }
@:keep class __ASSET__assets_images_door_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_enemy_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_enemy_hit_wav extends null { }
@:keep class __ASSET__assets_images_final_root_games_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_goomba_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_goomba_hit_wav extends null { }
@:keep class __ASSET__assets_images_hurt_wav extends null { }
@:keep class __ASSET__assets_images_images_go_here_txt extends null { }
@:keep class __ASSET__assets_images_key_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_key_wav extends null { }
@:keep class __ASSET__assets_images_left_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_light_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_particle_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_particle_wav extends null { }
@:keep class __ASSET__assets_images_pickletile_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_player_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_player1_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_right_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_ships_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_shoot_wav extends null { }
@:keep class __ASSET__assets_images_spike_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_squash_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_staticspike_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_tile_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_turret_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_up_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_uptile_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_images_yellowlight_png extends flash.display.BitmapData { public function new () { super (0, 0, true, 0); } }
@:keep class __ASSET__assets_music_main_wav extends null { }
@:keep class __ASSET__assets_music_music_goes_here_txt extends null { }
@:keep class __ASSET__assets_sounds_sounds_go_here_txt extends null { }
@:keep class __ASSET__assets_sounds_beep_mp3 extends null { }
@:keep class __ASSET__assets_sounds_flixel_mp3 extends null { }
@:keep class __ASSET__assets_sounds_beep_ogg extends null { }
@:keep class __ASSET__assets_sounds_flixel_ogg extends null { }


#elseif html5











































#elseif (windows || mac || linux)


@:file("assets/data/data-goes-here.txt") class __ASSET__assets_data_data_goes_here_txt extends flash.utils.ByteArray {}
@:file("assets/data/Level1.oel") class __ASSET__assets_data_level1_oel extends flash.utils.ByteArray {}
@:sound("assets/images/blade.wav") class __ASSET__assets_images_blade_wav extends flash.media.Sound {}
@:bitmap("assets/images/bullet.png") class __ASSET__assets_images_bullet_png extends flash.display.BitmapData {}
@:sound("assets/images/crumbling.wav") class __ASSET__assets_images_crumbling_wav extends flash.media.Sound {}
@:bitmap("assets/images/door.png") class __ASSET__assets_images_door_png extends flash.display.BitmapData {}
@:bitmap("assets/images/enemy.png") class __ASSET__assets_images_enemy_png extends flash.display.BitmapData {}
@:sound("assets/images/enemy_hit.wav") class __ASSET__assets_images_enemy_hit_wav extends flash.media.Sound {}
@:bitmap("assets/images/final root games.png") class __ASSET__assets_images_final_root_games_png extends flash.display.BitmapData {}
@:bitmap("assets/images/goomba.png") class __ASSET__assets_images_goomba_png extends flash.display.BitmapData {}
@:sound("assets/images/goomba_hit.wav") class __ASSET__assets_images_goomba_hit_wav extends flash.media.Sound {}
@:sound("assets/images/hurt.wav") class __ASSET__assets_images_hurt_wav extends flash.media.Sound {}
@:file("assets/images/images-go-here.txt") class __ASSET__assets_images_images_go_here_txt extends flash.utils.ByteArray {}
@:bitmap("assets/images/key.png") class __ASSET__assets_images_key_png extends flash.display.BitmapData {}
@:sound("assets/images/key.wav") class __ASSET__assets_images_key_wav extends flash.media.Sound {}
@:bitmap("assets/images/left.png") class __ASSET__assets_images_left_png extends flash.display.BitmapData {}
@:bitmap("assets/images/light.png") class __ASSET__assets_images_light_png extends flash.display.BitmapData {}
@:bitmap("assets/images/particle.png") class __ASSET__assets_images_particle_png extends flash.display.BitmapData {}
@:sound("assets/images/particle.wav") class __ASSET__assets_images_particle_wav extends flash.media.Sound {}
@:bitmap("assets/images/pickleTile.png") class __ASSET__assets_images_pickletile_png extends flash.display.BitmapData {}
@:bitmap("assets/images/player.png") class __ASSET__assets_images_player_png extends flash.display.BitmapData {}
@:bitmap("assets/images/player1.png") class __ASSET__assets_images_player1_png extends flash.display.BitmapData {}
@:bitmap("assets/images/right.png") class __ASSET__assets_images_right_png extends flash.display.BitmapData {}
@:bitmap("assets/images/ships.png") class __ASSET__assets_images_ships_png extends flash.display.BitmapData {}
@:sound("assets/images/shoot.wav") class __ASSET__assets_images_shoot_wav extends flash.media.Sound {}
@:bitmap("assets/images/spike.png") class __ASSET__assets_images_spike_png extends flash.display.BitmapData {}
@:bitmap("assets/images/squash.png") class __ASSET__assets_images_squash_png extends flash.display.BitmapData {}
@:bitmap("assets/images/staticSpike.png") class __ASSET__assets_images_staticspike_png extends flash.display.BitmapData {}
@:bitmap("assets/images/Tile.png") class __ASSET__assets_images_tile_png extends flash.display.BitmapData {}
@:bitmap("assets/images/turret.png") class __ASSET__assets_images_turret_png extends flash.display.BitmapData {}
@:bitmap("assets/images/up.png") class __ASSET__assets_images_up_png extends flash.display.BitmapData {}
@:bitmap("assets/images/upTile.png") class __ASSET__assets_images_uptile_png extends flash.display.BitmapData {}
@:bitmap("assets/images/yellowLight.png") class __ASSET__assets_images_yellowlight_png extends flash.display.BitmapData {}
@:sound("assets/music/main.wav") class __ASSET__assets_music_main_wav extends flash.media.Sound {}
@:file("assets/music/music-goes-here.txt") class __ASSET__assets_music_music_goes_here_txt extends flash.utils.ByteArray {}
@:file("assets/sounds/sounds-go-here.txt") class __ASSET__assets_sounds_sounds_go_here_txt extends flash.utils.ByteArray {}
@:sound("B:/HaxeToolkit/haxe/lib/flixel/3,3,5/assets/sounds/beep.mp3") class __ASSET__assets_sounds_beep_mp3 extends flash.media.Sound {}
@:sound("B:/HaxeToolkit/haxe/lib/flixel/3,3,5/assets/sounds/flixel.mp3") class __ASSET__assets_sounds_flixel_mp3 extends flash.media.Sound {}
@:sound("B:/HaxeToolkit/haxe/lib/flixel/3,3,5/assets/sounds/beep.ogg") class __ASSET__assets_sounds_beep_ogg extends flash.media.Sound {}
@:sound("B:/HaxeToolkit/haxe/lib/flixel/3,3,5/assets/sounds/flixel.ogg") class __ASSET__assets_sounds_flixel_ogg extends flash.media.Sound {}


#end
